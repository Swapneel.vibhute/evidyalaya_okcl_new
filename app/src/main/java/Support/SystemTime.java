package Support;

import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by abhinav on 17/9/16.
 */
public class SystemTime
{
    int pHour, pSecond;
    int pMinute;
    EditText editText;
    TimePickerDialog timePickerDialog;
    Context context;

    public SystemTime() {

    }

    public static String getSystemTime()
    {
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = timeFormat.format(new Date());
        Log.i("Time===>", "Yes " + time);
        return time;
    }

    public static String getSystemTime2()
    {
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String time = timeFormat.format(new Date());
        Log.i("Time===>", "Yes " + time);
        return time;
    }

    public static String getSystemDate()
    {
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd");
        String time = timeFormat.format(new Date());
        Log.i("Time===>", "Yes " + time);
        return time;
    }

    public static String getSystemDate1()
    {
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy");
        String time = timeFormat.format(new Date());
        Log.i("Time===>", "Yes " + time);
        return time;
    }

    public static double getSystemDateChart()
    {
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd.MM");
        String time = timeFormat.format(new Date());
        Log.i("Time===>", "Yes " + time);
        double value = Double.parseDouble(time);
        return value;
    }
}
