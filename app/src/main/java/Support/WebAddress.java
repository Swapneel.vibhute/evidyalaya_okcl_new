package Support;

/**
 * Created by abhinav on 27/9/16.
 */
public class WebAddress
{
    /*      public  final static String OKCLSOAPADDRESS= "http://www.evidyalaya.net.in/WebServices/OKCLWebServices.asmx";
          public  final static String DashboardReports= "http://103.16.143.251:8130/WebServices/DashboardReports.asmx";
          public final static String ComplaintMonitiorWS ="http://103.16.143.251:8130/WebServices/ComplaintMonitiorWS.asmx";

          public final static String TeacherStudFeedbackWS ="http://103.16.143.251:8130/WebServices/TeacherStudFeedbackWS.asmx";
          public  final static  String ICTCoordinatorWS ="http://103.16.143.251:8130/WebServices/ICTCoordinatorWS.asmx";

          public  final static  String ComponentChecklistWS = "http://103.16.143.251:8130/WebServices/ComponentChecklistWS.asmx";
  */
//   public final static String Universalsoapaddress = "http://www.evidyalaya.net.in";
   public final static String Universalsoapaddress = "http://evidyalaya.abhinavitsolutions.com";
 //  public final static String Universalsoapaddress = "http://test.evidyalaya.net.in";

    //http://test.evidyalaya.net.in/WebServices/ComplaintMonitiorWS.asmx?op=DownloadComplaint

    public final static String ComplaintMonitiorWS = Universalsoapaddress + "/WebServices/ComplaintMonitiorWS.asmx";

    public final static String ComponentChecklistWS = Universalsoapaddress + "/WebServices/ComponentChecklistWS.asmx";

    public final static String DashboardReports = Universalsoapaddress + "/WebServices/DashboardReports.asmx";

    public final static String ICTCoordinatorWS = Universalsoapaddress + "/WebServices/ICTCoordinatorWS.asmx";

    public final static String OKCLSOAPADDRESS = Universalsoapaddress + "/WebServices/OKCLWebServices.asmx";

    public final static String TeacherStudFeedbackWS = Universalsoapaddress + "/WebServices/TeacherStudFeedbackWS.asmx";


}