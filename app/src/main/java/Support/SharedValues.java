package Support;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SharedValues
{
    Context mContext;

    public SharedValues(Context mContext) {
        super();
        this.mContext = mContext;
    }
    public void saveSharedPreference(String key, String value) {
        Log.i("SAve", key + value);
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = pref.edit();
        ed.putString(key, value);
        ed.commit();
    }

    public String loadSharedPreference_StateId() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("stateid", "");
        return stateid;
    }

    public String loadSharedPreference_DistrictId() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("DistId", "");
        return stateid;
    }

    public String loadSharedPreference_Taluka_Id() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("taluka_id", "");
        return stateid;
    }

    public String loadSharedPreference_MobileNo() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String mobileNo = pref.getString("mobileNo", "");
        return mobileNo;
    }
    public String loadSharedPreference_UserRole() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String mobileNo = pref.getString("userRole", "");
        return mobileNo;
    }

    public String loadSharedPreference_UserRolename() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String mobileNo = pref.getString("userrolename", "");
        return mobileNo;
    }
    public String loadSharedPreference_savedate() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String mobileNo = pref.getString("savedate", "");
        return mobileNo;
    }

    public String loadSharedPreference_zonenumber() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String mobileNo = pref.getString("savezone", "");
        return mobileNo;
    }

    public String loadSharedPreference_flagforudisewise() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String mobileNo = pref.getString("udisecodeflag", "");
        return mobileNo;
    }

    public String loadSharedPreference_udisecodefromselectticket() {
    SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
    String mobileNo = pref.getString("udisecodefromselecttivkettypeforfragment", "");
    return mobileNo;
    }


 /*Shared Preferences For Offline Data Load*/
//
    public String loadSharedPreference_StudentLearning() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("studentlearning", "");
        return stateid;
    }

    public String loadSharedPreference_WormUpload() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("wormupload", "");
        return stateid;
    }
//
    public String loadSharedPreference_IssueState() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("issuestate", "");
        return stateid;
    }
//
    public String loadSharedPreference_DateWise() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("datewise", "");
        return stateid;
    }

    public String loadSharedPreference_ZoneWise() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("zonewise", "");
        return stateid;
    }
//
    public String loadSharedPreference_CountEnrollPassed() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("enrollpassed", "");
        return stateid;
    }

    public String loadSharedPreference_DistrictWiseSC() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("districtWiseSC", "");
        return stateid;
    }
    public String loadSharedPreference_DistWiseWorm() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("districtWiseWorm", "");
        return stateid;
    }
    public String loadSharedPreference_districtWise9_10() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("districtWise9_10", "");
        return stateid;
    }

    public String loadSharedPreference_SchoolInformation() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("schoolinfo", "");
        return stateid;
    }
    public String loadSharedPreference_SchoolInformation2() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("schoolinfo2", "");
        return stateid;
    }
    //
    public String loadSharedPreference_GetDistCount() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("DistCount", "");
        return stateid;
    }
    public String loadSharedPreference_getUdise() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("saveUdise", "");
        return stateid;
    }

    public String loadSharedPreference_getFlagStatus() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("flag", "");
        return stateid;
    }

    public String loadSharedPreference_dailyCount() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("dailyCount", "");
        return stateid;
    }
    public String loadSharedPreference_DistWiseTT() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("DistrictWiseTT", "");
        return stateid;
    }

    public String loadSharedPreference_DistWiseIssue() {
        SharedPreferences pref = mContext.getSharedPreferences("ezeeTest", Context.MODE_PRIVATE);
        String stateid = pref.getString("DistrictWiseIssueState", "");
        return stateid;
    }
}
