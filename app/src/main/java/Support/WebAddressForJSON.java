package Support;

/**
 * Created by abhinav on 10/9/17.
 */

public class WebAddressForJSON
{
  // public final static String UniversalJsonAddress  = "http://www.evidyalaya.net.in/JsonWebservices/";
   public final static String UniversalJsonAddress  = "http://test.evidyalaya.net.in/JsonWebservices/";
    public final static String GetDistrictWiseUdiseCode  = UniversalJsonAddress+"/Service1.svc/GetDistrictWiseUdiseCode?";
    public final static String GetUdiseWiseCount  = UniversalJsonAddress+"/Service1.svc/GetUdiseWiseCount?";
    public final static String UploadMileStone  = UniversalJsonAddress+"/MielStone.svc/UploadMileStone/MileStone";
    public final static String GetTeacherTraningTopCount  = UniversalJsonAddress+"/Service1.svc/GetTeacherTraningTopCount";
    public final static String GetTeacherTraingCountAllMob  = UniversalJsonAddress+"/Service1.svc/GetTeacherTraingCountAllMob?";
}
//"http://evidyalaya.net.in:8115";

//"http://test.evidyalaya.net.in/"