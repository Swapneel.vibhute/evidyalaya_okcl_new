package com.okcl.ICTMonitor.app;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import Support.GPSTracker;
import Support.SharedValues;
import Support.SystemTime;
import WebServices.DownloadUdiseByHMobile;
import WebServices.ForgetPasswordWebservice;
import WebServices.LoginValidationWS;

import static android.Manifest.permission.READ_CONTACTS;

/*
 * A login screen that offers login via Mobile Number and password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {
    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    Button register;
    DBAdapter adapter;
    Context context;
    Button downloadjunior, displayjunior, Ict;
    String userRole,distId,uid;
    GPSTracker gpsTracker;
    double latitude;
    double longitude;
    TelephonyManager tel;
    TextView forgotpassword;
    String deviceid;
    private static final int REQUEST_CONTACTS = 1;
    SystemTime st;
    String strDevId;

    private static String[] PERMISSIONS_CONTACT =
            {
                    android.Manifest.permission.READ_CONTACTS,
                    android.Manifest.permission.WRITE_CONTACTS,
                    android.Manifest.permission.GET_ACCOUNTS,
                    android.Manifest.permission.READ_PHONE_STATE,
                    android.Manifest.permission.SEND_SMS,
                    android.Manifest.permission.INTERNET,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
            };

    Handler passwordhandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            super.handleMessage(msg);
            if (msg.what == 105) {

            } else if (msg.what == 103)
            {
                Toast.makeText(getApplicationContext(), "Invalid Username please enter correct username .", Toast.LENGTH_SHORT).show();
               /* mEmailView.setText("");
                mPasswordView.setText("");*/
            } else if (msg.what == 106)
            {
                Toast.makeText(getApplicationContext(), "your password is send to your register mobile number ", Toast.LENGTH_SHORT).show();
            }
        }
    };

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                if (msg.what == 1) {
//                    distId =  new SharedValues(getApplicationContext()).loadSharedPreference_DistrictId();
                    new SharedValues(getApplicationContext()).saveSharedPreference("DistId",adapter.getdistid().trim());
                    //Toast.makeText(context, "District :"+adapter.getdistid().trim(), Toast.LENGTH_SHORT).show();
                    new SharedValues(getApplicationContext()).saveSharedPreference("userRole",adapter.getRole().toString().trim());
                    userRole = new SharedValues(getApplicationContext()).loadSharedPreference_UserRole();
//                    Toast.makeText(context, "role in login "+userRole, Toast.LENGTH_SHORT).show();
//                    distId =  new SharedValues(getApplicationContext()).loadSharedPreference_DistrictId();
/*
                    if (userRole.equals("3"))// 3	ICT Coordinator	9545137000
                    {
                        Intent intent = new Intent(LoginActivity.this, Drawer.class);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "Welcome\n" +
                                "e-Vidyalaya, School & Mass Education, Odisha", Toast.LENGTH_SHORT).show();
                        finish();
                        Intent i = new Intent(LoginActivity.this, LocationService.class);
                        startService(i);
                    }

                    //  Like.,,if(str.equals("john") || str.equals("mary") || str.equals("peter"))

                        if (userRole.equals("1") || userRole.equals("2") || userRole.equals("4") || userRole.equals("5") || userRole.equals("6") || userRole.equals("7") || userRole.equals("8") || userRole.equals("9") || userRole.equals("10") || userRole.equals("11") || userRole.equals("12") || userRole.equals("13") || userRole.equals("14") || userRole.equals("15") || userRole.equals("16") || userRole.equals("17") || userRole.equals("18") || userRole.equals("19") || userRole.equals("20"))
                        {
                            new SharedValues(getApplicationContext()).saveSharedPreference("flag","true");
                            //show only Icttrack & Dashboard
                            Intent intent = new Intent(LoginActivity.this, DrawerForHigherAuthority.class);
                            startActivity(intent);
                            finish();
                        }*/
                    if(isInternetOn()) {
                        if (userRole.equals("8")) {
                            Intent intent = new Intent(LoginActivity.this, NewDashBoardGraphActivity.class);
                            intent.putExtra("fromLogin", false);
                            startActivity(intent);
                            finish();
                        } else if (userRole.equals("5")) {
                            Intent intent = new Intent(LoginActivity.this, DistrictLevelGraph.class);
                            intent.putExtra("fromLogin",true);
                            startActivity(intent);
                            finish();
                        } else if (userRole.equals("2")) {
                            String u;
                            String hm =  new SharedValues(getApplicationContext()).loadSharedPreference_MobileNo();
                            new DownloadUdiseByHMobile(LoginActivity.this,LoginActivity.this,hm.toString(),handler).execute("");
                        }
                    }else{
                        Toast.makeText(context, "Please check your internet connetion`", Toast.LENGTH_SHORT).show();
                        if(userRole.equals("8")) {
                            try {
                                Intent intent = new Intent(LoginActivity.this, NewDashBoardGraphActivity.class);
                                startActivity(intent);
                                finish();
                            } catch (Exception err) {
                                Log.i("Error", "Show Error" + err);
                                Toast.makeText(getApplicationContext(), "Check your Internet connection " + err, Toast.LENGTH_SHORT).show();
                            }
                        }else if(userRole.equals("2")){
                            String u;
                            u = new SharedValues(getApplicationContext()).loadSharedPreference_getUdise();
                            Intent intent = new Intent(LoginActivity.this,SchoolDetails.class);
                            intent.putExtra("UDISE",u);
                            startActivity(intent);
                            finish();
                        }
                    }
                } else if (msg.what == 105) {
                    Toast.makeText(getApplicationContext(), "Invalid Username and password please try again.", Toast.LENGTH_SHORT).show();
                    mEmailView.setText("");
                    mPasswordView.setText("");
                } else if (msg.what == 106) {
                    Toast.makeText(getApplicationContext(), "your mobile number  is not verify from your senior so please contact with us", Toast.LENGTH_SHORT).show();
                }/*else if (msg.what == 109) {
                    Intent i = new Intent(LoginActivity.this, Otp.class);
                    i.putExtra("Mob", mEmailView.getText().toString());
                    i.putExtra("ref", "0");//pass values via intent to otp page
                    startActivity(i);
                }*/else if(msg.what == 2){
                    try{
                        String[] udise = new String[1];
                        String u;
                        JSONObject jsonObject = new JSONObject(msg.obj.toString());
                        udise = jsonObject.getString("Udisecodedash").replace("[", "").replace("]", "").split(",");
//                        Toast.makeText(context, "UDISE : "+udise[0].replace("\"",""), Toast.LENGTH_SHORT).show();
                        u = udise[0].replace("\"","") ;
                        new SharedValues(getApplicationContext()).saveSharedPreference("saveUdise",u);
                        Intent intent = new Intent(LoginActivity.this,SchoolDetails.class);
                        intent.putExtra("UDISE",u);
                        startActivity(intent);
                        finish();
                    }catch (JSONException err){
                        Toast.makeText(context, "Error :"+err, Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
               // Toast.makeText(context, "Something went wrong.", Toast.LENGTH_SHORT).show();
            }
        }
    };

    Handler handlerf = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {

            } else if (msg.what == 105) {

            }
        }
    };

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]
            {
                    "foo@example.com:hello", "bar@example.com:wor" +
                    "d"
            };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        /*This is OKCL Code*/
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.logintitlescreen);
        setTitle("Login Screen");
      /*  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

        // Set up the login form.
        context = this;

        //f3Cf88nmpfU:APA91bG5DasY55p8ofRryOvm8OfXyQYtViw3P4DMKNIx9t1-qiJQiO0uURx7NCJVSN5HtVgq8y3WKSYHV9RQc_RtGGx61S_RnvHKPMwQCIWDidRdGaCK0kJr1QFtc24j5-uP52uDmQjk
        adapter = new DBAdapter(context, DBAdapter.DATABASE_NAME, null, DBAdapter.DATABASE_VERSION, DBAdapter.db);
        gpsTracker = new GPSTracker(this);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        register = (Button) findViewById(R.id.goRegistration);
        forgotpassword = (TextView) findViewById(R.id.forgotpassword);
        adapter.addRoles();
        adapter.addRolesCategory();
        adapter.addzones();

        forgotpassword();

        /*try {
            if (isInternetOn()) {
                //if (userRole.equals("1") || userRole.equals("2") || userRole.equals("4") || userRole.equals("5") || userRole.equals("6") || userRole.equals("7") || userRole.equals("8") || userRole.equals("9") || userRole.equals("10") || userRole.equals("11") || userRole.equals("12") || userRole.equals("13") || userRole.equals("14") || userRole.equals("15") || userRole.equals("16") || userRole.equals("17") || userRole.equals("18") || userRole.equals("19"))
                new DownloadCountMaster(LoginActivity.this, context, handlerf).execute("");
            } else {
                Toast.makeText(getApplicationContext(), "Check your Internet connection ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        copySchoolDB();
        copySDT();

        userRole = adapter.getRole();
//        Toast.makeText(context, "Role"+new SharedValues(getApplicationContext()).loadSharedPreference_UserRole(), Toast.LENGTH_SHORT).show();
//        Toast.makeText(context, "role : "+userRole, Toast.LENGTH_SHORT).show();
        if (adapter.getstatus() != null) //session maintain
        {
            if (adapter.getstatus().equals("1"))
            {
                if(isInternetOn()) {
                    if (userRole.equals("8")) {
                        Intent intent = new Intent(LoginActivity.this, NewDashBoardGraphActivity.class);
                        intent.putExtra("fromLogin", false);
                        startActivity(intent);
                        finish();
                    } else if (userRole.equals("5")) {
                        Intent intent = new Intent(LoginActivity.this, DistrictLevelGraph.class);
                        intent.putExtra("fromLogin", false);
                        startActivity(intent);
                        finish();
                    } else if (userRole.equals("2")) {
                        String u;
                        String hm =  new SharedValues(getApplicationContext()).loadSharedPreference_MobileNo();
                        new DownloadUdiseByHMobile(LoginActivity.this,LoginActivity.this,hm.toString(),handler).execute("");
                    }
                }
                else
                {
                    //Toast.makeText(context, "Please check your internet connetion`", Toast.LENGTH_SHORT).show();
                    if(userRole.equals("8")) {
                        try {
                            Intent intent = new Intent(LoginActivity.this, NewDashBoardGraphActivity.class);
                            intent.putExtra("fromLogin", false);
                            startActivity(intent);
                            finish();
                        } catch (Exception err) {
                            Log.i("Error", "Show Error" + err);
                            Toast.makeText(getApplicationContext(), "Check your Internet connection " + err, Toast.LENGTH_SHORT).show();
                        }
                    }else if(userRole.equals("2")){
                        String u;
                        u = new SharedValues(getApplicationContext()).loadSharedPreference_getUdise();
                        Intent intent = new Intent(LoginActivity.this,SchoolDetails.class);
                        intent.putExtra("UDISE",u);
                        startActivity(intent);
                        finish();
                    }else if(userRole.equals("5")){
                        Intent intent = new Intent(LoginActivity.this, DistrictLevelGraph.class);
                        intent.putExtra("fromLogin", false);
                        startActivity(intent);
                        finish();
                    }
                }
            }
            else
            {

            }
        }

       uid = new SharedValues(getApplicationContext()).loadSharedPreference_UserRole();

//        Toast.makeText(context, "role : "+uid+"\nURole : "+userRole, Toast.LENGTH_SHORT).show();
        if (!getIntent().getBooleanExtra("fromDashboard",false)) {
            if (uid.equals("NA") || userRole == null) {
                new SharedValues(this).saveSharedPreference("userRole","NA");
                Intent intent = new Intent(LoginActivity.this, NewDashBoardGraphActivity.class);
                intent.putExtra("fromLogin", true);
                startActivity(intent);
                finish();
            }
        }/*/*else if (uid.equals("1") || uid.equals("2") || uid.eqFuals("4") || uid.equals("5") || uid.equals("6") || uid.equals("7") || userRole.equals("8") || uid.equals("9") || uid.equals("10") || uid.equals("11") || uid.equals("12") || uid.equals("13") || uid.equals("14") || uid.equals("15") || uid.equals("16") || uid.equals("17") || uid.equals("18") || uid.equals("19")) {
            Intent intent = new Intent(LoginActivity.this, DrawerForHigherAuthority.class);
            startActivity(intent);
            finish();
        }*/


        // adapter.addStatus();
        // adapter.addIssueCategory();
        //  startService(new Intent(LoginActivity.this, LocationService.class));
        mPasswordView = (EditText) findViewById(R.id.password);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    //  attemptLogin();
                    return true;
                }
                return false;
            }
        });
        getPermission();
        MultiDex.install(this);
        populateAutoComplete();
        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkNetworkConn()) {
                    attemptLogin();
                } else {
                    Toast.makeText(LoginActivity.this, "Check your network connection", Toast.LENGTH_LONG).show();
                }
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs
        Intent intent = new Intent(LoginActivity.this, NewDashBoardGraphActivity.class);
        startActivity(intent);
        finish();
    }
    private void forgotpassword()
    {
        forgotpassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setMessage("Enter your registered mobile number");
                final EditText mobileNumber = new EditText(context);
                mobileNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                mobileNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
                mobileNumber.setHint("10 digit mobile number only");
                alert.setView(mobileNumber);

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        String value = mobileNumber.getText().toString();
                        Log.d("Mobile number== ", value);

                        new ForgetPasswordWebservice(context, LoginActivity.this, value, passwordhandler).execute("");
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                {

                    public void onClick(DialogInterface dialog, int which)
                    {
                        return;
                    }
                });
                alert.show();
            }
        });
    }

    public boolean isInternetOn() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {
            return false;
        }
        return false;
    }

    private void populateAutoComplete()
    {
        if (!mayRequestContacts())
        {
            return;
        }
        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts()
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        if (shouldShowRequestPermissionRationale(READ_CONTACTS))
        {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new OnClickListener()
                    {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v)
                        {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else
        {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    private boolean checkNetworkConn() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        try
        {
            String abc = mEmailView.getText().toString();
            String pqr = mPasswordView.getText().toString();

            TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            strDevId = tManager.getDeviceId();

            new LoginValidationWS(LoginActivity.this, context, abc, pqr, strDevId, handler).execute();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        /*if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);

            Intent i = new Intent(LoginActivity.this, Drawer.class);
            startActivity(i);
        }*/
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 0;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }
        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown schoolInfoTableArrayList.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    return pieces[1].equals(mPassword);
                }
            }
            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    private void copySchoolDB() {
        File dbFile = new File(Environment.getDataDirectory() + "/data/"
                + getPackageName() + "/database/SchoolsDB");
        if (!dbFile.exists()) {
            Log.i("in", "in");
            new CopySchoolDbFile(getApplicationContext()).copySchoolDB();
        }
    }

    private void copySDT() {
        File dbFile = new File(Environment.getDataDirectory() + "/data/"
                + getPackageName() + "/database/SDT");
        if (!dbFile.exists()) {
            Log.i("in", "in");
            new CopySDTFile(getApplicationContext()).copyFile();
        }
    }

    public void getPermission() {
        // Verify that all required contact permissions have been granted.
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Contacts permissions have not been granted.

            if (
                    ActivityCompat.shouldShowRequestPermissionRationale(this,
                            android.Manifest.permission.READ_PHONE_STATE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(this,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            || ActivityCompat.shouldShowRequestPermissionRationale(this,
                            android.Manifest.permission.INTERNET)
                            || ActivityCompat.shouldShowRequestPermissionRationale(this,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(this,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Provide an additional rationale to the user if the permission was not granted
                // and the user would benefit from additional context for the use of the permission.
                // For example, if the request has been denied previously.

                ActivityCompat.requestPermissions(this, PERMISSIONS_CONTACT, REQUEST_CONTACTS);
            } else {
                // Contact permissions have not been granted yet. Request them directly.
                ActivityCompat.requestPermissions(this, PERMISSIONS_CONTACT, REQUEST_CONTACTS);
            }
        } else {
            // Contact permissions have been granted. Show the contacts fragment.
        }
    }


}