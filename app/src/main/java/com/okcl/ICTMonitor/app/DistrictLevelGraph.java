package com.okcl.ICTMonitor.app;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Highlight;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import Support.SharedValues;
import WebServices.DownloadDistBarchartIssue;
import WebServices.DownloadDistCount;
import WebServices.DownloadDistLevelTTCount;
import WebServices.DownloadDistLineChart;
import WebServices.DownloaddistBarchartSL;

public class DistrictLevelGraph extends AppCompatActivity {

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                if (msg.obj != null) {
                    new SharedValues(getApplicationContext()).saveSharedPreference("datewise",msg.obj.toString().trim());
                    drawLineGraph(msg.obj.toString());
                    new DownloaddistBarchartSL(DistrictLevelGraph.this, DistrictLevelGraph.this,DistId.toString().trim(), handler).execute("");
                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }
            }else  if (msg.what == 2) {
                if (msg.obj != null) {
                    new SharedValues(getApplicationContext()).saveSharedPreference("studentlearning",msg.obj.toString().trim());
                    drawBarGraphSL(msg.obj.toString());
                    new DownloadDistBarchartIssue(DistrictLevelGraph.this, DistrictLevelGraph.this,DistId.toString().trim(), handler).execute("");
                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }
            }else  if (msg.what == 3) {
                if (msg.obj != null) {
                    new SharedValues(getApplicationContext()).saveSharedPreference("issuestate",msg.obj.toString().trim());
                    drawBarGraphIssue(msg.obj.toString());
                    new DownloadDistCount(DistrictLevelGraph.this, DistrictLevelGraph.this,DistId.toString().trim(), handler).execute("");

                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }
            }else  if (msg.what == 4) {
                if (msg.obj != null) {
                    new SharedValues(getApplicationContext()).saveSharedPreference("DistCount",msg.obj.toString().trim());
                    showDistWiseCount(msg.obj.toString());
                    new DownloadDistLevelTTCount(DistrictLevelGraph.this, DistrictLevelGraph.this,DistName.toString().trim(), handler).execute("");
                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }
            }else if(msg.what == 5){
                if(msg.obj != null){
                    new SharedValues(getApplicationContext()).saveSharedPreference("enrollpassed",msg.obj.toString().trim());
                    //Toast.makeText(DistrictLevelGraph.this, "result :"+msg.obj.toString(), Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(msg.obj.toString());
                         TECount.setText(jsonObject.getString("TeacherEnrolled").replace("[", "").replace("]", "").replace("\"","").toString());
                         TPCount.setText(jsonObject.getString("TeacherPassed").replace("[", "").replace("]", "").replace("\"","").toString());
                         WormCount.setText(jsonObject.getString("WormUplod").replace("[", "").replace("]", "").replace("\"","").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }else{
                    Toast.makeText(DistrictLevelGraph.this, "Somthing is wrong", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
    LineChart DistLineChart;
    BarChart distBarchartSL,DistBarchartIssue;
    String DistId,DistName;
    DBAdapter database;
    TextView errorText,uCount,iCount,sCount,TECount,TPCount,WormCount;
    YAxis y,y1;
    XAxis x;
    ProgressDialog progressDialog;
    SwipeRefreshLayout swipeContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_district_level_graph);
//        new ToolbarConfigurer(DistrictLevelGraph.this, (Toolbar) findViewById(R.id.toolbar1), true);
        DistLineChart = (LineChart) findViewById(R.id.distDateWiseSchoolAttendance);
        distBarchartSL = (BarChart) findViewById(R.id.distStudentlearn);
        DistBarchartIssue = (BarChart) findViewById(R.id.distIssueStatus);
        errorText = (TextView) findViewById(R.id.offlineError);
        DistId = new SharedValues(getApplicationContext()).loadSharedPreference_DistrictId();
        database = new DBAdapter(getApplicationContext(), DBAdapter.DATABASE_NAME, null, DBAdapter.DATABASE_VERSION, DBAdapter.db);
        DistName = database.getDistName(DistId.trim());
        uCount = (TextView) findViewById(R.id.DistUniqueCount);
        iCount = (TextView) findViewById(R.id.DistInternetCount);
        sCount = (TextView) findViewById(R.id.DistSmsCount);
        TECount = (TextView) findViewById(R.id.countTeacherEnrolled);
        TPCount = (TextView) findViewById(R.id.countTeacherPassed);
        WormCount = (TextView) findViewById(R.id.countWorm);
        setTitle(DistName.trim()+" Dashboard");
        boolean islogin =  getIntent().getBooleanExtra("fromLogin",true);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
//        Toast.makeText(this, "Dist : "+DistName, Toast.LENGTH_SHORT).show();
        if(isInternetOn()){
            if(islogin){
                progressDialog = new ProgressDialog(this);
                progressDialog.setTitle("Please Wait...");
                progressDialog.setMessage("Genarating a graph please wait...");
                progressDialog.setCancelable(false);
                progressDialog.show();
                new DownloadDistLineChart(DistrictLevelGraph.this, DistrictLevelGraph.this,DistId.toString().trim(), handler).execute("");
            }else{
                String str ;
                str = new SharedValues(getApplicationContext()).loadSharedPreference_DateWise();
                drawLineGraph(str);
                str = new SharedValues(getApplicationContext()).loadSharedPreference_StudentLearning();
                drawBarGraphSL(str);
                str = new SharedValues(getApplicationContext()).loadSharedPreference_IssueState();
                drawBarGraphIssue(str);
                str = new SharedValues(getApplicationContext()).loadSharedPreference_GetDistCount();
                showDistWiseCount(str);
                str = new SharedValues(getApplicationContext()).loadSharedPreference_CountEnrollPassed();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(str);
                    TECount.setText(jsonObject.getString("TeacherEnrolled").replace("[", "").replace("]", "").replace("\"","").toString());
                    TPCount.setText(jsonObject.getString("TeacherPassed").replace("[", "").replace("]", "").replace("\"","").toString());
                    WormCount.setText(jsonObject.getString("WormUplod").replace("[", "").replace("]", "").replace("\"","").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else{
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "Your Offline, This is your last uploaded data", Toast.LENGTH_SHORT).show();
           // errorText.setVisibility(View.VISIBLE);
            String str ;
            str = new SharedValues(getApplicationContext()).loadSharedPreference_DateWise();
            drawLineGraph(str);
            str = new SharedValues(getApplicationContext()).loadSharedPreference_StudentLearning();
            drawBarGraphSL(str);
            str = new SharedValues(getApplicationContext()).loadSharedPreference_IssueState();
            drawBarGraphIssue(str);
            str = new SharedValues(getApplicationContext()).loadSharedPreference_GetDistCount();
            showDistWiseCount(str);
            str = new SharedValues(getApplicationContext()).loadSharedPreference_CountEnrollPassed();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(str);
                TECount.setText(jsonObject.getString("TeacherEnrolled").replace("[", "").replace("]", "").replace("\"","").toString());
                TPCount.setText(jsonObject.getString("TeacherPassed").replace("[", "").replace("]", "").replace("\"","").toString());
                WormCount.setText(jsonObject.getString("WormUplod").replace("[", "").replace("]", "").replace("\"","").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(isInternetOn()){
                    swipeContainer.setColorSchemeResources(R.color.unique,R.color.internet,R.color.sms,R.color.limb);
                    progressDialog = new ProgressDialog(DistrictLevelGraph.this);
                    progressDialog.setTitle("Please Wait...");
                    progressDialog.setMessage("Genarating a graph please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    new DownloadDistLineChart(DistrictLevelGraph.this, DistrictLevelGraph.this,DistId.toString().trim(), handler).execute("");
                }else{
                    Toast.makeText(DistrictLevelGraph.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                }

                swipeContainer.setRefreshing(false);

//                DistWiseGraph.this.overridePendingTransition(0, 0);
            }
        });



        TPCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DistrictLevelGraph.this, DistrictWiseSchoolInfo.class);
                intent.putExtra("dist", DistName);
                intent.putExtra("worm", false);
                intent.putExtra("label", "NA");
                startActivity(intent);
            }
        });
        TECount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DistrictLevelGraph.this, DistrictWiseSchoolInfo.class);
                intent.putExtra("dist", DistName);
                intent.putExtra("worm", false);
                intent.putExtra("label", "NA");
                startActivity(intent);
            }
        });
        WormCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DistrictLevelGraph.this, DistrictWiseSchoolInfo.class);
                intent.putExtra("dist", DistName);
                intent.putExtra("worm", true);
                intent.putExtra("label", "NA");
                startActivity(intent);
            }
        });
        DistLineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                try {
                    Intent intent = new Intent(DistrictLevelGraph.this, DistrictWiseSchoolInfo.class);
                    intent.putExtra("dist", DistName.trim());
                    intent.putExtra("worm", false);
                    intent.putExtra("label", "NA");
                    startActivity(intent);
                }catch (Exception err){
                    Toast.makeText(DistrictLevelGraph.this, "Somthing is wrong ", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onNothingSelected() {

            }
        });
        distBarchartSL.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                try {
                    Intent intent = new Intent(DistrictLevelGraph.this, DistrictWiseSchoolInfo.class);
                    intent.putExtra("dist", DistName.trim());
                    intent.putExtra("worm", false);
                    intent.putExtra("label", "NA");
                    startActivity(intent);
                }catch (Exception err){
                    Toast.makeText(DistrictLevelGraph.this, "Somthing is wrong ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected() {

            }
        });

        DistBarchartIssue.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                try {
                    Intent intent = new Intent(DistrictLevelGraph.this, DistrictWiseSchoolInfo.class);
                    intent.putExtra("dist", DistName.trim());
                    intent.putExtra("worm", false);
                    intent.putExtra("label", "NA");
                    startActivity(intent);
                }catch (Exception err){
                    Toast.makeText(DistrictLevelGraph.this, "Somthing is wrong ", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onNothingSelected() {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.item2);
        MenuItem itemlogout = menu.findItem(R.id.item3);
        itemlogout.setVisible(true);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.item1){
            Intent intent = new Intent(DistrictLevelGraph.this,DistrictLevelGraph.class);
            startActivity(intent);
            finish();
        }else  if(id == R.id.item3){
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Closing Activity")
                    .setMessage("Are you sure you want to Log Out?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            database.setStatusDeActive();

                            /*adapter.juniordelete(addjunoirbeen);
                            adapter.assigntabledelete(assignbeen);*/

                            startActivity(new Intent(DistrictLevelGraph.this, LoginActivity.class));

                            Toast.makeText(DistrictLevelGraph.this, "Logout Successfully", Toast.LENGTH_SHORT).show();
                            new SharedValues(getApplicationContext()).saveSharedPreference("userRole","NA");
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        }
//        onBackPressed();
        return true;
    }
    @Override
    public void onBackPressed() {
              new android.support.v7.app.AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Closing Activity")
                    .setMessage("Are you sure you want to Exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
    }

    public boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {

            return false;
        }
        return false;
    }

    public void drawLineGraph(String str){
        final ArrayList<Entry> data1 = new ArrayList<>();
        final ArrayList<Entry> data2 = new ArrayList<>();
        final ArrayList<Entry> data3 = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();
        int uniquecount = 0, internetcount = 0, smscount = 0;
        String[] data = new String[8];
        try {
            JSONObject jsonObject = new JSONObject(str);

            data = jsonObject.getString("Date").replace("[", "").replace("]", "").replace("\"","").split(",");

            for (int i = 0; i < data.length; i++) {
                labels.add((data[i]));
            }

            data = jsonObject.getString("UniqueAttendance").replace("[", "").replace("]", "").split(",");

            for (int i = 0; i < data.length; i++) {
                data1.add(new Entry(Integer.parseInt(data[i]), i));
                uniquecount = ((uniquecount) + (Integer.parseInt(data[i])));

            }
            LineDataSet UniqueAttendance = new LineDataSet(data1, "Unique");  // creating dataset for group1
//            UniqueAttendance.setLineWidth(5f);
            UniqueAttendance.setDrawFilled(true);
            UniqueAttendance.setColor(R.color.internet);
            UniqueAttendance.setDrawCircleHole(true);
            UniqueAttendance.setCircleColorHole(R.color.internet);
//            UniqueAttendance.setFillColor(R.color.internet);

            data = jsonObject.getString("InternetAttendance").replace("[", "").replace("]", "").split(",");

            for (int i = 0; i < data.length; i++) {
                data2.add(new Entry(Integer.parseInt(data[i]), i));
                internetcount = ((internetcount) + (Integer.parseInt(data[i])));
            }

            LineDataSet InternetAttendance = new LineDataSet(data2, "Internet");  // creating dataset for group1
            InternetAttendance.setColor(R.color.internet);
            InternetAttendance.setDrawFilled(true);
            InternetAttendance.setDrawCircleHole(true);
            InternetAttendance.setCircleColorHole(R.color.internet);
//            InternetAttendance.setFillColor(R.color.internet);

            data = jsonObject.getString("SMSAttendance").replace("[", "").replace("]", "").split(",");

            for (int i = 0; i < data.length; i++) {
                data3.add(new Entry(Integer.parseInt(data[i]), i));
                smscount = ((smscount) + (Integer.parseInt(data[i])));
            }

            LineDataSet SMSAttendance = new LineDataSet(data3, "Sms");  // creating dataset for group1
            SMSAttendance.setColor(R.color.internet);
            SMSAttendance.setDrawFilled(true);
            SMSAttendance.setDrawCircleHole(true);
            SMSAttendance.setCircleColorHole(R.color.internet);
//            SMSAttendance.setFillColor(R.color.internet);

            SMSAttendance.setDrawFilled(true);

            ArrayList<LineDataSet> dataSets = new ArrayList<>();
            dataSets.add(UniqueAttendance);
            dataSets.add(InternetAttendance);
            dataSets.add(SMSAttendance);


            y = DistLineChart.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = DistLineChart.getAxisRight();
            y1.setEnabled(false);

            DistLineChart.getAxisRight().setDrawGridLines(false);
            DistLineChart.getAxisLeft().setDrawGridLines(false);
            DistLineChart.getXAxis().setDrawGridLines(false);
            DistLineChart.setDescription("");

            LineData la = new LineData(labels, dataSets);
            DistLineChart.setData(la);
            DistLineChart.animateX(2500);

        } catch (JSONException e) {
            Toast.makeText(DistrictLevelGraph.this, "Something is wrong...", Toast.LENGTH_SHORT).show();
        }
    }


    public void drawBarGraphSL(String str){
        String string = "";
        String[] arr_10_std_count = new String[4];
        ArrayList<String> labels = new ArrayList<String>();
        final ArrayList<BarEntry> bargroup10 = new ArrayList<>();
        final ArrayList<BarEntry> bargroup9 = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(str);

//Tenth Std
            arr_10_std_count = jsonObject.getString("studcount10th").replace("[", "").replace("]", "").split(",");
            for(int i=0;i<arr_10_std_count.length;i++) {
                bargroup10.add(new BarEntry(Integer.parseInt(arr_10_std_count[i]), i));

            }

//Ninth Std
            arr_10_std_count = jsonObject.getString("studcount9th").replace("[", "").replace("]", "").split(",");
            for(int i = 0; i<arr_10_std_count.length;i++) {
                bargroup9.add(new BarEntry(Integer.parseInt(arr_10_std_count[i]), i));
            }


            BarDataSet barDataSet1 = new BarDataSet(bargroup10, "10th");  // creating dataset for group1
            barDataSet1.setColor(getResources().getColor(R.color.unique));

            BarDataSet barDataSet2 = new BarDataSet(bargroup9, "9th"); // creating dataset for group1
            barDataSet2.setColor(getResources().getColor(R.color.internet));

//Year
            arr_10_std_count = jsonObject.getString("Years").replace("[", "").replace("]", "").replace("\"","").split(",");
            for(int i=0;i<arr_10_std_count.length;i++) {
                if(arr_10_std_count[i].trim().equalsIgnoreCase("2015-2016")){
                    string = "15-16";
                }else if(arr_10_std_count[i].equalsIgnoreCase("2016-2017")){
                    string = "16-17";
                }else if(arr_10_std_count[i].equalsIgnoreCase("2017-2018")){
                    string = "17-18";
                }else if(arr_10_std_count[i].equalsIgnoreCase("2018-2019")){
                    string = "18-19";
                }
                labels.add(string);
            }

            ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
            dataSets.add(barDataSet2);
            dataSets.add(barDataSet1);

            y = distBarchartSL.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = distBarchartSL.getAxisRight();
            y1.setEnabled(false);

            distBarchartSL.getAxisRight().setDrawGridLines(false);
            distBarchartSL.getAxisLeft().setDrawGridLines(false);
            distBarchartSL.getXAxis().setDrawGridLines(false);
            distBarchartSL.setDescription("");


            BarData data = new BarData(labels, dataSets); // initialize the Bardata with argument labels and dataSet
            distBarchartSL.setData(data);
            distBarchartSL.animateY(2000);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void drawBarGraphIssue(String str){
        final ArrayList<BarEntry> bargroupoverdue = new ArrayList<>();
        final ArrayList<BarEntry> bargroupbyRaised = new ArrayList<>();
        final ArrayList<BarEntry> bargroupbyClosed = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();
        String[] issuestatus = new String[1];
        try {
            JSONObject jsonObject = new JSONObject(str);
//bargroupoverdue
            issuestatus = jsonObject.getString("overdue").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < issuestatus.length; i++) {
                bargroupoverdue.add(new BarEntry(Integer.parseInt(issuestatus[i]), i));
            }

//bargroupbyRaised
            issuestatus = jsonObject.getString("Raised").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < issuestatus.length; i++) {
                bargroupbyRaised.add(new BarEntry(Integer.parseInt(issuestatus[i]), i));
            }

//bargroupbyClosed
            issuestatus = jsonObject.getString("Closed").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < issuestatus.length; i++) {
                bargroupbyClosed.add(new BarEntry(Integer.parseInt(issuestatus[i]), i));
            }

//Dataset
            BarDataSet barDataSetoverdue = new BarDataSet(bargroupoverdue, "Overdue");  // creating dataset for group1
            barDataSetoverdue.setColor(getResources().getColor(R.color.red));

            BarDataSet barDataSetRaised = new BarDataSet(bargroupbyRaised, "Raised"); // creating dataset for group1
            barDataSetRaised.setColor(getResources().getColor(R.color.orange));

            BarDataSet barDataSetClosed = new BarDataSet(bargroupbyClosed, "Closed");  // creating dataset for group1
            barDataSetClosed.setColor(getResources().getColor(R.color.limb));

//Status
            issuestatus = jsonObject.getString("Status").replace("[", "").replace("]", "").replace("\"","").split(",");
            for (int i = 0; i < issuestatus.length; i++) {
                labels.add(issuestatus[i]);
            }

            ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
            dataSets.add(barDataSetRaised);
            dataSets.add(barDataSetClosed);
            dataSets.add(barDataSetoverdue);


            y = DistBarchartIssue.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = DistBarchartIssue.getAxisRight();
            y1.setEnabled(false);

            DistBarchartIssue.getAxisRight().setDrawGridLines(false);
            DistBarchartIssue.getAxisLeft().setDrawGridLines(false);
            DistBarchartIssue.getXAxis().setDrawGridLines(false);
            DistBarchartIssue.setDescription("");


            BarData data = new BarData(labels, dataSets); // initialize the Bardata with argument labels and dataSet
            DistBarchartIssue.setData(data);
            DistBarchartIssue.animateY(2000);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showDistWiseCount(String str){
        String[] getData = new String[1];
        String yourFormattedString;
        int count=0;
        try {
            JSONObject jsonObject = new JSONObject(str);
            getData = jsonObject.getString("UniqueAttendance").replace("[", "").replace("]", "").split(",");
            for(int i=0;i<getData.length;i++) {
                count = ((count) + (Integer.parseInt(getData[i])));
                uCount.setText(""+ NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count)));
            }

            count = 0;
            getData = jsonObject.getString("AttendanceByInternet").replace("[", "").replace("]", "").split(",");
            for(int i=0;i<getData.length;i++) {
                count = ((count) + (Integer.parseInt(getData[i])));
                iCount.setText(""+NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count)));
            }

            count = 0;
            getData = jsonObject.getString("AttendanceBySMS").replace("[", "").replace("]", "").split(",");
            for(int i=0;i<getData.length;i++) {
                count = ((count) + (Integer.parseInt(getData[i])));
                sCount.setText(""+NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count)));
            }

        }catch (JSONException err){
            err.printStackTrace();
        }
    }

/*    public static class ToolbarConfigurer implements View.OnClickListener {
        private Activity activity;

        public ToolbarConfigurer(Activity activity, Toolbar toolbar, boolean displayHomeAsUpEnabled) {
            toolbar.setTitle((this.activity = activity).getTitle());
            if (!displayHomeAsUpEnabled) return;
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
            toolbar.setNavigationOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(activity, "back", Toast.LENGTH_SHORT).show();
            NavUtils.navigateUpFromSameTask(activity);
        }
    }*/

}
