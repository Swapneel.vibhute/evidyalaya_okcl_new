package com.okcl.ICTMonitor.app;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Highlight;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import Support.SharedValues;
import WebServices.DownloadDailyCount;
import WebServices.DownloadDatewiseSchoolCoordinatorAttendance;
import WebServices.DownloadGetTeacherEnrolledPassed;
import WebServices.DownloadIssueStatus;
import WebServices.DownloadZoneWiseSchoolCoordinatorAttendance;
import WebServices.DownloadZonewiseWORMUploadedstatus;
import WebServices.DownloadbarStudentLearningyear;

/*
*
*   Dashboard to view Date-wise SC Attendance, Student Learning,
*   Issue Status and Worm Upload Dashboard.
*   This activity will shows follwoing graphs :
*   1. Date-wise SC Attendence.
*   2. Student Learning.
*   3. Issue Status.
*   4. Zone-wise WORM Uploaded status.
*
*/

public class NewDashBoardGraphActivity extends AppCompatActivity {

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                if (msg.obj != null) {
                    new SharedValues(getApplicationContext()).saveSharedPreference("studentlearning", msg.obj.toString().trim());
                    drawStudentLearning(msg.obj.toString());
                    new DownloadZoneWiseSchoolCoordinatorAttendance(NewDashBoardGraphActivity.this, NewDashBoardGraphActivity.this, handler).execute("");
                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }

            } else if (msg.what == 2) {
                if (msg.obj != null) {
/*                    new SharedValues(getApplicationContext()).saveSharedPreference("zonewise",msg.obj.toString().trim());
                    drawZoneWiseSCAttendance(msg.obj.toString());*/
                    new SharedValues(getApplicationContext()).saveSharedPreference("studentlearning", msg.obj.toString().trim());
                    drawStudentLearning(msg.obj.toString());
                    new DownloadIssueStatus(NewDashBoardGraphActivity.this, NewDashBoardGraphActivity.this, handler).execute("");
                } else if (msg.what == 105) {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }

            } else if (msg.what == 3) {
                if (msg.obj != null) {
                    new SharedValues(getApplicationContext()).saveSharedPreference("issuestate", msg.obj.toString().trim());
                    drawIssueState(msg.obj.toString());
                    new DownloadZonewiseWORMUploadedstatus(NewDashBoardGraphActivity.this, NewDashBoardGraphActivity.this, handler).execute("");
                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }

            } else if (msg.what == 4) {
                if (msg.obj != null) {
                    new SharedValues(getApplicationContext()).saveSharedPreference("wormupload", msg.obj.toString().trim());
                    drawZoneWiseWormUpload(msg.obj.toString());
                    new DownloadDatewiseSchoolCoordinatorAttendance(NewDashBoardGraphActivity.this, NewDashBoardGraphActivity.this, handler).execute("");
                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }

            } else if (msg.what == 5) {
                if (msg.obj != null) {
                    new SharedValues(getApplicationContext()).saveSharedPreference("datewise", msg.obj.toString().trim());
                    drawDateWiseSCAttendance(msg.obj.toString());
                    new DownloadGetTeacherEnrolledPassed(NewDashBoardGraphActivity.this, NewDashBoardGraphActivity.this, handler).execute("");
                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }

            } else if (msg.what == 6) {
                if (msg.obj != null) {
                    new SharedValues(getApplicationContext()).saveSharedPreference("enrollpassed", msg.obj.toString().trim());
                    countTETP(msg.obj.toString());
                    new DownloadDailyCount(NewDashBoardGraphActivity.this, NewDashBoardGraphActivity.this, handler).execute("");
                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }
            } else if (msg.what == 7) {
                if (msg.obj != null) {
                    String str = msg.obj.toString();
                    new SharedValues(getApplicationContext()).saveSharedPreference("dailyCount", str);
                    showDailyCount(str);
                    dialog.dismiss();
                }
            }
        }
    };

    BarChart barChart, barChart1, barChart2, barChart3;
    LineChart linechart;
    int tenthCount = 0, ninthCount = 0;
    TextView ninthStd, tenthStd, uCount, iCount, sCount, countTE, countTP, offlineErr;
    String role, distId, mobile;
    Button clickHere;
    String flag;
    ProgressDialog dialog;
    YAxis y;
    YAxis y1;
    DecimalFormat formatter = new DecimalFormat("#,##,###,####");
    DBAdapter adapter;
    ScrollView scroll;
    SwipeRefreshLayout swipeContainer;
    @Override
    public void onBackPressed() {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Activity")
                .setMessage("Are you sure you want to Exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.item2);
        MenuItem itemlogout = menu.findItem(R.id.item3);
        if (role.equals("NA")) {
            item.setVisible(true);
        } else if (role.equals("2") || role.equals("5") || role.equals("8")) {
            item.setVisible(false);
            itemlogout.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.item2) {
            Intent intent = new Intent(NewDashBoardGraphActivity.this, LoginActivity.class);
            intent.putExtra("fromDashboard", true);
            startActivity(intent);
            finish();
        } else if (id == R.id.item1) {
            Intent intent = new Intent(NewDashBoardGraphActivity.this, NewDashBoardGraphActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.item3) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Closing Activity")
                    .setMessage("Are you sure you want to Log Out?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            adapter.setStatusDeActive();

                            /*adapter.juniordelete(addjunoirbeen);
                            adapter.assigntabledelete(assignbeen);*/

                            Intent intent = new Intent(NewDashBoardGraphActivity.this, LoginActivity.class);
                            intent.putExtra("fromLogin",false);
                            startActivity(intent);

                            Toast.makeText(NewDashBoardGraphActivity.this, "Logout Successfully", Toast.LENGTH_SHORT).show();
                            new SharedValues(getApplicationContext()).saveSharedPreference("userRole", "NA");
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        }
        return true;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_dash_board_graph);
//        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
/*        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.findViewById(R.id.internetBox).getLayoutParams();
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            layoutParams.setMargins(200, 0, 0, 0);
        } else {
            layoutParams.setMargins(80, 0, 0, 0);
        }*/

        /*scroll=(ScrollView)findViewById(R.id.ScrollView);
        scroll.smoothScrollTo(100,100);*/

        adapter = new DBAdapter(getApplicationContext(), DBAdapter.DATABASE_NAME, null, DBAdapter.DATABASE_VERSION, DBAdapter.db);


//        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

        barChart = (BarChart) findViewById(R.id.Studentlearn);
        barChart1 = (BarChart) findViewById(R.id.ZoneWiseSchoolAttendance);
        barChart2 = (BarChart) findViewById(R.id.IssueStatus);
        barChart3 = (BarChart) findViewById(R.id.ZoneWiseWormUpload);
        linechart = (LineChart) findViewById(R.id.DateWiseSchoolAttendance);
        ninthStd = (TextView) findViewById(R.id.ninthCount);
        tenthStd = (TextView) findViewById(R.id.tenthcount);
        uCount = (TextView) findViewById(R.id.UniqueCount);
        iCount = (TextView) findViewById(R.id.InternetCount);
        sCount = (TextView) findViewById(R.id.SmsCount);
        countTE = (TextView) findViewById(R.id.countTeacherEnrolled);
        countTP = (TextView) findViewById(R.id.countTeacherPassed);
        offlineErr = (TextView) findViewById(R.id.offlineError);
        SharedValues sv = new SharedValues(this);
        role = sv.loadSharedPreference_UserRole();
        distId = sv.loadSharedPreference_DistrictId();
        boolean islogin =  getIntent().getBooleanExtra("fromLogin",true);
        clickHere = (Button) findViewById(R.id.clickHere);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);


        //Toast.makeText(this, "Role "+role, Toast.LENGTH_SHORT).show();
/*        new SharedValues(getApplicationContext()).saveSharedPreference("flag","");
        flag = new SharedValues(getApplicationContext()).loadSharedPreference_getFlagStatus().trim();
        Toast.makeText(this, "Mobile :"+flag, Toast.LENGTH_SHORT).show();*/

    /*    try{
                Intent i = getIntent();
                flag = i.getStringExtra("flag").toString();
            Toast.makeText(this, "Flag : "+flag, Toast.LENGTH_SHORT).show();
                if(flag.equals(null)){
                    flag = "false";
                    Toast.makeText(this, "Flag : "+flag, Toast.LENGTH_SHORT).show();
                }else{
                    flag = "true";
                    Toast.makeText(this, "Flag : "+flag, Toast.LENGTH_SHORT).show();
                }

        }catch (Exception er){
            Toast.makeText(this, "Erro : "+er, Toast.LENGTH_SHORT).show();
        }*/
        // Toast.makeText(this, "Role : "+role, Toast.LENGTH_SHORT).show();
            if(role.equals("NA")){
                clickHere.setEnabled(false);
            }else if(role.equals("2") || role.equals("5") || role.equals("8")){
                clickHere.setEnabled(true);
            }
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(isInternetOn()){
                    dialog = new ProgressDialog(NewDashBoardGraphActivity.this);
                    dialog.setMessage("Genarating a graph please wait...");
                    dialog.setIndeterminate(false);
                    dialog.setCancelable(false);
                    dialog.show();
                    new DownloadbarStudentLearningyear(NewDashBoardGraphActivity.this, NewDashBoardGraphActivity.this, handler).execute("");
                    swipeContainer.setColorSchemeResources(R.color.unique);
                    swipeContainer.setRefreshing(false);
                }else{
                    Toast.makeText(NewDashBoardGraphActivity.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                    swipeContainer.setRefreshing(false);
                }
            }
        });
        if(isInternetOn()){
            if(islogin){
                dialog = new ProgressDialog(this);
                dialog.setMessage("Genarating a graph please wait...");
                dialog.setIndeterminate(false);
                dialog.setCancelable(false);
                dialog.show();
                new DownloadbarStudentLearningyear(NewDashBoardGraphActivity.this, NewDashBoardGraphActivity.this, handler).execute("");

            }else{
                String str;
                str = sv.loadSharedPreference_DateWise();
                drawDateWiseSCAttendance(str);
//            str = sv.loadSharedPreference_ZoneWise();
//            drawZoneWiseSCAttendance(str);
                str = sv.loadSharedPreference_StudentLearning();
                drawStudentLearning(str);
                str = sv.loadSharedPreference_IssueState();
                drawIssueState(str);
                str = sv.loadSharedPreference_WormUpload();
                drawZoneWiseWormUpload(str);
                str = sv.loadSharedPreference_CountEnrollPassed();
                countTETP(str);
                str = sv.loadSharedPreference_dailyCount();
                showDailyCount(str);
            }
        }else{
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "Your Offline, This is your last uploaded data", Toast.LENGTH_SHORT).show();
            String str;
            str = sv.loadSharedPreference_DateWise();
            drawDateWiseSCAttendance(str);
//            str = sv.loadSharedPreference_ZoneWise();
//            drawZoneWiseSCAttendance(str);
            str = sv.loadSharedPreference_StudentLearning();
            drawStudentLearning(str);
            str = sv.loadSharedPreference_IssueState();
            drawIssueState(str);
            str = sv.loadSharedPreference_WormUpload();
            drawZoneWiseWormUpload(str);
            str = sv.loadSharedPreference_CountEnrollPassed();
            countTETP(str);
            str = sv.loadSharedPreference_dailyCount();
            showDailyCount(str);
        }
        clickHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInternetOn()){
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://evidyalaya.mkcl.org/login/loginpage"));
                    startActivity(browserIntent);
                }else{
                    Toast.makeText(NewDashBoardGraphActivity.this, "please check your interet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
      /*  if (isInternetOn()) {
            dialog = new ProgressDialog(this);
            dialog.setMessage("Genarating a graph please wait...");
            dialog.setIndeterminate(false);
            dialog.setCancelable(false);
            dialog.show();
            new DownloadbarStudentLearningyear(NewDashBoardGraphActivity.this, NewDashBoardGraphActivity.this, handler).execute("");
        } else {
            dialog = new ProgressDialog(this);
            dialog.setMessage("Genarating a graph please wait...");
            dialog.setIndeterminate(false);
            dialog.setCancelable(false);
            dialog.show();
            // offlineErr.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Your Offline, This is your last uploaded data", Toast.LENGTH_SHORT).show();
            String str;
            str = sv.loadSharedPreference_DateWise();
            drawDateWiseSCAttendance(str);
//            str = sv.loadSharedPreference_ZoneWise();
//            drawZoneWiseSCAttendance(str);
            str = sv.loadSharedPreference_StudentLearning();
            drawStudentLearning(str);
            str = sv.loadSharedPreference_IssueState();
            drawIssueState(str);
            str = sv.loadSharedPreference_WormUpload();
            drawZoneWiseWormUpload(str);
            str = sv.loadSharedPreference_CountEnrollPassed();
            countTETP(str);
            str = sv.loadSharedPreference_dailyCount();
            showDailyCount(str);
            dialog.dismiss();
        }*/
    }

    public boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {

            return false;
        }
        return false;
    }

    private void parseXML(XmlPullParser parser) throws XmlPullParserException, IOException, SQLException {

        int eventType = parser.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String name = null;

            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    name = parser.getName();
                    Log.i(" in Signup", name);

                    break;
                case XmlPullParser.START_TAG: {

                    name = parser.getName();
                    Log.i("Error in ", name);

                }
                break;
                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    Log.i("Error  Signup", name);
                    break;
            }
            eventType = parser.next();
        }
    }

    public void drawStudentLearning(String str) {
        String string = "";
        String[] arr_10_std_count = new String[4];
        ArrayList<String> labels = new ArrayList<String>();
        final ArrayList<BarEntry> bargroup10 = new ArrayList<>();
        final ArrayList<BarEntry> bargroup9 = new ArrayList<>();
        tenthCount = 0;
        ninthCount = 0;
        try {
            JSONObject jsonObject = new JSONObject(str);

//Tenth Std
            arr_10_std_count = jsonObject.getString("studcount10th")
                    .replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < arr_10_std_count.length; i++) {
                bargroup10.add(new BarEntry(Integer.parseInt(arr_10_std_count[i]), i));
                tenthCount = tenthCount + ((Integer.parseInt(arr_10_std_count[i])));
                Log.i("TenthCount", String.valueOf(tenthCount));
            }


//Ninth Std
            arr_10_std_count = jsonObject.getString("studcount9th")
                    .replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < arr_10_std_count.length; i++) {
                bargroup9.add(new BarEntry(Integer.parseInt(arr_10_std_count[i]), i));
                ninthCount = ninthCount + ((Integer.parseInt(arr_10_std_count[i])));
            }


            BarDataSet barDataSet1 = new BarDataSet(bargroup10, "10th");  // creating dataset for group1
            barDataSet1.setColor(getResources().getColor(R.color.unique));

            BarDataSet barDataSet2 = new BarDataSet(bargroup9, "9th"); // creating dataset for group1
            barDataSet2.setColor(getResources().getColor(R.color.internet));

//Year
            arr_10_std_count = jsonObject.getString("Years").replace("[", "").replace("]", "").replace("\"", "").split(",");
            for (int i = 0; i < arr_10_std_count.length; i++) {
                if (arr_10_std_count[i].trim().equalsIgnoreCase("2015-2016")) {
                    string = "15-16";
                } else if (arr_10_std_count[i].equalsIgnoreCase("2016-2017")) {
                    string = "16-17";
                } else if (arr_10_std_count[i].equalsIgnoreCase("2017-2018")) {
                    string = "17-18";
                } else if (arr_10_std_count[i].equalsIgnoreCase("2018-2019")) {
                    string = "18-19";
                }
                labels.add(string);
            }

            ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
            dataSets.add(barDataSet2);
            dataSets.add(barDataSet1);


            BarData data = new BarData(labels,  dataSets); // initialize the Bardata with argument labels and dataSet
            barChart.setData(data);

            barChart.setDescription("");  // set the description

            barChart.animateY(2000);
            y = barChart.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            barChart.getAxisRight().setDrawGridLines(false);
            barChart.getAxisLeft().setDrawGridLines(false);
            barChart.getXAxis().setDrawGridLines(false);

            y1 = barChart.getAxisRight();
            y1.setEnabled(false);

//            String yourFormattedString;
//            yourFormattedString = formatter.format(ninthCount);
            ninthStd.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in"))
                    .format(new BigDecimal(ninthCount))));

//            yourFormattedString = formatter.format(tenthCount);
            tenthStd.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in"))
                    .format(new BigDecimal(tenthCount))));


            barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                    if (role.equals("NA")) {
                        barChart.setTouchEnabled(false);
                    } else {
//                        barChart.setTouchEnabled(true);
                        String barValue = barChart.getBarData().getXVals().get(e.getXIndex());
                        if (isInternetOn()) {
                            //Toast.makeText(NewDashBoardGraphActivity.this, "Entry "+e.getVal()+"\nDataset "+dataSetIndex+"\nHighlight "+h.getDataSetIndex(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(NewDashBoardGraphActivity.this, DistWiseGraph.class);
                            intent.putExtra("zone", barValue.trim());
                            intent.putExtra("isBarchart2", "false");
                            intent.putExtra("lineBar", "false");
                            intent.putExtra("isBarchart", "true");
                            intent.putExtra("isbarchart1", "false");
                            intent.putExtra("isBarchart3", "false");
                            intent.putExtra("TT", "false");
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(NewDashBoardGraphActivity.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                            try {
                                Intent intent = new Intent(NewDashBoardGraphActivity.this, DistWiseGraph.class);
                                intent.putExtra("zone", barValue.trim());
                                intent.putExtra("isBarchart2", "false");
                                intent.putExtra("lineBar", "false");
                                intent.putExtra("isBarchart", "true");
                                intent.putExtra("isbarchart1", "false");
                                intent.putExtra("isBarchart3", "false");
                                intent.putExtra("TT", "false");
                                startActivity(intent);
                                finish();
                            } catch (Exception err) {
                                Log.i("Error", "Show Error" + err);
                                Toast.makeText(getApplicationContext(), "Check your Internet connection " + err, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected() {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void drawDateWiseSCAttendance(String str) {
        final ArrayList<Entry> data1 = new ArrayList<>();
        final ArrayList<Entry> data2 = new ArrayList<>();
        final ArrayList<Entry> data3 = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();
        String[] data = new String[8];
        try {
            JSONObject jsonObject = new JSONObject(str);

            data = jsonObject.getString("Date").replace("[", "").replace("]", "").replace("\"", "").split(",");

            for (int i = 0; i < data.length; i++) {
                labels.add((data[i]));
            }

            data = jsonObject.getString("UniqueAttendance").replace("[", "").replace("]", "").split(",");

            for (int i = 0; i < data.length; i++) {
                data1.add(new Entry(Integer.parseInt(data[i]), i));

            }
            LineDataSet UniqueAttendance = new LineDataSet(data1, "Unique");  // creating dataset for group1
            UniqueAttendance.setColor(getResources().getColor(R.color.unique));

//            UniqueAttendance.setLineWidth(5f);
            UniqueAttendance.setDrawFilled(true);
            UniqueAttendance.setColor(R.color.unique);
            UniqueAttendance.setDrawCircleHole(true);
            UniqueAttendance.setCircleColorHole(R.color.unique);
//            UniqueAttendance.setFillColor(R.color.internet);

            data = jsonObject.getString("InternetAttendance").replace("[", "").replace("]", "").split(",");

            for (int i = 0; i < data.length; i++) {
                data2.add(new Entry(Integer.parseInt(data[i]), i));
            }

            LineDataSet InternetAttendance = new LineDataSet(data2, "Internet");  // creating dataset for group1
            InternetAttendance.setColor(getResources().getColor(R.color.internet));

            InternetAttendance.setColor(R.color.internet);
            InternetAttendance.setDrawFilled(true);
            InternetAttendance.setDrawCircleHole(true);
            InternetAttendance.setCircleColorHole(R.color.internet);

            data = jsonObject.getString("SMSAttendance").replace("[", "").replace("]", "").split(",");

            for (int i = 0; i < data.length; i++) {
                data3.add(new Entry(Integer.parseInt(data[i]), i));
            }

            LineDataSet SMSAttendance = new LineDataSet(data3, "Sms");  // creating dataset for group1
            SMSAttendance.setColor(getResources().getColor(R.color.sms));

            SMSAttendance.setColor(R.color.sms);
            SMSAttendance.setDrawFilled(true);
            SMSAttendance.setDrawCircleHole(true);
            SMSAttendance.setCircleColorHole(R.color.sms);

            SMSAttendance.setDrawFilled(true);

            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(UniqueAttendance);
            dataSets.add(InternetAttendance);
            dataSets.add(SMSAttendance);

            LineData la = new LineData(labels, dataSets);
            linechart.setData(la);
            linechart.animateX(2500);

            y = linechart.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);
            XAxis x = linechart.getXAxis();

            y1 = linechart.getAxisRight();
            y1.setEnabled(false);

            linechart.getAxisRight().setDrawGridLines(false);
            linechart.getAxisLeft().setDrawGridLines(false);
            linechart.getXAxis().setDrawGridLines(false);
            linechart.setDescription("");


            linechart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                    if (role.equals("NA")) {
                        linechart.setTouchEnabled(false);
                    } else {
//                        linechart.setTouchEnabled(true);
                        String barValue = linechart.getLineData().getXVals().get(e.getXIndex());
                        if (isInternetOn()) {
                            Intent intent = new Intent(NewDashBoardGraphActivity.this, DistWiseGraph.class);
                            intent.putExtra("zone", barValue.trim());
                            intent.putExtra("lineBar", "true");
                            intent.putExtra("isbarchart1", "false");
                            intent.putExtra("isBarchart", "false");
                            intent.putExtra("isBarchart2", "false");
                            intent.putExtra("isBarchart3", "false");
                            intent.putExtra("TT", "false");
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(NewDashBoardGraphActivity.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                            try {
                                Intent intent = new Intent(NewDashBoardGraphActivity.this, DistWiseGraph.class);
                                intent.putExtra("zone", barValue.trim());
                                intent.putExtra("lineBar", "true");
                                intent.putExtra("isbarchart1", "false");
                                intent.putExtra("isBarchart2", "false");
                                intent.putExtra("isBarchart", "false");
                                intent.putExtra("isBarchart3", "false");
                                intent.putExtra("TT", "false");
                                startActivity(intent);
                                finish();
                            } catch (Exception err) {
                                Log.i("Error", "Show Error" + err);
                                Toast.makeText(getApplicationContext(), "Check your Internet connection " + err, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected() {

                }
            });

        } catch (JSONException e) {
            Toast.makeText(NewDashBoardGraphActivity.this, "Something is wrong...", Toast.LENGTH_SHORT).show();
        }
    }

    public void drawZoneWiseSCAttendance(String str) {
        final ArrayList<BarEntry> bargroupbysms = new ArrayList<>();
        final ArrayList<BarEntry> bargroupbyunique = new ArrayList<>();
        final ArrayList<BarEntry> bargroupbyinternet = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();
        String[] attendance = new String[6];
        String s[] = new String[2];
        try {
            JSONObject jsonObject = new JSONObject(str);
//by SMS
            attendance = jsonObject.getString("SMSAttendanceZone").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < attendance.length; i++) {
                bargroupbysms.add(new BarEntry(Integer.parseInt(attendance[i]), i));

            }

//by Internet
            attendance = jsonObject.getString("InternetAttendanceZone").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < attendance.length; i++) {
                bargroupbyinternet.add(new BarEntry(Integer.parseInt(attendance[i]), i));
            }

//by unique
            attendance = jsonObject.getString("UniqueAttendanceZone").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < attendance.length; i++) {
                bargroupbyunique.add(new BarEntry(Integer.parseInt(attendance[i]), i));
            }

//Dataset
            BarDataSet barDataSetSMS = new BarDataSet(bargroupbysms, "Sms");  // creating dataset for group1
            barDataSetSMS.setColor(getResources().getColor(R.color.sms));

            BarDataSet barDataSetInternet = new BarDataSet(bargroupbyinternet, "Internet"); // creating dataset for group1
            barDataSetInternet.setColor(getResources().getColor(R.color.internet));

            BarDataSet barDataSetUbique = new BarDataSet(bargroupbyunique, "Unique");  // creating dataset for group1
            barDataSetUbique.setColor(getResources().getColor(R.color.unique));

//Zone
            attendance = jsonObject.getString("Zone").replace("[", "").replace("]", "").replace("\"", "").split(",");
            for (int i = 0; i < attendance.length; i++) {
                s = attendance[i].split(" ");
                labels.add(s[1]);
            }

            ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
            dataSets.add(barDataSetUbique);
            dataSets.add(barDataSetInternet);
            dataSets.add(barDataSetSMS);



            BarData data = new BarData(labels, dataSets); // initialize the Bardata with argument labels and dataSet
            barChart1.setData(data);

            //barChart1.setDescription("Set Bar Chart Description");  // set the description

            barChart1.animateY(2000);
            y = barChart1.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = barChart1.getAxisRight();
            y1.setEnabled(false);

            barChart1.getAxisRight().setDrawGridLines(false);
            barChart1.getAxisLeft().setDrawGridLines(false);
            barChart1.getXAxis().setDrawGridLines(false);
            barChart1.setDescription("");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void drawIssueState(String str) {
        final ArrayList<BarEntry> bargroupoverdue = new ArrayList<>();
        final ArrayList<BarEntry> bargroupbyRaised = new ArrayList<>();
        final ArrayList<BarEntry> bargroupbyClosed = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();
        String[] issuestatus = new String[1];
        try {
            JSONObject jsonObject = new JSONObject(str);
//bargroupoverdue
            issuestatus = jsonObject.getString("overdue").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < issuestatus.length; i++) {
                bargroupoverdue.add(new BarEntry(Integer.parseInt(issuestatus[i]), i));
            }

//bargroupbyRaised
            issuestatus = jsonObject.getString("Raised").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < issuestatus.length; i++) {
                bargroupbyRaised.add(new BarEntry(Integer.parseInt(issuestatus[i]), i));
            }

//bargroupbyClosed
            issuestatus = jsonObject.getString("Closed").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < issuestatus.length; i++) {
                bargroupbyClosed.add(new BarEntry(Integer.parseInt(issuestatus[i]), i));
            }

//Dataset
            BarDataSet barDataSetoverdue = new BarDataSet(bargroupoverdue, "Overdue");  // creating dataset for group1
            barDataSetoverdue.setColor(getResources().getColor(R.color.RED));

            BarDataSet barDataSetRaised = new BarDataSet(bargroupbyRaised, "Raised"); // creating dataset for group1
            barDataSetRaised.setColor(getResources().getColor(R.color.orange));

            BarDataSet barDataSetClosed = new BarDataSet(bargroupbyClosed, "Closed");  // creating dataset for group1
            barDataSetClosed.setColor(getResources().getColor(R.color.limb));

//Status
            issuestatus = jsonObject.getString("Status").replace("[", "").replace("]", "").replace("\"", "").split(",");
            for (int i = 0; i < issuestatus.length; i++) {
                labels.add(issuestatus[i]);
            }

            ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
            dataSets.add(barDataSetRaised);
            dataSets.add(barDataSetClosed);
            dataSets.add(barDataSetoverdue);



            BarData data = new BarData(labels, dataSets); // initialize the Bardata with argument labels and dataSet
            barChart2.setData(data);


            barChart2.animateY(2000);
            y = barChart2.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = barChart2.getAxisRight();
            y1.setEnabled(false);

            barChart2.getAxisRight().setDrawGridLines(false);
            barChart2.getAxisLeft().setDrawGridLines(false);
            barChart2.getXAxis().setDrawGridLines(false);

            barChart2.setDescription("");  // set the description

            barChart2.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                    if (role.equals("NA")) {
                        barChart2.setTouchEnabled(false);
                    } else {
//                        barChart2.setTouchEnabled(true);
                        String barValue = barChart2.getBarData().getXVals().get(e.getXIndex());
                        if (isInternetOn()) {
                            //Toast.makeText(NewDashBoardGraphActivity.this, "Entry "+e.getVal()+"\nDataset "+dataSetIndex+"\nHighlight "+h.getDataSetIndex(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(NewDashBoardGraphActivity.this, DistWiseGraph.class);
                            intent.putExtra("zone", barValue.trim());
                            intent.putExtra("isBarchart2", "true");
                            intent.putExtra("lineBar", "false");
                            intent.putExtra("isBarchart", "false");
                            intent.putExtra("isbarchart1", "false");
                            intent.putExtra("isBarchart3", "false");
                            intent.putExtra("TT", "false");
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(NewDashBoardGraphActivity.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                            try {
                                Intent intent = new Intent(NewDashBoardGraphActivity.this, DistWiseGraph.class);
                                intent.putExtra("zone", barValue.trim());
                                intent.putExtra("isBarchart2", "true");
                                intent.putExtra("lineBar", "false");
                                intent.putExtra("isBarchart", "false");
                                intent.putExtra("isbarchart1", "false");
                                intent.putExtra("isBarchart3", "false");
                                intent.putExtra("TT", "false");
                                startActivity(intent);
                                finish();
                            } catch (Exception err) {
                                Log.i("Error", "Show Error" + err);
                                Toast.makeText(getApplicationContext(), "Check your Internet connection " + err, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected() {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void drawZoneWiseWormUpload(String str) {
        final ArrayList<BarEntry> bargroupzonecount = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();
        String[] zonecount = new String[6];
        String s[] = new String[2];
        try {
            JSONObject jsonObject = new JSONObject(str);

//bargroupzonecount
            zonecount = jsonObject.getString("ZoneCount").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < zonecount.length; i++) {
                bargroupzonecount.add(new BarEntry(Integer.parseInt(zonecount[i]), i));
            }

//Dataset
            BarDataSet barDataSetzonecount = new BarDataSet(bargroupzonecount, "Zone Count");  // creating dataset for group1
            barDataSetzonecount.setColor(getResources().getColor(R.color.internet));

//bargroupzone
            zonecount = jsonObject.getString("Zone").replace("[", "").replace("]", "").replace("\"", "").split(",");
            for (int i = 0; i < zonecount.length; i++) {
                s = zonecount[i].toString().trim().split(" ");
                labels.add(s[1]);
            }

            ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
            dataSets.add(barDataSetzonecount);



            BarData data = new BarData(labels, dataSets); // initialize the Bardata with argument labels and dataSet
            barChart3.setData(data);

            barChart3.setDescription("");  // set the description

            barChart3.animateY(2000);

            y = barChart3.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = barChart3.getAxisRight();
            y1.setEnabled(false);

            barChart3.getAxisRight().setDrawGridLines(false);
            barChart3.getAxisLeft().setDrawGridLines(false);
            barChart3.getXAxis().setDrawGridLines(false);


            barChart3.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                    if (role.equals("NA")) {
                        barChart3.setTouchEnabled(false);
/*                    Intent intent = new Intent(NewDashBoardGraphActivity.this,LoginActivity.class);
                    intent.putExtra("fromDashboard",true);
                    startActivity(intent);
                    finish();*/
                    } else {
//                        barChart3.setTouchEnabled(true);
                        String barValue = barChart3.getBarData().getXVals().get(e.getXIndex());
                        if (isInternetOn()) {
                            Intent intent = new Intent(NewDashBoardGraphActivity.this, DistWiseGraph.class);
                            intent.putExtra("zone", "Zone " + barValue.trim());
                            intent.putExtra("lineBar", "false");
                            intent.putExtra("isBarchart2", "false");
                            intent.putExtra("isBarchart3", "true");
                            intent.putExtra("isBarchart", "false");
                            intent.putExtra("isBarchart1", "false");
                            intent.putExtra("TT", "false");
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(NewDashBoardGraphActivity.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                            try {
                                Intent intent = new Intent(NewDashBoardGraphActivity.this, DistWiseGraph.class);
                                intent.putExtra("zone", barValue.trim());
                                intent.putExtra("lineBar", "false");
                                intent.putExtra("isBarchart2", "false");
                                intent.putExtra("isBarchart3", "true");
                                intent.putExtra("isBarchart", "false");
                                intent.putExtra("isBarchart1", "false");
                                intent.putExtra("TT", "false");
                                startActivity(intent);
                                finish();
                            } catch (Exception err) {
                                Log.i("Error", "Show Error" + err);
                                Toast.makeText(getApplicationContext(), "Check your Internet connection " + err, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected() {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void countTETP(String str) {
        String[] TeacherCount = new String[1];
        try {
            String yourFormattedString;
            JSONObject jsonObject = new JSONObject(str);

            TeacherCount = jsonObject.getString("Enrolled").replace("[", "").replace("]", "").split(",");
            //   yourFormattedString = formatter.format(Integer.parseInt(TeacherCount[0]));
            countTE.setText(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(Integer.parseInt(TeacherCount[0]))));

            TeacherCount = jsonObject.getString("Passed").replace("[", "").replace("]", "").split(",");
            //yourFormattedString = formatter.format(Integer.parseInt(TeacherCount[0]));
            countTP.setText(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(Integer.parseInt(TeacherCount[0]))));

        } catch (JSONException e) {
            e.printStackTrace();
        }


        countTE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawTeacherTraining();
            }
        });

        countTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawTeacherTraining();
            }
        });
    }


    public void showDailyCount(String str) {
        String s1[] = new String[1];
        String yourFormattedString;
        try {
            JSONObject jsonObject = new JSONObject(str);
            s1 = jsonObject.getString("UniqueAttendance").replace("[", "").replace("]", "").split(",");
            // yourFormattedString = formatter.format(Integer.parseInt(s1[0].toString()));
            uCount.setText(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(Integer.parseInt(s1[0].toString()))));

            s1 = jsonObject.getString("AttendanceByInternet").replace("[", "").replace("]", "").split(",");
            //yourFormattedString = formatter.format(Integer.parseInt(s1[0].toString()));
            iCount.setText(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(Integer.parseInt(s1[0].toString()))));

            s1 = jsonObject.getString("AttendanceBySMS").replace("[", "").replace("]", "").split(",");
            //yourFormattedString = formatter.format(Integer.parseInt(s1[0].toString()));
            sCount.setText(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(Integer.parseInt(s1[0].toString()))));
        } catch (JSONException er) {
            Toast.makeText(NewDashBoardGraphActivity.this, "Something is wrong", Toast.LENGTH_SHORT).show();
        }


    }

    public void drawTeacherTraining() {
        if (role.equals("NA")) {
            countTE.setEnabled(false);
            countTP.setEnabled(false);
        }else {
            countTE.setEnabled(true);
            countTP.setEnabled(true);
            if (isInternetOn()) {
                Intent intent = new Intent(NewDashBoardGraphActivity.this, DistWiseGraph.class);
                intent.putExtra("zone", countTE.getText());
                intent.putExtra("lineBar", "false");
                intent.putExtra("isbarchart1", "false");
                intent.putExtra("isBarchart", "false");
                intent.putExtra("isBarchart2", "false");
                intent.putExtra("isBarchart3", "false");
                intent.putExtra("TT", "true");
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(NewDashBoardGraphActivity.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                try {
                    Intent intent = new Intent(NewDashBoardGraphActivity.this, DistWiseGraph.class);
                    intent.putExtra("zone", countTE.getText());
                    intent.putExtra("lineBar", "false");
                    intent.putExtra("isbarchart1", "false");
                    intent.putExtra("isBarchart2", "false");
                    intent.putExtra("isBarchart", "false");
                    intent.putExtra("isBarchart3", "false");
                    intent.putExtra("TT", "true");
                    startActivity(intent);
                    finish();
                } catch (Exception err) {
                    Log.i("Error", "Show Error" + err);
                    Toast.makeText(getApplicationContext(), "Check your Internet connection " + err, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}