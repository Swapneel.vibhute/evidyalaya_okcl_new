package com.okcl.ICTMonitor.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;

import pojo.Beans;
import pojo.LoginDetailsBean;
import pojo.RegistrationTable;
import pojo.SchoolInfoTable;

/**
 * Created by abhinav on 11/9/16.
 */
public class DBAdapter extends SQLiteOpenHelper {

    public static SQLiteDatabase db;
    public static String DATABASE_NAME = "eVidyalaya.db";
    public static int DATABASE_VERSION = 127;

    private static final String TABLE_NAME_1 = "Roles";
    private static final String TABLE_NAME_2 = "Registration";
    private static final String TABLE_NAME_3 = "ICTCoOrdinatorReport";
    private static final String TABLE_NAME_4 = "visitedSchool";
    private static final String TABLE_NAME_5 = "RoleCategory";
    private static final String TABLE_NAME_6 = "TicketRemark";
    private static final String TABLE_NAME_7 = "Tracker";
    private static final String TABLE_NAME_9 = "Ticket";
    private static final String TABLE_NAME_10 = "Location";
    private static final String TABLE_NAME_11 = "IssueCategory";
    private static final String TABLE_NAME_12 = "tblFieldItem1";
    private static final String TABLE_NAME_13 = "tblFieldItemMaster2";
    private static final String TABLE_NAME_14 = "tblComponentChecklist";
    private static final String TABLE_NAME_15 = "Events";
    private static final String TABLE_NAME_16 = "SchoolRemark";
    private static final String TABLE_NAME_17 = "SchoolInformation";
    private static final String TABLE_NAME_18 = "tblTeacherStudFeedback";
    private static final String TABLE_NAME_19 = "TemptblsaveSchoolInformation";
    private static final String TABLE_NAME_20 = "LoginDetails";
    private static final String TABLE_NAME_21 = "SchoolSurvey";
    private static final String TABLE_NAME_22 = "Temptbldownload";
    private static final String TABLE_NAME_23 = "Dashboard";
    private static final String TABLE_NAME_24 = "tblAddJuniors";
    private static final String TABLE_NAME_25 = "nwdtblAddJuniors";
    private static final String TABLE_NAME_26 = "tblDownloadDashboardCounterdata";
    private static final String TABLE_NAME_27 = "dashboardcountermasterdata";
    private static final String TABLE_NAME_28 = "DownloadzonestateSL9th";
    private static final String TABLE_NAME_29 = "DownloadslzonestateSL10th";
    private static final String TABLE_NAME_30 = "TicketLogs";
    private static final String TABLE_NAME_31 = "DownloadZonewiseTT";
    private static final String TABLE_NAME_32 = "DownloadDistrictwiseTT";
    private static final String TABLE_NAME_33 = "DownloadDistrictwise9thSl";
    private static final String TABLE_NAME_34 = "zone1class9thtbl";
    private static final String TABLE_NAME_35 = "DownloadDistrictwise10thSl";
    private static final String TABLE_NAME_36 = "zone2class10thtbl";
    private static final String TABLE_NAME_37 = "TTDistrictwisestatictbl";
    private static final String TABLE_NAME_38 = "valuesofcountmaster";
    private static final String TABLE_NAME_39 = "Roottrackdata";
    private static final String TABLE_NAME_40 = "Dlcdashboarddistrictwise";
    private static final String TABLE_NAME_41 = "LanguageTable";
    private static final String TABLE_NAME_42 = "SchoolDataInDistrictTable";
    private static final String TABLE_NAME_43 = "TeachertrainingEnrolledlearnperform";
    private static final String TABLE_NAME_44 = "zonewiseenrollperformcompl";
    private static final String TABLE_NAME_45 = "newallstatictableTT";
    private static final String TABLE_NAME_46 = "AssignZoneMaster";
    private static final String TABLE_NAME_47 = "Storesurveyid";
    private static final String TABLE_NAME_48 = "Master";
    private static final String TABLE_NAME_49 = "evidyalayaMilestone";
    private static final String TABLE_NAME_50 = "studentlearning";

    private static final String TABLE_CREATE_1 = "create table if not exists " + TABLE_NAME_1
            + "( id integer primary key autoincrement, "
            + " categoryID integer, "
            + " roleName text, "
            + " Priority integer );";

    private static final String TABLE_CREATE_2 = "create table if not exists "
            + TABLE_NAME_2
            + "( id integer primary key autoincrement, "
            + " mobileNumber text, "
            + " FirstName text, "
            + " LastName text, "
            + " roleID text, "
            + " talukaID text, "
            + " districtID text, "
            + " stateID text, "
            + " eMail text, "
            + " referalMobile text, "
            + " pinCode text, "
            + " IMEL text, "
            + " sim text, "
            + " latitude text, "
            + " longitude text, "
            + " OTPstatus text );";

    private static final String TABLE_CREATE_3 = "create table if not exists "
            + TABLE_NAME_3
            + "( id integer primary key autoincrement, "
            + " Date text, "
            + " SchoolName text, "
            + " RaiseLocation text, "
            + " VisitedSchoolCount text, "
            + " TravelDistance text, "
            + " UIDISEcode text, "
            + " inTime text, "
            + " outTime text, "
            + " TotalTime text, "
            + " UserMobileNumber text, "
            + " Status text );";

    private static final String TABLE_CREATE_4 = "create table if not exists "
            + TABLE_NAME_4
            + "( id integer primary key autoincrement, "
            + " UIDISEcode text, "
            + " schoolName text, "
            + " schoolAddress text, "
            + " stateID integer, "
            + " districtID integer, "
            + " block integer, "
            + " pinCode text, "
            + " eMail text, "
            + " MobileNumber text, "
            + " STDnumber text, "
            + " schoolCoOrdinatorName text, "
            + " schoolCoOrdinatorNumber text, "
            + " Status text );";

    private static final String TABLE_CREATE_5 = "create table if not exists "
            + TABLE_NAME_5
            + "( id integer primary key autoincrement, "
            + " Category text );";

    private static final String TABLE_CREATE_6 = "create table if not exists "
            + TABLE_NAME_6
            + "( id integer primary key autoincrement, "
            + " TId text, "
            + " Remark text, "
            + " RoleID integer, "
            + " RoleName text, "
            + " InsertByMobileNumber text, "
            + " InsertByName text, "
            + " Date text, "
            + " Status text); ";

    private static final String TABLE_CREATE_7 = "create table if not exists "
            + TABLE_NAME_7
            + "( id integer primary key autoincrement, "
            + " date text, "
            + " latitude text, "
            + " longitude text, "
            + " IMEI text, "
            + " userMobileNo text, "
            + " Status text); ";

    private static final String TABLE_CREATE_9 = "create table if not exists "
            + TABLE_NAME_9
            + "( id integer primary key autoincrement, "
            + " TokenId text, "
            + " MobileNumber text, "
            + " RegisterBy text, "
            + " UIDISEcode text, "
            + " SchoolName text, "
            + " DistrictID text, "
            + " roleID integer, "
            + " RaiseDate text, "
            + " CategoryID integer, "
            + " SubCategoryID integer, "
            + " Description text, "
            + " Photo text, "
            + " StatusID integer, "
            + " AssignTo text, "
            + " AssignDate text, "
            + " DueDate text, "
            + " IMEI text, "
            + " ReRaised text, "
            + " TicketSource text, "
            + " Zone text, "
            + " Status integer );";

    private static final String TABLE_CREATE_10 = "create table if not exists "
            + TABLE_NAME_10
            + "( id integer primary key autoincrement, "
            + " MobileNumber text, "
            + " Latitude text, "
            + " Longitude text, "
            + " Date text );";

    private static final String TABLE_CREATE_11 = "create table if not exists "
            + TABLE_NAME_11
            + "( id integer primary key autoincrement, "
            + " CategoryName text );";

    private static final String TABLE_CREATE_12 = "create table if not exists "
            + TABLE_NAME_12
            + "( id integer primary key autoincrement, "
            + " FserverId integer, "
            + " FieldName text);";

    private static final String TABLE_CREATE_13 = "create table if not exists "
            + TABLE_NAME_13
            + "( id integer primary key autoincrement, "
            + " MServerId integer, "
            + " FieldItemValue text, "
            + " FMServerId integer );";

    private static final String TABLE_CREATE_14 = "create table if not exists "
            + TABLE_NAME_14
            + "( id integer primary key autoincrement, "
            + "FMserverId integer, "
            + "MserverId integer, "
            + "ComponentStatus integer,"
            + "ExpiryDate string,"
            + "Meterreading string ,"
            + "UdiseCode string,"
            + "IMEInumber string,"
            + "InsertBy string,"
            + "InsertDate string,"
            + "Photo1 string,"
            + "Photo2 string,"
            + "Recordfrom integer,"
            + "LocalID string,"
            + "lat string,"
            + "long string,"
            + "serveyid string,"
            + "Record_type string,"
            + "ServerStatus integer);";

    private static final String TABLE_CREATE_15 = "create table if not exists "
            + TABLE_NAME_15
            + "( id integer primary key autoincrement, "
            + "Date string, "
            + "InTime string, "
            + "OutTime string, "
            + "Event string, "
            + "MobileNumber string, "
            + "Status integer);";

    private static final String TABLE_CREATE_16 = "create table if not exists "
            + TABLE_NAME_16
            + "( id integer primary key autoincrement, "
            + " UDISE text, "
            + " SName text, "
            + " Remark text, "
            + " RoleID integer, "
            + " RoleName text, "
            + " InsertByMobileNumber text, "
            + " InsertByName text, "
            + " Date text, "
            + " IMEI text, "
            + " Status text); ";

    private static final String TABLE_CREATE_17 = "create table if not exists "
            + TABLE_NAME_17
            + "( id integer primary key autoincrement, "
            + " UDISE text, "
            + " SchoolName text, "
            + " DistrictID integer, "
            + " DistrictName text, "
            + " Zone text, "
            + " Landline text, "
            + " HMName text, "
            + " HMMobileNumber text, "
            + " SchoolEMailID text, "
            + " Latitude text, "
            + " Longitude text, "
            + " Photo text, "
            + " Block text, "
            + " Status text); ";

    private static final String TABLE_CREATE_18 = "create table if not exists "
            + TABLE_NAME_18
            + "( id integer primary key autoincrement, "
            + "Usermobilenumber string, "
            + "Name string,"
            + "ClassId integer ,"
            + "SubjectId integer,"
            + "RatingId integer,"
            + "RoleId string,"
            + "UdiseCode string,"
            + "InsertBy string,"
            + "LocalID string,"
            + "InsertDate string,"
            + "Photo1 string,"
            + "Recordfrom integer,"
            + "SurveyId integer,"
            + "ServerStatus integer);";

    private static final String TABLE_CREATE_19 = "create table if not exists "
            + TABLE_NAME_19
            + "( id integer primary key autoincrement, "
            + "UdiseCode string,"
            + "SchoolName string,"
            + "DistrictName string,"
            + "Latitude string, "
            + "Longitude string, "
            + "schoolComponentStatus integer,"
            + "lokedReasonid integer ,"
            + "Landline text, "
            + "HMName text, "
            + "HMMobileNumber text, "
            + "SchoolEMailID text, "
            + "InsertBy string,"
            + "InsertDate string,"
            + "Photo1 string,"
            + "IMEInumber string,"
            + "Fmserverid string,"
            + "RecordFrom string,"
            + "lat string,"
            + "long string,"
            + "serveyid string,"
            + "LocalID string,"
            + "Recordtype string  ,"
            + "ServerStatus integer);";

    private static final String TABLE_CREATE_20 = "create table if not exists "
            + TABLE_NAME_20
            + "( id integer primary key autoincrement, "
            + " PkRegId text, "
            + " Firstname text, "
            + " Lastname integer, "
            + " Username text, "
            + " Roleid text, "
            + " Rolename text, "
            + " Reffmobileno text, "
            + " Userpassword text, "
            + " Imei text, "
            + " SimSerial text, "
            + " Latitude text, "
            + " Longitude text, "
            + " Insertdate text, "
            + " Email text, "
            + " TalukaId int, "
            + " Photo text, "
            + " DistrictId int, "
            + " locatezone text,"
            + " Status text); ";

    private static final String TABLE_CREATE_21 = "create table if not exists "
            + TABLE_NAME_21
            + "( id integer primary key autoincrement, "
            + " UDISE text, "
            + " SchoolName text, "
            + " VisitorMobile text, "
            + " VisitorName integer, "
            + " Date integer, "
            + " Report text); ";

    private static final String TABLE_CREATE_22 = "create table if not exists "
            + TABLE_NAME_22
            + "( id integer primary key autoincrement, "
            + " FMserverId integer, "
            + " MserverId integer, "
            + " ComponentStatus integer,"
            + " ExpiryDate string,"
            + " Meterreading string ,"
            + " UdiseCode string,"
            + " IMEInumber string,"
            + " InsertBy string,"
            + " InsertDate string,"
            + " Photo1 string,"
            + " Photo2 string,"
            + " Recordfrom integer,"
            + " ServerStatus integer);";

    private static final String TABLE_CREATE_23 = "create table if not exists "
            + TABLE_NAME_23
            + "( id integer primary key autoincrement, "
            + " SCAttendanceTotal integer, "
            + " SCAttendanceSMS integer, "
            + " SCAttendanceInternet integer, "
            + " StudentLearningTotal integer, "
            + " StudentLearningComplete integer,"
            + " StudentLearningRemaining integer,"
            + " TeacherTrainingTotal integer, "
            + " TeacherTrainingComplete integer,"
            + " TeacherTrainingRemaining integer,"
            + " TicketCount integer ,"
            + " TicketAccept integer,"
            + " TicketAcknowledge integer,"
            + " TicketProcess integer,"
            + " TicketResolved integer,"
            + " TicketClosed integer,"
            + " TicketReraised integer,"
            + " SurveyTotal integer,"
            + " SurveyDone integer,"
            + " SurveyPercent string);";

    private static final String TABLE_CREATE_24 = "create table if not exists "
            + TABLE_NAME_24
            + "( id integer primary key autoincrement, "
            + " UserMobile text, "
            + " RefMobile text, "
            + " IMEINumber text,"
            + " InsertDate text,"
            + " ModifyDate text,"
            + " keyword text,"
            + " RefName text,"
            + " isActive text);";

    private static final String TABLE_CREATE_25 = "create table if not exists "
            + TABLE_NAME_25

       /*     <tblAddJuniors>
    <PK_Id>1</PK_Id>
    <UserMobile>9730939392</UserMobile>
    <RefMobile>9112019919</RefMobile>
    <IMEINumber>0</IMEINumber>
    <InsertDate>2016-11-20</InsertDate>
    <keyword>EVIDYALAYA</keyword>
    <RefName>abhijit rajmane</RefName>
    <isActive>0</isActive>
    </tblAddJuniors>
    </NewDataSet>*/

            + "( id integer primary key autoincrement, "
            + " UserMobile text, "
            + " RefMobile text, "
            + " IMEINumber text,"
            + " InsertDate text,"
            + " keyword text,"
            + " RefName text,"
            + " Status text,"
            + "  Serverid text, "
            + " isActive text);";

    private static final String TABLE_CREATE_26 = "create table if not exists "
            + TABLE_NAME_26//tblDownloadDashboardCounterdata

            + "( id integer primary key autoincrement, "
            + " SchoolServery text,"
            + "  SchoolCount text, "
            + " TicketCount text);";

    private static final String TABLE_CREATE_27 = "create table if not exists "
            + TABLE_NAME_27
            + "( id integer primary key autoincrement, "
            + " cid text, "
            + " DateValue text, "
            + " UniqueAtt text, "
            + " AttByInternet text, "
            + " AttBySMS text, "
            + " status text); ";

    private static final String TABLE_CREATE_28 = "create table if not exists "
            + TABLE_NAME_28
            + "( id integer primary key autoincrement, "
            + " cid text, "
            + " Zone text, "
            + " Orderby text, "
            + " ZoneCount text, "
            + " StateCount text, "
            + " status text); ";

    private static final String TABLE_CREATE_29 = "create table if not exists "
            + TABLE_NAME_29
            + "( id integer primary key autoincrement, "
            + " slid text, "
            + " slzone text, "
            + " Orderby text, "
            + " slZoneCount text, "
            + " StateCount text, "
            + " slstatus text); ";

    private static final String TABLE_CREATE_30 = "create table if not exists "
            + TABLE_NAME_30

            + "( id integer primary key autoincrement, "
            + " TokenId text,"
            + " LogDetails text, "
            + " InsertDate text, "
            + " LogStatusCode text);";

    /*
        <Table>
        <Id>1</Id>
        <DateValue>2016-12-07</DateValue>
        <UniqueAtt>2922</UniqueAtt>
        <AttByInternet>2037</AttByInternet>
        <AttBySMS>885</AttBySMS>
        </Table>*/
    private static final String TABLE_CREATE_31 = "create table if not exists "
            + TABLE_NAME_31
            + "( id integer primary key autoincrement, "
            + " TTid text, "
            + " TTzone text, "
            + " TTZoneCount text, "
            + " TTStateCount text, "
            + " TTstatus text); ";

    private static final String TABLE_CREATE_32 = "create table if not exists "
            + TABLE_NAME_32
            + "( id integer primary key autoincrement, "
            + " TTid text, "
            + " DistrictIdTT text, "
            + " DistrictNameTT text, "
            + " DistCountTT text, "
            + " TTstatus text); ";

    private static final String TABLE_CREATE_33 = "create table if not exists "
            + TABLE_NAME_33
            + "( id integer primary key autoincrement, "
            + " SLid text, "
            + " DistrictIdSL text, "
            + " DistrictNameSL text, "
            + " DistCountSL text, "
            + " statusSL text); ";

    private static final String TABLE_CREATE_34 = "create table if not exists "
            + TABLE_NAME_34
            + "( id integer primary key autoincrement, "
            + " Areaname text, "
            + " AreaId text, "
            + " referenceid text, "
            + " count text); ";


    /*  <NewDataSet>
      <Table>
      <Id>1</Id>
      <DistrictId>2101</DistrictId>
      <DistrictName>BARGARH</DistrictName>
      <DistCount>979</DistCount>
      </Table>*/

    private static final String TABLE_CREATE_35 = "create table if not exists "
            + TABLE_NAME_35
            + "( id integer primary key autoincrement, "
            + " Slid text, "
            + " SlDistrictIdSL text, "
            + " SlDistrictNameSL text, "
            + " SlDistCount text, "
            + " statusSL text); ";

    private static final String TABLE_CREATE_36 = "create table if not exists "
            + TABLE_NAME_36
            + "( id integer primary key autoincrement, "
            + " Areaname text, "
            + " AreaId text, "
            + " referenceid text, "
            + " count text); ";

    private static final String TABLE_CREATE_37 = "create table if not exists "
            + TABLE_NAME_37
            + "( id integer primary key autoincrement, "
            + " Areaname text, "
            + " AreaId text, "
            + " referenceid text, "
            + " count text); ";

    private static final String TABLE_CREATE_38 = "create table if not exists "
            + TABLE_NAME_38
            + "( id integer primary key autoincrement, "
            + " SLcountvalue text, "
            + " TotalTT text, "
            + " totalsurveyvalue text, "
            + " totalticketvalue text, "
            + " nineslcount text, "
            + " tenslcount text, "
            + " TTcompleted text, "
            + " TTtopperform text, "
            + " Datetime text );";

    private static final String TABLE_CREATE_39 = "create table if not exists "
            + TABLE_NAME_39
            + "( id integer primary key autoincrement, "
            + " serverid text, "
            + " maxid text, "
            + " lat text, "
            + " long text );";

    private static final String TABLE_CREATE_40 = "create table if not exists "
            + TABLE_NAME_40
            + "( id integer primary key autoincrement, "
            + " distid text, "
            + " maxudisecode text, "
            + " value1 text, "
            + " value2 text, "
            + " status text );";

    private static final String TABLE_CREATE_41 = "create table if not exists "
            + TABLE_NAME_41
            + "( id integer primary key, "
            + " Language text, "
            + " LANGUAGENAME text, "
            + " LanguageID text );";

    private static final String TABLE_CREATE_42 = "create table if not exists "
            + TABLE_NAME_42
            + "( id integer primary key, "
            + " DistrictName text, "
            + " DistrictID text, "
            + " SchoolUDISEID text, "
            + " Ninethcount text, "
            + " Teachercount text, "//ENROLLED
            + " Topperformed text, " //topperformed
            + " completed text, " //completed
            + " Tenthcount text);";

    private static final String TABLE_CREATE_43 = "create table if not exists "
            + TABLE_NAME_43
            + "( id integer primary key autoincrement, "

            + " TTid text, "
            + " DistrictIdTT text, "
            + " DistrictNameTT text, "
            + " TEnrolld text, "
            + " TTopPerform text, "
            + " Tcompleted text, "
            + " TTstatus text); ";

    private static final String TABLE_CREATE_44 = "create table if not exists "
            + TABLE_NAME_44
            + "( id integer primary key autoincrement, "
            + " TEnrolld text, "
            + " TTopPerform text, "
            + " Tcompleted text, "
            + " orderby text, "
            + " Zone text, "
            + " TTstatus text); ";

    private static final String TABLE_CREATE_45 = "create table if not exists "
            + TABLE_NAME_45
            + "( id integer primary key autoincrement, "
            + " Areaname text, "
            + " AreaId text, "
            + " referenceid text, "
            + " TEnrolld text, "
            + " TTopPerform text, "
            + " Tcompleted text); ";

    private static final String TABLE_CREATE_46 = "create table if not exists "
            + TABLE_NAME_46
            + "( id integer primary key autoincrement, "
            + " Juniormbno text, "
            + " Rolename text, "
            + " Zonenumber text, "
            + " Status text, "
            + "serverstatus text, "
            + " Createdby text, "
            + " Createddate text); ";

    private static final String TABLE_CREATE_47 = "create table if not exists " + TABLE_NAME_47
            + "( id integer primary key autoincrement, "
            + " serveyid text );";

    private static final String TABLE_CREATE_48 = "create table if not exists " + TABLE_NAME_48

            + "( id integer primary key autoincrement, "
            + " Udise_code text, "
            + " Time_date text, "
            + " Survey_id text, "
            + " local_id text, "
            + " Record_type text, "
            + " Status text); ";

    private static final String TABLE_CREATE_49 = "create table if not exists " + TABLE_NAME_49
            + "( id integer primary key autoincrement, "
            + "FMserverId integer, "
            + "MserverId integer, "
            + "ComponentStatus integer,"
            + "ExpiryDate string,"
            + "Meterreading string ,"
            + "UdiseCode string,"
            + "IMEInumber string,"
            + "InsertBy string,"
            + "InsertDate string,"
            + "Photo1 string,"
            + "Photo2 string,"
            + "Recordfrom integer,"
            + "LocalID string,"
            + "lat string,"
            + "long string,"
            + "serveyid string,"
            + "Record_type string,"
            + "noofDays string,"
            + "totalcomplaint string,"
            + "solveComplaint string,"
            + "paneltyStore string,"
            + "numberofPeriodsictlab string,"
            + "remark string,"
            + "genratorreading string,"
            + "ServerStatus integer);";

    private static final String TABLE_CREATE_50 = "create table if not exists "
            + TABLE_NAME_50
            + "( id integer primary key autoincrement, "
            + "  sl text, "
            + "  Datetime text );";

    public DBAdapter(Context context, String dbName, SQLiteDatabase.CursorFactory factory,
                     int version, SQLiteDatabase db) {
        super(context, dbName, factory, version);
        DBAdapter.db = db;
        // DBAdapter.db = getWritableDatabase();
        DATABASE_NAME = dbName;
        DATABASE_VERSION = version;
    }

    public static SQLiteDatabase getDb() {
        return db;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE_1);
        db.execSQL(TABLE_CREATE_2);
        db.execSQL(TABLE_CREATE_3);
        db.execSQL(TABLE_CREATE_4);
        db.execSQL(TABLE_CREATE_5);
        db.execSQL(TABLE_CREATE_6);
        db.execSQL(TABLE_CREATE_7);
        db.execSQL(TABLE_CREATE_9);
        db.execSQL(TABLE_CREATE_10);
        db.execSQL(TABLE_CREATE_11);
        db.execSQL(TABLE_CREATE_12);
        db.execSQL(TABLE_CREATE_13);
        db.execSQL(TABLE_CREATE_14);
        db.execSQL(TABLE_CREATE_15);
        db.execSQL(TABLE_CREATE_16);
        db.execSQL(TABLE_CREATE_17);
        db.execSQL(TABLE_CREATE_18);
        db.execSQL(TABLE_CREATE_19);
        db.execSQL(TABLE_CREATE_20);
        db.execSQL(TABLE_CREATE_21);
        db.execSQL(TABLE_CREATE_22);
        db.execSQL(TABLE_CREATE_23);
        db.execSQL(TABLE_CREATE_24);
        db.execSQL(TABLE_CREATE_25);
        db.execSQL(TABLE_CREATE_26);
        db.execSQL(TABLE_CREATE_27);
        db.execSQL(TABLE_CREATE_28);
        db.execSQL(TABLE_CREATE_29);
        db.execSQL(TABLE_CREATE_30);
        db.execSQL(TABLE_CREATE_31);
        db.execSQL(TABLE_CREATE_31);
        db.execSQL(TABLE_CREATE_32);
        db.execSQL(TABLE_CREATE_33);
        db.execSQL(TABLE_CREATE_34);
        db.execSQL(TABLE_CREATE_35);
        db.execSQL(TABLE_CREATE_36);
        db.execSQL(TABLE_CREATE_37);
        db.execSQL(TABLE_CREATE_38);
        db.execSQL(TABLE_CREATE_39);
        db.execSQL(TABLE_CREATE_40);
        db.execSQL(TABLE_CREATE_41);
        db.execSQL(TABLE_CREATE_42);
        db.execSQL(TABLE_CREATE_43);
        db.execSQL(TABLE_CREATE_44);
        db.execSQL(TABLE_CREATE_45);
        db.execSQL(TABLE_CREATE_46);
        db.execSQL(TABLE_CREATE_47);
        db.execSQL(TABLE_CREATE_48);
        db.execSQL(TABLE_CREATE_49);
        db.execSQL(TABLE_CREATE_50);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_1);
        Log.i("Roles table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_2);
        Log.i("Registration table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_3);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_4);
        Log.i("Visited School table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_5);
        Log.i("Role Category table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_6);
        Log.i("Ticket Remark table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_7);
        Log.i("Tracker table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_9);
        Log.i("Ticket table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_10);
        Log.i("Status table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_11);
        Log.i("Issue category table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_12);
        Log.i("Field Item table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_13);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_14);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_15);
        Log.i("Events table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_16);
        Log.i("School Remark table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_17);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_18);
        Log.i("tblTeacherStudFeedback ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_19);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_20);
        Log.i("Login Details table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_21);
        Log.i("School Survey table ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_22);
        Log.i("Temptbldownload ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_23);
        Log.i("Dashboard ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_24);
        Log.i("tblAddJuniors ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_25);
        Log.i("nwdtblAddJuniors ", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_26);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_27);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_28);
        Log.i(" DownloadzonestateSL9th", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_29);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_30);
        Log.i(" tikkkkkkkkkk", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_31);
        Log.i(" TTZonewise", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_32);
        Log.i(" DownloaddistictwiseTT", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_33);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_34);
        Log.i(" zone1classtbl", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_35);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_36);
        Log.i(" zone2class10thtbl", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_37);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_38);
        Log.i(" updatedashvalue", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_39);
        Log.i(" Roottrackdata", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_40);
        Log.i(" dlcdashboard", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_41);
        Log.i(" dlcdashboard", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_42);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_43);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_44);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_45);
        Log.i(" staticallTT", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_46);
        Log.i(" AssignZoneMaster", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_47);
        Log.i(" AssignZoneMaster", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_48);
        Log.i(" Master", "recreated");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_49);
        Log.i(" evidyalayaMilestone", "recreated");

        onCreate(db);
    }

    public DBAdapter open() throws SQLException {
        try {
            db = getWritableDatabase();
            System.out.println(db.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public void addRoles()
    {
        try {
            open();

            String query = "select * from " + TABLE_NAME_1;
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.getCount() == 0)
            {
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('SCHOOL CO-ORDINATOR',1,8);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('HM',2,12);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('ICT CO-ORDINATOR',3,4);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('BEO',2,11);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('DEO',2,10);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('RMSA (OMSA)',2,9);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('SME & D',0,20);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('OKCL ADMIN',3,1);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('OMSM',0,19);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('IL & FS ETS',0,18);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('TCIL',0,17);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('ZONAL MANAGER',1,6);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('DLC CO-ORDINATOR',3,3);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('CALL CENTER EXECUTIVE',3,2);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('VENDOR ADMIN',1,5);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('Sub System Admin',0,16);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('Parents',0,14);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('TEACHER',2,13);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('Students',0,15);");
                db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID,Priority)" + " VALUES ('DISTRICT CO-ORDINATOR',1,7);");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        close();
    }

    public void addRolesCategory() {
        try {
            open();
            String query = "select * from " + TABLE_NAME_5;
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.getCount() == 0) {
                db.execSQL("INSERT INTO " + TABLE_NAME_5 + "(Category)" + " VALUES ('VENDOR');");
                db.execSQL("INSERT INTO " + TABLE_NAME_5 + "(Category)" + " VALUES ('S & MED');");
                db.execSQL("INSERT INTO " + TABLE_NAME_5 + "(Category)" + " VALUES ('OKCL');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        close();
    }

    public ArrayList<String> getRolesCategory() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> role_list = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_5;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                role_list.add(cursor.getString(cursor.getColumnIndex("Category")));

            } while (cursor.moveToNext());
        }
        return role_list;
    }

    public int getrolesCategoryID(String name) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_5 + " where Category ='" + name + "'";
        int id = 0;
        Cursor mCursor = db.rawQuery(query, null);
        if (mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            do {
                id = mCursor.getInt(mCursor.getColumnIndex("id"));

            } while (mCursor.moveToNext());
        }
        return id;
    }

    public ArrayList<String> getRoles(int cid) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> role_list = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_1 + " where categoryID = " + cid + " order by Priority asc";

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                role_list.add(cursor.getString(cursor.getColumnIndex("roleName")));

            } while (cursor.moveToNext());
        }
        return role_list;
    }

    public String getRoleId(String name) {
        String query = "select * from " + TABLE_NAME_1 + " where roleName = '" + name + "'";
        String ids = "";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst() != false) {
            cursor.moveToFirst();
            do {

                ids = cursor.getString(cursor.getColumnIndex("id"));

            } while (cursor.moveToNext());
        }
        return ids;
    }

    public String getRoleName(String id) {
        String query = "select * from " + TABLE_NAME_1 + " where id = '" + id + "'";
        String ids = "";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst() != false) {
            cursor.moveToFirst();
            do {

                ids = cursor.getString(cursor.getColumnIndex("roleName"));

            } while (cursor.moveToNext());
        }
        return ids;
    }

    public long insertRegistration(RegistrationTable registrationTable) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ContentValues contentValues = new ContentValues();

        contentValues.put("mobileNumber", registrationTable.getMobilNumber());
        contentValues.put("FirstName", registrationTable.getFirstName());
        contentValues.put("LastName", registrationTable.getLastName());
        contentValues.put("roleID", registrationTable.getRoleID());
        contentValues.put("talukaID", registrationTable.getTalukaID());
        contentValues.put("districtID", registrationTable.getDistrictID());
        contentValues.put("stateID", registrationTable.getStateID());
        contentValues.put("eMail", registrationTable.getEMail());
        contentValues.put("referalMobile", registrationTable.getReferalMobile());
        contentValues.put("pinCode", registrationTable.getPin());
        contentValues.put("IMEL", registrationTable.getIMEI());
        contentValues.put("sim", registrationTable.getSim());
        contentValues.put("latitude", registrationTable.getLatitude());
        contentValues.put("longitude", registrationTable.getLongitude());
        contentValues.put("OTPstatus", registrationTable.getOTPstatus());

        long i = db.insert(TABLE_NAME_2, null, contentValues);
        return i;
    }

    public long updateOTPstatus(RegistrationTable registrationTable) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ContentValues contentValues = new ContentValues();

        contentValues.put("OTPstatus", "Verified");

        long i = db.update(TABLE_NAME_2, contentValues, "mobileNumber = ?", new String[]{String.valueOf(registrationTable.getMobilNumber())});
        return i;
    }

       public String getnamefromlistpointid(String id) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_13 + " where MServerId = " + id;
        Cursor cursor = db.rawQuery(query, null);
        Beans beans = new Beans();
        cursor.moveToFirst();
        String d = cursor.getString(cursor.getColumnIndex("FieldItemValue"));
        return d;
    }

   public ArrayList<String> getlocation(String number) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        ArrayList<String> locationdata = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_10 + " where MobileNumber = '" + number + "'";
        cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            locationdata.add(cursor.getString(cursor.getColumnIndex("Latitude")));
            locationdata.add(cursor.getString(cursor.getColumnIndex("Longitude")));
            locationdata.add(cursor.getString(cursor.getColumnIndex("MobileNumber")));
            locationdata.add(cursor.getString(cursor.getColumnIndex("Date")));
        }
        return locationdata;
    }

    public ArrayList<String> getLatitudeArray() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> lat_list = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_39;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                lat_list.add(cursor.getString(cursor.getColumnIndex("lat")));

            } while (cursor.moveToNext());
        }
        return lat_list;
    }

    public ArrayList<String> getLongitudeArray() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> longlist = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_39;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                longlist.add(cursor.getString(cursor.getColumnIndex("long")));

            } while (cursor.moveToNext());
        }
        return longlist;
    }

    public ArrayList<String> getdatevalue() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> IssueCategory_list = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_38;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                IssueCategory_list.add(cursor.getString(cursor.getColumnIndex("Datetime")));

            } while (cursor.moveToNext());
        }
        return IssueCategory_list;
    }

    public ArrayList<String> getIssueCategory(int id) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> IssueCategory_list = new ArrayList<>();
        String temp="School Coordinator(SC)";
        String query = "select FieldItemValue from " + TABLE_NAME_13 + " where FMServerId = " + id+" and FieldItemValue not in('"+temp+"')";

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                IssueCategory_list.add(cursor.getString(cursor.getColumnIndex("FieldItemValue")));

            } while (cursor.moveToNext());
        }
        return IssueCategory_list;
    }

    public String getIssueCategoryID(String id) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String valieid = "";
        String query = "select * from " + TABLE_NAME_13 + " where FieldItemValue = '" + id + "'";

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                valieid = (cursor.getString(cursor.getColumnIndex("MServerId")));

            } while (cursor.moveToNext());
        }
        return valieid;
    }

  /* public long insertjunorlist_item(Addjunoirbeen s)
   {
        long i = 0;
        try {
            open();

            ContentValues contentValues = new ContentValues();
            contentValues.put("UserMobile", s.getUserMobile());
            contentValues.put("RefMobile", s.getRefMobile());
            contentValues.put("IMEINumber", s.getIMEINumber());
            contentValues.put("InsertDate", s.getInsertDate());
            contentValues.put("keyword", s.getKeyword());
            contentValues.put("RefName", s.getRefName());
            contentValues.put("isActive", s.getIsActive());
            contentValues.put("Serverid", s.getServerid());
            contentValues.put("Status", "0");

            i = db.insert(TABLE_NAME_25, null, contentValues);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;
    }*/





    public boolean isAddMobileAvailable(String mb)//get mb number from databse;
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_25 + " where RefMobile = " + mb;
        Cursor cursor = null;
        boolean b = false;
        cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            b = true;
        }
        return b;
    }

    /*  public ArrayList<DashboardCountBean> getAllOrder2()
    {
        ArrayList<DashboardCountBean> list = new ArrayList<DashboardCountBean>();
        try {
            open();
            Cursor cursor;
            String selectQuery = "select * from " +TABLE_CREATE_27;
            cursor = db.rawQuery(selectQuery, null);
            System.out.println("???" + cursor.getCount());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do
                    list.add(new DashboardCountBean(cursor.getString(0), cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5)));
                while (cursor.moveToNext());
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally {
            cursor.close();
            closeDatabase();
        }
        return list;
    }
*/
    public ArrayList<String> getdashboarduniquecount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ArrayList<String> getuniquelist = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_27;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                getuniquelist.add(cursor.getString(cursor.getColumnIndex("UniqueAtt")));

            } while (cursor.moveToNext());
        }
        return getuniquelist;
    }

    public ArrayList<String> getalltotalvalues() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getvalueslist = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_38;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
             /*   + " SLcountvalue text, "
                        + " TotalTT text, "
                        + " totalsurveyvalue text, "
                        + " totalticketvalue text, "*/

                getvalueslist.add(cursor.getString(cursor.getColumnIndex("SLcountvalue")));
                getvalueslist.add(cursor.getString(cursor.getColumnIndex("TotalTT")));
                getvalueslist.add(cursor.getString(cursor.getColumnIndex("totalsurveyvalue")));
                getvalueslist.add(cursor.getString(cursor.getColumnIndex("totalticketvalue")));
                getvalueslist.add(cursor.getString(cursor.getColumnIndex("nineslcount")));
                getvalueslist.add(cursor.getString(cursor.getColumnIndex("tenslcount")));
                getvalueslist.add(cursor.getString(cursor.getColumnIndex("TTcompleted")));
                getvalueslist.add(cursor.getString(cursor.getColumnIndex("TTtopperform")));


            } while (cursor.moveToNext());
        }
        return getvalueslist;
    }


    public ArrayList<String> getdashboardscattendencestateinternet() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ArrayList<String> getuniquelist = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_27;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getuniquelist.add(cursor.getString(cursor.getColumnIndex("AttByInternet")));

            } while (cursor.moveToNext());
        }
        return getuniquelist;
    }

    public ArrayList<String> getsmsattendence() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getuniquelist = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_27;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                getuniquelist.add(cursor.getString(cursor.getColumnIndex("AttBySMS")));

            } while (cursor.moveToNext());
        }
        return getuniquelist;
    }

    public ArrayList<String> getdashdatecount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getdashboarddatelist = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_27;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getdashboarddatelist.add(cursor.getString(cursor.getColumnIndex("DateValue")));
            }

            while (cursor.moveToNext());
        }
        return getdashboarddatelist;
    }

    public ArrayList<String> get9thstateslcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getstatecount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_28;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getstatecount.add(cursor.getString(cursor.getColumnIndex("StateCount")));
            }

            while (cursor.moveToNext());
        }
        return getstatecount;
    }

    public ArrayList<String> get10thstateslcount()

    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> get1ostatecount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_29;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                get1ostatecount.add(cursor.getString(cursor.getColumnIndex("StateCount")));
            }

            while (cursor.moveToNext());
        }
        return get1ostatecount;
    }

    public ArrayList<String> getsl9thzonecount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getstatecount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_28 + " order by Orderby asc";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getstatecount.add(cursor.getString(cursor.getColumnIndex("ZoneCount")));
            }

            while (cursor.moveToNext());
        }
        return getstatecount;
    }

    public ArrayList<String> getsl10thzonecount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzonecount10 = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_29 + " order by Orderby asc";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzonecount10.add(cursor.getString(cursor.getColumnIndex("slZoneCount")));
            }

            while (cursor.moveToNext());
        }
        return getzonecount10;
    }

    public ArrayList<String> getsl10thzonename() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzonecount10 = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_29 + " order by Orderby asc";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzonecount10.add(cursor.getString(cursor.getColumnIndex("slzone")));
            }

            while (cursor.moveToNext());
        }
        return getzonecount10;
    }

    public ArrayList<String> getsl9thzonname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getstatecount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_28 + " order by Orderby asc";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getstatecount.add(cursor.getString(cursor.getColumnIndex("Zone")));
            }

            while (cursor.moveToNext());
        }
        return getstatecount;
    }

    public ArrayList<String> getzonewiseTTcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzonewiseTTcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_31;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzonewiseTTcount.add(cursor.getString(cursor.getColumnIndex("TTZoneCount")));
            }

            while (cursor.moveToNext());
        }
        return getzonewiseTTcount;
    }

    public ArrayList<String> getzonewiseEnroTTcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzonewiseTTcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_44 + " order by Orderby asc";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzonewiseTTcount.add(cursor.getString(cursor.getColumnIndex("TEnrolld")));
            }

            while (cursor.moveToNext());
        }
        return getzonewiseTTcount;
    }

    public ArrayList<String> getzonewiseEnroTTname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzonewiseTTcount = new ArrayList<String>();


        String query = "select * from " + TABLE_NAME_44 + " order by Orderby asc";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzonewiseTTcount.add(cursor.getString(cursor.getColumnIndex("Zone")));
            }

            while (cursor.moveToNext());
        }
        return getzonewiseTTcount;
    }

    public ArrayList<String> getzonewisetopcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzonewiseTTcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_44 + " order by Orderby asc";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzonewiseTTcount.add(cursor.getString(cursor.getColumnIndex("TTopPerform")));
            }

            while (cursor.moveToNext());
        }
        return getzonewiseTTcount;
    }

    public ArrayList<String> getzonewisecompletedcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzonewiseTTcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_44 + " order by Orderby asc";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzonewiseTTcount.add(cursor.getString(cursor.getColumnIndex("Tcompleted")));
            }

            while (cursor.moveToNext());
        }
        return getzonewiseTTcount;
    }

    public ArrayList<String> getDistrictwiseTTcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getdistrictwiseTTcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_32;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getdistrictwiseTTcount.add(cursor.getString(cursor.getColumnIndex("DistCountTT")));
            }

            while (cursor.moveToNext());
        }
        return getdistrictwiseTTcount;
    }
    public ArrayList<String> getDistrictwisenameofTT() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getdistrictwiseTTname = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_32;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getdistrictwiseTTname.add(cursor.getString(cursor.getColumnIndex("DistrictNameTT")));
            }

            while (cursor.moveToNext());
        }
        return getdistrictwiseTTname;
    }

    public ArrayList<String> getMasterCount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> List = new ArrayList<String>();

        int i = 0;
        ArrayList<String> subCategory_list = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_13;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                List.add(cursor.getString(cursor.getColumnIndex("FieldItemValue")));
            }
            while (cursor.moveToNext());
        }
        return List;
    }

    public int getIssueID(String name) {
        String query = "select * from " + TABLE_NAME_12 + " where FieldName ='" + name + "'";
        int ids = 0;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst() != false) {
            cursor.moveToFirst();
            do {
                ids = cursor.getInt(cursor.getColumnIndex("FserverId"));

            } while (cursor.moveToNext());
        }
        return ids;
    }

    public ArrayList<String> getIssueSubCategoryList(int cid) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> subCategory_list = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_13 + " where FMServerId = " + cid;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                subCategory_list.add(cursor.getString(cursor.getColumnIndex("FieldItemValue")));

            } while (cursor.moveToNext());
        }
        return subCategory_list;
    }

    public int getSubIssueID(String name) {
        String query = "select * from " + TABLE_NAME_13 + " where FieldItemValue ='" + name + "'";
        int ids = 0;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst() != false) {
            cursor.moveToFirst();
            do {
                ids = cursor.getInt(cursor.getColumnIndex("MServerId"));

            } while (cursor.moveToNext());
        }
        return ids;
    }
    public ArrayList<String> getlockedspinnervalue() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> role_list = new ArrayList<String>();
        String query = "select * from tblFieldItemMaster2 where FMServerId = 14";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                role_list.add(cursor.getString(cursor.getColumnIndex("FieldItemValue")));

            } while (cursor.moveToNext());

        }
        return role_list;
    }


    public String getMaxID() {
        String query = "select LocalID from " + TABLE_NAME_14 + " order by LocalID desc limit 1";
        //int ids = 0;
        String S = "";
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst() != false) {
                cursor.moveToFirst();

                S = cursor.getString(cursor.getColumnIndex("LocalID"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return S;
    }

    public String getsizeoflocalid(String localID)
    {

        Cursor cursor = null;

        String val = "";
        try {
            open();
            String query = "select * from " + TABLE_NAME_14 + " where LocalID = '" + localID + "' and serverstatus = '0'";
            cursor = db.rawQuery(query, null);
            if (cursor.getCount() < 0) {

                val = String.valueOf(cursor.getCount());
            } else {
                val = String.valueOf(cursor.getCount());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return val;

    }
    public String getsizeofmilestonelocalid(String localID)
    {

        Cursor cursor = null;

        String val = "";
        try {
            open();


            String query = "select * from " + TABLE_NAME_49 + " where LocalID = '" + localID + "' and serverstatus = '0'";
            cursor = db.rawQuery(query, null);
            if (cursor.getCount() < 0) {

                val = String.valueOf(cursor.getCount());
            } else {
                val = String.valueOf(cursor.getCount());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return val;

    }

    public Cursor getFieldItem() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        cursor = db.rawQuery("select * from tblFieldItemMaster2 where FMServerId = 1", null);
        return cursor;
    }

    public Cursor getSchoolRemark(String udise) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        cursor = db.rawQuery("select * from SchoolRemark where UDISE = '" + udise + "'", null);
        return cursor;
    }

    public Cursor getFieldItemchecklist() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        cursor = db.rawQuery("select * from tblFieldItemMaster2 where FMServerId = 12", null);
        return cursor;
    }

    public String getnm(int a)//server id id a is get from parser//
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String d = "NA";
        String query = "select * from " + TABLE_NAME_13 + " where MServerId = " + a;
        Cursor cursor = db.rawQuery(query, null);
        Beans beans = new Beans();
        cursor.moveToFirst();
        try {
            d = cursor.getString(cursor.getColumnIndex("FieldItemValue"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return d;
    }

    public long insertSchoolRemark(SchoolInfoTable schoolInfoTable) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ContentValues contentValues = new ContentValues();

        contentValues.put("UDISE", schoolInfoTable.getUDISE());
        contentValues.put("SName", schoolInfoTable.getSchoolName());
        contentValues.put("Remark", schoolInfoTable.getRemark());
        contentValues.put("RoleID", schoolInfoTable.getRoleID());
        contentValues.put("RoleName", schoolInfoTable.getRoleName());
        contentValues.put("InsertByMobileNumber", schoolInfoTable.getMobile());
        contentValues.put("InsertByName", schoolInfoTable.getInsertByName());
        contentValues.put("Date", schoolInfoTable.getDate());
        contentValues.put("IMEI", schoolInfoTable.getIMEI());
        contentValues.put("Status", schoolInfoTable.getStatus());

        long i = db.insert(TABLE_NAME_16, null, contentValues);
        return i;
    }

    public void deleteSchoolRemark(SchoolInfoTable schoolInfoTable) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        db.delete(TABLE_NAME_16, null, null);
    }



    public ArrayList<SchoolInfoTable> UploadSchoolRemarkList() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<SchoolInfoTable> SchoolInformationList = new ArrayList<SchoolInfoTable>();
        SchoolInfoTable SchoolInfoTable;
        String query = "select * from " + TABLE_NAME_16 + " where Status = 0";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                SchoolInfoTable = new SchoolInfoTable(cursor.getInt(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("UDISE")),
                        cursor.getString(cursor.getColumnIndex("SName")),
                        cursor.getString(cursor.getColumnIndex("Remark")),
                        cursor.getInt(cursor.getColumnIndex("RoleID")),
                        cursor.getString(cursor.getColumnIndex("RoleName")),
                        cursor.getString(cursor.getColumnIndex("InsertByMobileNumber")),
                        cursor.getString(cursor.getColumnIndex("InsertByName")),
                        cursor.getString(cursor.getColumnIndex("IMEI")),
                        cursor.getString(cursor.getColumnIndex("Date")), false);

                SchoolInformationList.add(SchoolInfoTable);
            }
            while (cursor.moveToNext());
        }
        return SchoolInformationList;
    }

    public long updateSchoolRemark(SchoolInfoTable schoolInfoTable) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ContentValues contentValues = new ContentValues();

        contentValues.put("Status", "1");

        long i = db.update(TABLE_NAME_16, contentValues, "id = ?", new String[]{String.valueOf(schoolInfoTable.getId())});
        return i;
    }


    public ArrayList<SchoolInfoTable> getSchoolRemarkCount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ArrayList<SchoolInfoTable> remarkList = new ArrayList<SchoolInfoTable>();
        SchoolInfoTable schoolInfoTable;
        String query = "select * from " + TABLE_NAME_16;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                schoolInfoTable = new SchoolInfoTable();

                schoolInfoTable.setRemark(cursor.getString(cursor.getColumnIndex("Remark")));

                remarkList.add(schoolInfoTable);
            } while (cursor.moveToNext());
        }
        return remarkList;
    }

    public ArrayList<SchoolInfoTable> getSchoolRemarktList(String udise) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<SchoolInfoTable> remarkList = new ArrayList<SchoolInfoTable>();

        String query = "select * from " + TABLE_NAME_16 + " where UDISE = " + udise;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                SchoolInfoTable schoolInfoTable = new SchoolInfoTable();

                schoolInfoTable.setRemark(cursor.getString(cursor.getColumnIndex("Remark")));
                schoolInfoTable.setUDISE(cursor.getString(cursor.getColumnIndex("UDISE")));
                schoolInfoTable.setSchoolName(cursor.getString(cursor.getColumnIndex("SName")));
                schoolInfoTable.setRoleID(cursor.getInt(cursor.getColumnIndex("RoleID")));
                schoolInfoTable.setRoleName(cursor.getString(cursor.getColumnIndex("RoleName")));
                schoolInfoTable.setInsertBy(cursor.getString(cursor.getColumnIndex("InsertByMobileNumber")));
                schoolInfoTable.setDate(cursor.getString(cursor.getColumnIndex("Date")));

                remarkList.add(schoolInfoTable);
            } while (cursor.moveToNext());
        }
        return remarkList;
    }

    public String getUdise(String hm){
        String u = "NA";
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        String query = "select * from " + TABLE_NAME_19 +" where HMMobileNumber='"+hm+"'";
        cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                u = cursor.getString(cursor.getColumnIndex("Udisecode"));
            } while (cursor.moveToNext());
        }
        return u;
    }

    public Cursor getlabstatuslist() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        cursor = db.rawQuery("select * from tblFieldItemMaster2 where FMServerId = 30", null);
        return cursor;
    }

    public long insertSchoolInformation(SchoolInfoTable schoolInfoTable)
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Cursor cursor;
        long i;

        ContentValues contentValues = new ContentValues();

        contentValues.put("UDISE", schoolInfoTable.getUDISE());
        contentValues.put("SchoolName", schoolInfoTable.getSchoolName());
        contentValues.put("DistrictID", schoolInfoTable.getDistrictID());
        contentValues.put("DistrictName", schoolInfoTable.getDistrictName());
        contentValues.put("Zone", schoolInfoTable.getZone());
        contentValues.put("Landline", schoolInfoTable.getLandLine());
        contentValues.put("HMName", schoolInfoTable.getHMName());
        contentValues.put("HMMobileNumber", schoolInfoTable.getHMMob());
        contentValues.put("SchoolEMailID", schoolInfoTable.getSchoolEMailID());
        contentValues.put("Latitude", schoolInfoTable.getLatitude());
        contentValues.put("Longitude", schoolInfoTable.getLongitude());
        contentValues.put("Photo", schoolInfoTable.getPhoto());
        contentValues.put("Block", schoolInfoTable.getBlock());
        contentValues.put("Status", schoolInfoTable.getStatus());

        String query = "select * from " + TABLE_NAME_17 + " where UDISE = '" + schoolInfoTable.getUDISE() + "'";
        cursor = db.rawQuery(query, null);
        if (cursor.getCount() == 0) {
            i = db.insert(TABLE_NAME_17, null, contentValues);
        } else {
            i = db.update(TABLE_NAME_17, contentValues, "UDISE = ?", new String[]{schoolInfoTable.getUDISE()});
        }
        return i;
    }

    public long updateSchoolInformation(SchoolInfoTable schoolInfoTable) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        long i;

        ContentValues contentValues = new ContentValues();

        contentValues.put("SchoolName", schoolInfoTable.getSchoolName());
        contentValues.put("HMName", schoolInfoTable.getHMName());
        contentValues.put("HMMobileNumber", schoolInfoTable.getHMMob());
        contentValues.put("Photo", schoolInfoTable.getPhoto());
        contentValues.put("Status", "0");

        i = db.update(TABLE_NAME_17, contentValues, "UDISE = ?", new String[]{schoolInfoTable.getUDISE()});
        return i;
    }

    public ArrayList<SchoolInfoTable> uploadSChool() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        ArrayList<SchoolInfoTable> data = new ArrayList<SchoolInfoTable>();
        SchoolInfoTable schoolInfoTable;
        String query = "select * from " + TABLE_NAME_17 + " where Status = 0";
        cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                schoolInfoTable = new SchoolInfoTable();
                schoolInfoTable.setUDISE(cursor.getString(cursor.getColumnIndex("UDISE")));
                schoolInfoTable.setSchoolName(cursor.getString(cursor.getColumnIndex("SchoolName")));
                schoolInfoTable.setHMName(cursor.getString(cursor.getColumnIndex("HMName")));
                schoolInfoTable.setHMMob(cursor.getString(cursor.getColumnIndex("HMMobileNumber")));
                schoolInfoTable.setPhoto(cursor.getString(cursor.getColumnIndex("Photo")));

                data.add(schoolInfoTable);

            } while (cursor.moveToNext());
        }
        return data;
    }
    public ArrayList<SchoolInfoTable> getSchoolInformationList(String udise) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ArrayList<SchoolInfoTable> SchoolInformationList = new ArrayList<SchoolInfoTable>();

        String query = "select * from " + TABLE_NAME_17 + " where UDISE = " + udise;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            SchoolInfoTable schoolInfoTable = new SchoolInfoTable();

            schoolInfoTable.setUDISE(cursor.getString(cursor.getColumnIndex("UDISE")));
            schoolInfoTable.setSchoolName(cursor.getString(cursor.getColumnIndex("SchoolName")));
            schoolInfoTable.setDistrictID(cursor.getInt(cursor.getColumnIndex("DistrictID")));
            schoolInfoTable.setDistrictName(cursor.getString(cursor.getColumnIndex("DistrictName")));
            schoolInfoTable.setZone(cursor.getString(cursor.getColumnIndex("Zone")));
            schoolInfoTable.setLandLine(cursor.getString(cursor.getColumnIndex("Landline")));
            schoolInfoTable.setHMName(cursor.getString(cursor.getColumnIndex("HMName")));
            schoolInfoTable.setHMMob(cursor.getString(cursor.getColumnIndex("HMMobileNumber")));
            schoolInfoTable.setSchoolEMailID(cursor.getString(cursor.getColumnIndex("SchoolEMailID")));
            schoolInfoTable.setLatitude(cursor.getString(cursor.getColumnIndex("Latitude")));
            schoolInfoTable.setLongitude(cursor.getString(cursor.getColumnIndex("Longitude")));
            schoolInfoTable.setPhoto(cursor.getString(cursor.getColumnIndex("Photo")));
            schoolInfoTable.setBlock(cursor.getString(cursor.getColumnIndex("Block")));

            SchoolInformationList.add(schoolInfoTable);
        }
        return SchoolInformationList;
    }

    public ArrayList<String> getSchoollocation(String udise) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        ArrayList<String> locationdata = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_17 + " where UDISE = '" + udise + "'";
        cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            locationdata.add(cursor.getString(cursor.getColumnIndex("Latitude")));
            locationdata.add(cursor.getString(cursor.getColumnIndex("Longitude")));
            locationdata.add(cursor.getString(cursor.getColumnIndex("SchoolName")));
        }
        return locationdata;
    }

    public ArrayList<SchoolInfoTable> getUDISE() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        ArrayList<SchoolInfoTable> allUDISE = new ArrayList<SchoolInfoTable>();
        SchoolInfoTable schoolInfoTable;
        String query = "select * from " + TABLE_NAME_17;
        cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                schoolInfoTable = new SchoolInfoTable();
                schoolInfoTable.setUDISE(cursor.getString(cursor.getColumnIndex("UDISE")));
                schoolInfoTable.setSchoolName(cursor.getString(cursor.getColumnIndex("SchoolName")));

                allUDISE.add(schoolInfoTable);

            } while (cursor.moveToNext());
        }
        return allUDISE;
    }



    public String getLastSurveyID()
    {
        String query="";
        String lastSurveyId="0";
        Cursor cursor=null;
        try
        {
            open();
            query="SELECT * FROM evidyalayaMilestone ORDER BY id DESC LIMIT 1";
            cursor=db.rawQuery(query,null);


            if(cursor.getCount()>0)
            {
                cursor.moveToFirst();
                do
                {
                    lastSurveyId=cursor.getString(cursor.getColumnIndex("serveyid"));
                }while (cursor.moveToNext());

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return  lastSurveyId;
    }
    public String getudiseByName(String name) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select UDISE from " + TABLE_NAME_17 + " where SchoolName = '" + name + "'";
        Cursor cursor = db.rawQuery(query, null);
        SchoolInfoTable schoolInfoTable = new SchoolInfoTable();
        cursor.moveToFirst();
        String d = cursor.getString(cursor.getColumnIndex("UDISE"));
        return d;
    }

    /*public ArrayList<SchoolInfoTable> getSchoolSurveyList(String uidise)
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        ArrayList<SchoolInfoTable> SchoolSurveyList = new ArrayList<SchoolInfoTable>();
        SchoolInfoTable schoolInfoTable;
        String query = "select * from " + TABLE_NAME_21 + " where UDISE = " + uidise;
        cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            do {
                schoolInfoTable = new SchoolInfoTable();
                schoolInfoTable.setUDISE(cursor.getString(cursor.getColumnIndex("UDISE")));
                schoolInfoTable.setInsertByName(cursor.getString(cursor.getColumnIndex("VisitorName")));
                schoolInfoTable.setDate(cursor.getString(cursor.getColumnIndex("Date")));

                SchoolSurveyList.add(schoolInfoTable);

            }while (cursor.moveToNext());
        }
        return SchoolSurveyList;
    }*/

    public long insertLoginDetails(LoginDetailsBean loginDetailsBean, String zone) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ContentValues contentValues = new ContentValues();

        contentValues.put("PkRegId", loginDetailsBean.getPkregid());
        contentValues.put("Firstname", loginDetailsBean.getFirstname());
        contentValues.put("Lastname", loginDetailsBean.getLastname());
        contentValues.put("Username", loginDetailsBean.getUsername());
        contentValues.put("Roleid", loginDetailsBean.getRoleid());
        contentValues.put("Rolename", loginDetailsBean.getRolename());
        contentValues.put("Reffmobileno", loginDetailsBean.getReffmobile());
        contentValues.put("Userpassword", loginDetailsBean.getUserpassword());
        contentValues.put("Imei", loginDetailsBean.getImei());
        contentValues.put("SimSerial", loginDetailsBean.getSimserial());
        contentValues.put("Latitude", loginDetailsBean.getLati());
        contentValues.put("Longitude", loginDetailsBean.getLongi());
        contentValues.put("Insertdate", loginDetailsBean.getDate());
        contentValues.put("Email", loginDetailsBean.getEmail());
        contentValues.put("TalukaId", loginDetailsBean.getTalukaid());
        contentValues.put("Photo", loginDetailsBean.getPhoto());
        contentValues.put("Status", loginDetailsBean.getStatus());
        contentValues.put("DistrictId", loginDetailsBean.getDistrictid());
        contentValues.put("locatezone", zone);

        long i = db.insert(TABLE_NAME_20, null, contentValues);
        return i;
    }

    public String getstatus() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_20;
        String id = null;
        Cursor mCursor = db.rawQuery(query, null);
        if (mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            id = mCursor.getString(mCursor.getColumnIndex("Status"));
        }
        return id;
    }

    public String getRole()
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_20;
        String id = null;
        Cursor mCursor = db.rawQuery(query, null);
        if (mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            do {
                id = mCursor.getString(mCursor.getColumnIndex("Roleid"));

            } while (mCursor.moveToNext());
        }
        return id;
    }
    public String getzonenumber()
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_20;
        String id = null;
        Cursor mCursor = db.rawQuery(query, null);
        if (mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            do {
                id = mCursor.getString(mCursor.getColumnIndex("locatezone"));

            } while (mCursor.moveToNext());
        }
        return id;
    }

    public String gettalukafromlogin()
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_20;
        String id = null;
        Cursor mCursor = db.rawQuery(query, null);
        if (mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            do {
                id = String.valueOf(mCursor.getInt(mCursor.getColumnIndex("TalukaId")));

            } while (mCursor.moveToNext());
        }
        return id;
    }

    public String getdistrictfromlogintable()
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_20;
        String id = null;
        Cursor mCursor = db.rawQuery(query, null);
        if (mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            do {
                id = String.valueOf(mCursor.getInt(mCursor.getColumnIndex("DistrictId")));

            } while (mCursor.moveToNext());
        }
        return id;
    }

    public String getdistid() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_20;
        String id = null;
        Cursor mCursor = db.rawQuery(query, null);
        if (mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            do {
                id = mCursor.getString(mCursor.getColumnIndex("DistrictId"));

            } while (mCursor.moveToNext());
        }
        return id;
    }


    public String getdistidwithparameter(String s) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_20 + "where ";
        String id = null;
        Cursor mCursor = db.rawQuery(query, null);
        if (mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            do {
                id = mCursor.getString(mCursor.getColumnIndex("Distid"));

            } while (mCursor.moveToNext());
        }
        return id;
    }


    public void setStatusDeActive() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put("Status", 0);

        db.update(TABLE_NAME_20, contentValues, null, null);

    }

    public String getActiveUser() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String mobileNo = null;
        String query = "select Username from " + TABLE_NAME_20;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            mobileNo = cursor.getString(cursor.getColumnIndex("Username"));
        }
        System.out.println("Active user mobile no--" + mobileNo);
        return mobileNo;
    }

    public ArrayList<String> getUser() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> role_list = new ArrayList<String>();
        String query = "select Username from " + TABLE_NAME_20;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                role_list.add(cursor.getString(cursor.getColumnIndex("Username")));

            } while (cursor.moveToNext());
        }
        return role_list;
    }

    public void deleteLoginDetails(LoginDetailsBean loginDetailsBean) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        db.delete(TABLE_NAME_20, null, null);
    }


    /*public ArrayList<String> getSchoolSurveyListbyUDISE()
    {
        ArrayList<String> dateList=new ArrayList<String>();
        String sqlQuey="select Username from "+TABLE_NAME_20;
        Cursor cursor=db.rawQuery(sqlQuey, null);
        if (cursor.getCount()>0) {
            if (cursor.moveToFirst()) {
                do {
                    dateList.add(cursor.getString(1));
                } while (cursor.moveToNext());
            }
        }
        else {
            dateList.add("No Date Available");
        }
        return dateList;
    }*/

    public ArrayList<String> getSchoolSurveyListbyUDISE() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> role_list = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_21;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                role_list.add(cursor.getString(cursor.getColumnIndex("VisitorName")));
                // role_list.add(cursor.getString(cursor.getColumnIndex("UDISE")));

            } while (cursor.moveToNext());
        }
        return role_list;
    }

    public Cursor getChildList(String type) {
        Cursor mCursor = null;
        try {

            //mCursor = db.query(TABLE_NAME_3, null, "type = ?", new String[]{type}, null, null, null);
            mCursor = db.rawQuery("select SchoolName, UDISE from " + TABLE_NAME_21 + " where VisitorName = '" + type + "'", null);

        } catch (Exception e) {
            Log.e("ERROR", "getData");
            e.printStackTrace();
        }
        return mCursor;
    }

    public Cursor getSchoolSurveyList(String udise) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        cursor = db.rawQuery("select * from " + TABLE_NAME_21 + " where UDISE = '" + udise + "'", null);
        return cursor;
    }

    public RegistrationTable getDate(String mobno) {

        try {
            open();
        } catch (Exception e) {
            e.printStackTrace();
        }
        RegistrationTable registration = null;

        String query = "select * from " + TABLE_NAME_20 + " where Username = " + mobno;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                registration = new RegistrationTable();
                registration.setFirstName(cursor.getString(cursor.getColumnIndex("Firstname")));
                registration.setLastName(cursor.getString(cursor.getColumnIndex("Lastname")));
                registration.setEMail(cursor.getString(cursor.getColumnIndex("Email")));
                registration.setMobilNumber(cursor.getString(cursor.getColumnIndex("Username")));
                registration.setTalukaID(cursor.getInt(cursor.getColumnIndex("TalukaId")));
                registration.setPhoto(cursor.getString(cursor.getColumnIndex("Photo")));

            } while (cursor.moveToNext());
        }
        return registration;
    }

    public RegistrationTable getDrowerData(String mobno) {

        try {
            open();
        } catch (Exception e) {
            e.printStackTrace();
        }
        RegistrationTable registration = null;

        String query = "select * from " + TABLE_NAME_20 + " where Username = " + mobno;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                registration = new RegistrationTable();
                registration.setFirstName(cursor.getString(cursor.getColumnIndex("Firstname")));
                registration.setLastName(cursor.getString(cursor.getColumnIndex("Lastname")));
                registration.setEMail(cursor.getString(cursor.getColumnIndex("Email")));
                registration.setMobilNumber(cursor.getString(cursor.getColumnIndex("Username")));
                registration.setTalukaID(cursor.getInt(cursor.getColumnIndex("TalukaId")));
                registration.setPhoto(cursor.getString(cursor.getColumnIndex("Photo")));

            } while (cursor.moveToNext());
        }
        return registration;
    }

    public long updateData(RegistrationTable registrationTable) {
        try {
            open();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ContentValues cv = new ContentValues();

        cv.put("FirstName", registrationTable.getFirstName());
        cv.put("LastName", registrationTable.getLastName());
        cv.put("Email", registrationTable.getEMail());

        long i = db.update(TABLE_NAME_2, cv, "mobileNumber = ?", new String[]{(registrationTable.getMobilNumber())});
        return i;
    }

    public long updatephoto(LoginDetailsBean registrationTable) {
        try {
            open();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ContentValues cv = new ContentValues();

        cv.put("Photo", registrationTable.getPhoto());


        long i = db.update(TABLE_NAME_20, cv, "Username = ?", new String[]{(registrationTable.getUsername())});
        return i;
    }


    /* ,[UserMobile]
              ,[RefMobile]
              ,[IMEINumber]
              ,[InsertDate]
              ,[ModifyDate]
              ,[keyword]
              ,[RefName]
              ,[isActive]
      FROM [DBeVidyalaya].[dbo].[tblAddJuniors]*/

  /*  public DownloadDashboardBeen getdashboardcounterdatalist()//coding must getdashboardcount as per current month
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        DownloadDashboardBeen downloadDashboardBeen = new DownloadDashboardBeen();
        String query = "select * from " + TABLE_NAME_26;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0)
        {
            cursor.moveToFirst();


            downloadDashboardBeen.setSchoolServey(cursor.getString(cursor.getColumnIndex("SchoolServery")));
            downloadDashboardBeen.setSchoolCount(cursor.getString(cursor.getColumnIndex("SchoolCount")));
            downloadDashboardBeen.setTicketCount(cursor.getString(cursor.getColumnIndex("TicketCount")));
        }
        return downloadDashboardBeen;
    }*/


    public boolean setDeactiveClass(String arrylist, String refname) {
        ContentValues initialValues = new ContentValues();
        try {

            initialValues.put("isActive", "0");
            initialValues.put("Status", "1");// person active

        } catch (Exception e) {
            e.printStackTrace();
        }
        return db.update(TABLE_NAME_25, initialValues, "RefMobile = ? and refName = ?", new String[]{arrylist.toString(), refname.trim()}) > 0;

    }

    /*public boolean setActiveClass(String arrylist)*/
    public boolean setActiveClass(String arrylist, String refname) {
        ContentValues initialValues = new ContentValues();
        try {

            initialValues.put("isActive", "1"); // person active
            initialValues.put("Status", "1"); // data upload*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        return db.update(TABLE_NAME_25, initialValues, "RefMobile = ? and refName = ?", new String[]{arrylist.toString(), refname.trim()}) > 0;

    }

    public int getAllActiveFieldUnUploadedRecordCount()//count all active deactive field in database whr staus =1;
    {

        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        int count = 0;

        String query = "select * from " + TABLE_NAME_25 + " where Status ='1'";
        Cursor cursor = null;
        cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            count = cursor.getCount();
        } else {
            System.out.print("no daTA");
        }
        return count;
    }

    public boolean isServerIdAvailableforupadate(String serverId)
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        boolean recordAvailable = false;
        String query = "select * from " + TABLE_NAME_25 + " where Serverid = " + serverId;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            recordAvailable = true;
        }
        //close();
        return recordAvailable;
    }

    public boolean iszoneisalocatedforticketchange(String udisecode,String zonenumber)
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        boolean recordAvailable = false;
        String query = "select * from " + TABLE_NAME_25 + " where Serverid = " + udisecode;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0)
        {
            recordAvailable = true;
        }
        //close();
        return recordAvailable;
    }


      public Cursor getEventList(String date) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        cursor = db.rawQuery("select * from " + TABLE_NAME_15 + " where Date = '" + date + "'", null);
        return cursor;
    }

        public ArrayList<String> getEvents(String date) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        ArrayList<String> eventData = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_15 + " where Date = '" + date + "'";
        cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                eventData.add(cursor.getString(cursor.getColumnIndex("Event")));
            }
            while ((cursor.moveToNext()));
        }
        return eventData;
    }


    public ArrayList<String> EventsCount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        ArrayList<String> eventData = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_15;
        cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                eventData.add(cursor.getString(cursor.getColumnIndex("Event")));
            }
            while ((cursor.moveToNext()));
        }
        return eventData;
    }


    public Cursor getTicketLog(String tokenId) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        cursor = db.rawQuery("select * from " + TABLE_NAME_30 + " where TokenId = '" + tokenId + "'", null);
        return cursor;
    }

    public ArrayList<String> getTicketLogCount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> role_list = new ArrayList<String>();
        String query = "select * from " + TABLE_NAME_30;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                role_list.add(cursor.getString(cursor.getColumnIndex("LogDetails")));

            } while (cursor.moveToNext());
        }
        return role_list;
    }

    public ArrayList<String> getzonenamelist() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzonecount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_28;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzonecount.add(cursor.getString(cursor.getColumnIndex("Zone")));

            }

            while (cursor.moveToNext());
        }
        return getzonecount;
    }
////////////////////////////////////////////////////////////////////////////////////////////

    public ArrayList<String> getzone1TTareaname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '1'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                String str = cursor.getString(cursor.getColumnIndex("Areaname"));
               /* if (str.length() > 2)
                  str= str.substring(0,3);*/
                getzone1districtcount.add(str);

            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }


    public ArrayList<String> getzone2TTareaname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '2'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone3TTareaname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '3'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone4TTareaname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '4'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone5TTareaname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '5'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }


    public ArrayList<String> getzone6TTareaname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '6'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }
/////////////////////////////////////////////////////////////////////////////////////


    public ArrayList<String> getzone1areaname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_34 + " where referenceid = '1'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                String str = cursor.getString(cursor.getColumnIndex("Areaname"));
               /* if (str.length() > 2)
                  str= str.substring(0,3);*/
                getzone1districtcount.add(str);

            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }


    public ArrayList<String> getzone2areaname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_34 + " where referenceid = '2'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")).trim());
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone3areaname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_34 + " where referenceid = '3'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone4areaname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_34 + " where referenceid = '4'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone5areaname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_34 + " where referenceid = '5'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }


    public ArrayList<String> getzone6areaname() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_34 + " where referenceid = '6'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }


//////////////////////////////////////////////////////////////////////////////////


    public ArrayList<String> get10th1zonename() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_36 + " where referenceid = '1'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }


    public ArrayList<String> get10th2zonename() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_36 + " where referenceid = '2'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> get10th3zonename() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_36 + " where referenceid = '3'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> get10th4zonename() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_36 + " where referenceid = '4'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> get10th5zonename() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_36 + " where referenceid = '5'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }


    public ArrayList<String> get10th6zonename() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_36 + " where referenceid = '6'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Areaname")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }


    public ArrayList<String> getzone1count() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_34 + " where referenceid = '1'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("count")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone2count() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_34 + " where referenceid = '2'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("count")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone3count() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_34 + " where referenceid = '3'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("count")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone4count() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_34 + " where referenceid = '4'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("count")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone5count() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_34 + " where referenceid = '5'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("count")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone6count() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_34 + " where referenceid = '6'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("count")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone21count() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_36 + " where referenceid = '1'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("count")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone22count() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_36 + " where referenceid = '2'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("count")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone23count() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_36 + " where referenceid = '3'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("count")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone24count() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_36 + " where referenceid = '4'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("count")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone25count() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_36 + " where referenceid = '5'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("count")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> getzone26count() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_36 + " where referenceid = '6'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("count")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT1getzonecount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '1'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("TEnrolld")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT2getzonecount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '2'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("TEnrolld")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT3getzonecount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '3'";


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("TEnrolld")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT4getzonecount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '4'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("TEnrolld")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT5getzonecount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '5'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("TEnrolld")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT6getzonecount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '6'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("TEnrolld")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }


    public ArrayList<String> TT1getzonetopcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '1'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("TTopPerform")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT2getzonetopcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '2'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("TTopPerform")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT3getzonetopcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '3'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("TTopPerform")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT4getzonetopcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '4'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("TTopPerform")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT5getzonetopcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '5'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("TTopPerform")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT6getzonetopcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '6'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("TTopPerform")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT1getzonecompletedcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '1'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Tcompleted")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }


    public ArrayList<String> TT2getzonecompletedcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '2'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Tcompleted")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT3getzonecompletedcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '3'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Tcompleted")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT4getzonecompletedcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '4'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Tcompleted")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public ArrayList<String> TT5getzonecompletedcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '5'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Tcompleted")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }


    public ArrayList<String> TT6getzonecompletedcount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_45 + " where referenceid = '6'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("Tcompleted")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }


    public ArrayList<String> getmainzonecount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_28;


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("ZoneCount")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public void addzones()
    {
        try {
            open();

            String query = "select * from " + TABLE_NAME_34;
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.getCount() == 0) {

                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('BARGARH','2101','4','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('JHARSUGUDA','2102','4','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('SAMBALPUR','2103','4','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('DEOGARH','2104','4','');");


                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('SUNDERGARH','2105','5','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KEONJHAR','2106','4','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId, referenceid, count)" + " VALUES ('MAYURBHANJ','2107','1','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname, AreaId,referenceid,count)" + " VALUES ('BALASORE','2108','1','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('BHADRAK','2109','1','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KENDRAPARA','2110','2','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('JAGATSINGHPUR','2111','2','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('CUTTACK','2112','3','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('JAJPUR','2113','2','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('DHENKANAL','2114','5','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('ANGUL','2115','5','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('NAYAGARH','2116','3','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KHURDA','2117','3','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('PURI','2118','3','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('GANJAM','2119','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('GAJAPATI','2120','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KANDHAMAL','2121','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('BOUDH','2122','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('SUBARNAPUR','2123','5','');");//SONEPUR


                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('BALANGIR','2124','5','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('NUAPADA','2125','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KALAHANDI','2126','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('RAYAGADA','2127','6','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('NABARANGPUR','2128','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KORAPUT','2129','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_34 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('MALKANGIRI','2130','6','999');");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void addzonestbl2() {
        try {
            open();

            String query = "select * from " + TABLE_NAME_36;
            Cursor cursor = db.rawQuery(query, null);
            int c=cursor.getCount();

            if (cursor.getCount() == 0)
            {
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('BARGARH','2101','4','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('JHARSUGUDA','2102','4','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('SAMBALPUR','2103','4','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('DEOGARH','2104','4','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('SUNDERGARH','2105','5','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KEONJHAR','2106','4','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId, referenceid, count)" + " VALUES ('MAYURBHANJ','2107','1','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname, AreaId,referenceid,count)" + " VALUES ('BALASORE','2108','1','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('BHADRAK','2109','1','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KENDRAPARA','2110','2','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('JAGATSINGHPUR','2111','2','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('CUTTACK','2112','3','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('JAJPUR','2113','2','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('DHENKANAL','2114','5','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('ANGUL','2115','5','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('NAYAGARH','2116','3','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KHURDA','2117','3','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('PURI','2118','3','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('GANJAM','2119','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('GAJAPATI','2120','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KANDHAMAL','2121','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('BOUDH','2122','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('SUBARNAPUR','2123','5','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('BALANGIR','2124','5','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('NUAPADA','2125','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KALAHANDI','2126','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('RAYAGADA','2127','6','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('NABARANGPUR','2128','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KORAPUT','2129','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_36 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('MALKANGIRI','2130','6','1010');");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        close();
    }

    public void addTTcount() {
        try {
            open();

            String query = "select * from " + TABLE_NAME_37;
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.getCount() == 0) {

                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('BARGARH','2101','4','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('JHARSUGUDA','2102','4','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('SAMBALPUR','2103','4','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('DEOGARH','2104','4','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('SUNDERGARH','2105','5','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KEONJHAR','2106','4','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId, referenceid, count)" + " VALUES ('MAYURBHANJ','2107','1','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname, AreaId,referenceid,count)" + " VALUES ('BALASORE','2108','1','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('BHADRAK','2109','1','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KENDRAPARA','2110','2','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('JAGATSINGHPUR','2111','2','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('CUTTACK','2112','3','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('JAJPUR','2113','2','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('DHENKANAL','2114','5','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('ANGUL','2115','5','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('NAYAGARH','2116','3','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KHURDA','2117','3','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('PURI','2118','3','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('GANJAM','2119','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('GAJAPATI','2120','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KANDHAMAL','2121','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('BOUDH','2122','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('SUBARNAPUR','2123','5','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('BALANGIR','2124','5','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('NUAPADA','2125','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KALAHANDI','2126','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('RAYAGADA','2127','6','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('NABARANGPUR','2128','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('KORAPUT','2129','6','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_37 + "(Areaname,AreaId,referenceid, count)" + " VALUES ('MALKANGIRI','2130','6','111');");

//               db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID)" + " VALUES ('SPD',2);");

            }

            db.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public void addTTAllcount() {
        try {
            open();

            String query = "select * from " + TABLE_NAME_45;
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.getCount() == 0) {


                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('BARGARH','2101','4','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('JHARSUGUDA','2102','4','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('SAMBALPUR','2103','4','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('DEOGARH','2104','4','','','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('SUNDERGARH','2105','5','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('KEONJHAR','2106','4','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId, referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('MAYURBHANJ','2107','1','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname, AreaId,referenceid,TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('BALASORE','2108','1','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('BHADRAK','2109','1','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('KENDRAPARA','2110','2','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('JAGATSINGHPUR','2111','2','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('CUTTACK','2112','3','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('JAJPUR','2113','2','','','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('DHENKANAL','2114','5','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('ANGUL','2115','5','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('NAYAGARH','2116','3','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('KHURDA','2117','3','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('PURI','2118','3','','','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('GANJAM','2119','6','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('GAJAPATI','2120','6','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('KANDHAMAL','2121','6','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('BOUDH','2122','6','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('SUBARNAPUR','2123','5','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('BALANGIR','2124','5','','','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('NUAPADA','2125','6','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('KALAHANDI','2126','6','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('RAYAGADA','2127','6','','','');");

                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('NABARANGPUR','2128','6','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('KORAPUT','2129','6','','','');");
                db.execSQL("INSERT INTO " + TABLE_NAME_45 + "(Areaname,AreaId,referenceid, TEnrolld,TTopPerform,Tcompleted)" + " VALUES ('MALKANGIRI','2130','6','','','');");

//               db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID)" + " VALUES ('SPD',2);");

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }


    }



      public String getICTMob(String name) {
        String query = "select * from " + TABLE_NAME_21 + " where VisitorName = '" + name + "'";
        String ids = "";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst() != false) {
            cursor.moveToFirst();
            do {

                ids = cursor.getString(cursor.getColumnIndex("VisitorMobile"));

            } while (cursor.moveToNext());
        }
        return ids;
    }



    public void addlanguage() {
        try {
            open();

            String query = "select * from " + TABLE_NAME_41;
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.getCount() == 0) {

                //                db.execSQL("INSERT INTO " + TABLE_NAME_37+ "(Areaname,AreaId,referenceid, count)" + " VALUES ('BARGARH','2101','4','');");


                db.execSQL("INSERT INTO " + TABLE_NAME_41 + "(Language, LANGUAGENAME, LanguageID)" + " VALUES ('eng', 'English', '1');");
                db.execSQL("INSERT INTO " + TABLE_NAME_41 + "(Language, LANGUAGENAME, LanguageID)" + " VALUES ('hi', 'Hindi', '2');");
                db.execSQL("INSERT INTO " + TABLE_NAME_41 + "(Language, LANGUAGENAME, LanguageID)" + " VALUES ('ody', 'Odiya', '3');");


//               db.execSQL("INSERT INTO " + TABLE_NAME_1 + "(roleName,categoryID)" + " VALUES ('SPD',2);");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        close();
    }


    public long updatelang(String language)

    {
        long i = 0;

        ContentValues contentValues = new ContentValues();

        if (language.equals("eng")) {

            contentValues.put("LanguageID", "1");
            i = db.update(TABLE_NAME_41, contentValues, "Language = ?", new String[]{language});
            update1();


        }


        if (language.equals("ody")) {

            contentValues.put("LanguageID", "1");
            i = db.update(TABLE_NAME_41, contentValues, "Language = ?", new String[]{language});
            update2();

        }


        if (language.equals("hi")) {

            contentValues.put("LanguageID", "1");
            i = db.update(TABLE_NAME_41, contentValues, "Language = ?", new String[]{language});
            update3();

        }

        return i;
    }




    /*    public long updateVoterMobNum(String mob, String id) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put("MOB_NUMBER", mob);

        long i = database.update(TABLE_NAME_VOTER_LIST, contentValues, "TABLE_ID = ?", new String[]{id});
        return i;
    }
*/

    public String getlangid() {
        String query = "select LANGUAGENAME from " + TABLE_NAME_41 + " where LanguageID = '1'";
        String ids = "";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst() != false) {
            cursor.moveToFirst();
            do {

                ids = cursor.getString(cursor.getColumnIndex("LANGUAGENAME"));

            } while (cursor.moveToNext());
        }
        return ids;

    }


    public long update1()

    {
        long i = 0;

        ContentValues contentValues = new ContentValues();

        contentValues.put("LanguageID", "0");

        i = db.update(TABLE_NAME_41, contentValues, "Language = ? OR Language = ?", new String[]{"ody", "hi"});


        return i;
    }


    public long update2()

    {
        long i = 0;

        ContentValues contentValues = new ContentValues();

        contentValues.put("LanguageID", "0");

        i = db.update(TABLE_NAME_41, contentValues, "Language = ? OR Language = ?", new String[]{"eng", "hi"});


        return i;
    }


    public long update3()

    {
        long i = 0;

        ContentValues contentValues = new ContentValues();

        contentValues.put("LanguageID", "0");

        i = db.update(TABLE_NAME_41, contentValues, "Language = ? OR Language = ?", new String[]{"ody", "eng"});


        return i;
    }





    public String getDistID(String Dist) {
        String d = "0";
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from "+TABLE_NAME_34+" where Areaname ='"+Dist+"'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            d = cursor.getString(cursor.getColumnIndex("AreaId"));
        }
        return d;
    }

    public String getDistIDfor9th(String Dist)
    {
        String d = "0";
        String query="";
        Cursor cursor=null;
        try {
            open();

            query = "select AreaId from " + TABLE_NAME_34 + " where Areaname = '" + Dist + "'";
            cursor = db.rawQuery(query, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();

                // d=cursor.getInt(cursor.getColumnIndex("AreaId"));
                d = cursor.getString(cursor.getColumnIndex("AreaId"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return d;
    }

    public String getmaxudisId(String DistID) {
        String d = null;
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_42 + " where DistrictID = '" + DistID + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToLast();
            d = cursor.getString(cursor.getColumnIndex("SchoolUDISEID"));
        }
        return d;
    }



    public ArrayList<String> getlistofDistrictUdise(String id) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ArrayList<String> List = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_42 + " where DistrictID = '" + id + "'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                List.add(cursor.getString(cursor.getColumnIndex("SchoolUDISEID")));
            }

            while (cursor.moveToNext());
        }
        return List;
    }

       public String getTenthCount(String udise) {

        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String d = null;
        String query = "select * from " + TABLE_NAME_42 + " where SchoolUDISEID = '" + udise + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            d = cursor.getString(cursor.getColumnIndex("Tenthcount"));

        }
        return d;
    }

    public String getTeacherCount(String udise) {
        String d = null;
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_42 + " where SchoolUDISEID = '" + udise + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            d = cursor.getString(cursor.getColumnIndex("Teachercount"));

        }
        return d;
    }

    public String getperformedTeacherCount(String udise) {
        String d = null;
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_42 + " where SchoolUDISEID = '" + udise + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            d = cursor.getString(cursor.getColumnIndex("Topperformed"));

        }
        return d;
    }

    public String getcompletedTT(String udise) {
        String d = null;
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_42 + " where SchoolUDISEID = '" + udise + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            d = cursor.getString(cursor.getColumnIndex("completed"));

        }
        return d;
    }

    public long updateProfile(RegistrationTable registrationTable) {
        try {
            open();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ContentValues cv = new ContentValues();

        cv.put("Firstname", registrationTable.getFirstName());
        cv.put("Lastname", registrationTable.getLastName());
        cv.put("Email", registrationTable.getEMail());
        cv.put("Photo", registrationTable.getPhoto());

        long i = db.update(TABLE_NAME_20, cv, "Username = ?", new String[]{(registrationTable.getMobilNumber())});
        return i;
    }

    public int getTicketMaxID() {
        String query = "select id from " + TABLE_NAME_9 + " order by id desc limit 1";
        int ids = 0;
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst() != false) {
                cursor.moveToFirst();

                ids = cursor.getInt(cursor.getColumnIndex("id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ids;
    }

    public String getTicketPhoto() {
        String query = "select * from " + TABLE_NAME_9 + " order by id desc limit 1";
        String ids = "0";
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null && cursor.moveToFirst() != false) {
                cursor.moveToFirst();

                ids = cursor.getString(cursor.getColumnIndex("Photo"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ids;
    }

  /*  public ArrayList<Addjunoirbeen> getjuniorInformationList(int isactive)//get junior frm databzse
    {
        try {
            open();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        ArrayList<Addjunoirbeen> getjuniorInformationList = new ArrayList<Addjunoirbeen>();

        String query = "select * from "+TABLE_NAME_25+" where isActive = '" + isactive+"'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            do {
                Addjunoirbeen addjunoirbeen = new Addjunoirbeen();
                addjunoirbeen.setUserMobile(cursor.getString(cursor.getColumnIndex("UserMobile")));
                addjunoirbeen.setRefMobile(cursor.getString(cursor.getColumnIndex("RefMobile")));
                addjunoirbeen.setRefName(cursor.getString(cursor.getColumnIndex("RefName")));
                addjunoirbeen.setIsActive(cursor.getString(cursor.getColumnIndex("isActive")));
                getjuniorInformationList.add(addjunoirbeen);
            }
            while (cursor.moveToNext());
        }
        return getjuniorInformationList;
    }


*/

    public void updateSchoolStatus() {
        try {
            open();
            ContentValues values;
            values = new ContentValues();
            values.put("Status", 1);

            long i = db.update(TABLE_NAME_17, values, "Status = ?", new String[]{"0"});
            System.out.println(i);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    public ArrayList<String> getjuniorlist(String id) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ArrayList<String> List = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_25 + " where  isActive  = '" + id + "'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                List.add(cursor.getString(cursor.getColumnIndex("RefMobile")) + "- "
                        + cursor.getString(cursor.getColumnIndex("RefName")));
            }

            while (cursor.moveToNext());
        }
        return List;
    }

    public boolean isrecordavailable(String zonenumber, String jrnumber) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        boolean b = false;

        String query = "select * from " + TABLE_NAME_46 + " where Zonenumber  = '" + zonenumber + "' and Juniormbno = '" + jrnumber + "' and Status = '0'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            b = true;
        }

        return b;
    }

    public boolean isrecordavailable111(String zonenumber, String jrnumb) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        boolean b = false;

        String query = "select * from " + TABLE_NAME_46 + " where Zonenumber  = '" + zonenumber + "' and Juniormbno = '" + jrnumb + "' and Status = '1'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            b = true;
        }

        return b;
    }

    public boolean isrecordavailablezone(String zonenumber) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        boolean b = false;

        String query = "select * from " + TABLE_NAME_46 + " where Zonenumber  = '" + zonenumber + "'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            b = true;
        }

        return b;
    }


    public void close() {
        db.close();
    }

    public ArrayList<String> getwebserverstatus() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> getzone1districtcount = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_46 + " where serverstatus = '1'";

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                getzone1districtcount.add(cursor.getString(cursor.getColumnIndex("serverstatus")));
            }

            while (cursor.moveToNext());
        }
        return getzone1districtcount;
    }

    public int getAllNotuploadedAddNumberBeenRecordCount() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        int count = 0;
        String query = "select * from " + TABLE_NAME_46 + " where serverstatus = '0'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null) {
            cursor.moveToFirst();
            {
                count = cursor.getCount();
            }
        }
        return count;
    }


    public boolean isZoneavailable(String zonenumber) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        boolean b = false;

        String query = "select * from " + TABLE_NAME_46 + " where  Zonenumber  = '" + zonenumber + "'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            b = true;
        }

        return b;
    }

    public boolean isMobileavailable(String mob) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        boolean b = false;

        String query = "select * from " + TABLE_NAME_46 + " where  Juniormbno  = '" + mob + "'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            b = true;
        }
        return b;
    }

    public boolean isavailableButDeactive(String zonenumber, String mob) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        boolean b = false;

        String query = "select * from " + TABLE_NAME_46 + " where Juniormbno = '" + mob + "' and Zonenumber = '" + zonenumber + "' and Status = '0'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            b = true;
        }
        return b;
    }

    public String getzone() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from " + TABLE_NAME_20;
        String id = null;
        Cursor mCursor = db.rawQuery(query, null);
        if (mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            do {
                id = mCursor.getString(mCursor.getColumnIndex("locatezone"));

            } while (mCursor.moveToNext());
        }
        return id;
    }

    public ArrayList<String> getassignzonelist() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ArrayList<String> List = new ArrayList<String>();

        String query = "select * from " + TABLE_NAME_20;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {


                // String id=cursor.getString(cursor.getColumnIndex("locatezone"));

                List.add(cursor.getString(cursor.getColumnIndex("locatezone")));

               /* String[] ss = id.split(",");

                String name1 = ss[0];
*/
            }

            while (cursor.moveToNext());
        }
        return List;
    }

    public String getserveyid() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String mobileNo = null;
        String query = "select * from " + TABLE_NAME_47;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            mobileNo = cursor.getString(cursor.getColumnIndex("serveyid"));
        }
        System.out.println("serveyid--" + mobileNo);
        return mobileNo;
    }

    public String getActiverefmobile() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String mobileNo = null;
        String query = "select * from " + TABLE_NAME_20;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            mobileNo = cursor.getString(cursor.getColumnIndex("Reffmobileno"));
        }
        System.out.println("Reffmobileno--" + mobileNo);
        return mobileNo;
    }

    public boolean getstatuslocalid(String LocalId) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        boolean recordAvailable = false;
        String query = "select * from " + TABLE_NAME_14 + " where LocalID = '" + LocalId + "' and ServerStatus  = '1'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            recordAvailable = true;
        }
        //close();
        return recordAvailable;
    }

    public Cursor getuploaddata() {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        cursor = db.rawQuery("select * from Master where Record_type  = 'S'", null);

        return cursor;
    }
    public Cursor getMilestonemasterdata()
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Cursor cursor;
        cursor = db.rawQuery("select * from Master where Record_type  = 'M'", null);

        return cursor;
    }

      public long updateserverstatusactive(String localid) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ContentValues contentValues = new ContentValues();

        contentValues.put("Status", "1");

        long i = db.update(TABLE_NAME_48, contentValues, "local_id = ?", new String[]{(localid)});
        return i;
    }
    public long Milestonequeryupdate()
    {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ContentValues contentValues=new ContentValues();
        contentValues.put("ServerStatus","1");
        Cursor cursor;
        long i=0;
        String query = "select ServerStatus from " + TABLE_NAME_49 + " where ServerStatus = 0";
        cursor = db.rawQuery(query, null);
        if (cursor.getCount()>0)
        {
            i = db.update(TABLE_NAME_49, contentValues, null,null);

        }
        return i;
    }

    public String getuploadstatus()
    {

        Cursor cursor = null;

        String val = "";
        try {
            open();
            String query = "select * from " + TABLE_NAME_49 + " where  serverstatus = '0'";
            cursor = db.rawQuery(query, null);
            if (cursor.getCount() < 0) {

                val = String.valueOf(cursor.getCount());
            } else {
                val = String.valueOf(cursor.getCount());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return val;

    }

    public String getzonefromdistrict(String District)
    {
        try {
            open();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        String d ="NA";

        String query = "Select * from " + TABLE_NAME_34 + " where AreaId = '" + District + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            d = cursor.getString((cursor.getColumnIndex("referenceid")));
        }
        return d;
    }

  public ArrayList<String> getzonelistfromdistrict(String zone)
    {
        ArrayList<String> list= new ArrayList<>();
        try {
            open();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        String d =null;

        String query = "Select Areaname from " + TABLE_NAME_34 + " where referenceid = '" + zone + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            do {

                d = cursor.getString((cursor.getColumnIndex("Areaname")));
                list.add(d);
            }while(cursor.moveToNext());
        }return list;
    }
    public String getDistName(String distID){
        String d = "0";
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String query = "select * from "+TABLE_NAME_34+" where AreaId ='"+distID+"'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            d = cursor.getString(cursor.getColumnIndex("Areaname"));
        }
        return d;
    }





}