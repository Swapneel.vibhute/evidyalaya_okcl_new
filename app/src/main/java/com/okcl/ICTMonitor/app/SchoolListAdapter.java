package com.okcl.ICTMonitor.app;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhinav on 4/7/18.
 */

public class SchoolListAdapter extends ArrayAdapter<String> {
    TextView textViewName;
    ArrayList<String> list;
    private Context mCtx;
    public SchoolListAdapter(ArrayList<String> List, Context mCtx) {
        super(mCtx, R.layout.activity_listview, List);
        this.list = List;
        this.mCtx = mCtx;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //getting the layoutinflater
        LayoutInflater inflater = LayoutInflater.from(mCtx);

        //creating a view with our xml layout
        View listViewItem = inflater.inflate(R.layout.activity_listview, null, true);

        //getting text views
        textViewName = (TextView) listViewItem.findViewById(R.id.listText);
        textViewName.setText(list.toString());

        //returning the listitem
        return listViewItem;
    }


}
