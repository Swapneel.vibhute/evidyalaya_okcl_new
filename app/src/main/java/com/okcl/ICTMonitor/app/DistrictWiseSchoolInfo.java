package com.okcl.ICTMonitor.app;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import WebServices.downloadSchoolForWorm;
import WebServices.getSchoolUdise;

/*
* This Activity Designed for to Download  District wise School List.
*
*/

public class DistrictWiseSchoolInfo extends AppCompatActivity {
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message message) {
            JSONArray SchoolName,udise ;
            String sc[] ;
            super.handleMessage(message);
            if (message.what == 1) {
                if (message.obj != null) {
                    try {
                        dbAdapter = new DBSchoolAdaptor(getApplicationContext());
                        JSONObject jsonObject = new JSONObject(message.obj.toString());
                        udise = jsonObject.getJSONArray("UDISECode");
                        String s ,u;
                        for(int i = 0; i < udise.length() ; i++){
                                u = udise.getString(i).toString();
                               sc =  dbAdapter.getschoolName(u.trim()).split("-");
                                s =  sc[0] + " - " + u.toString();
                                list.add(s.toString());
                                l.add(String.valueOf(i + 1)+". ");
                        }
                       /* l = list ;
                        aa = new ArrayAdapter<String>(DistrictWiseSchoolInfo.this, android.R.layout.simple_list_item_1, l);
                        schoolList.setAdapter(aa);*/
                        customeAdapter = new CustomeAdapter(DistrictWiseSchoolInfo.this,list,l);
                        schoolList.setAdapter(customeAdapter);
                    }catch (JSONException err){
                        Toast.makeText(getApplicationContext(), "Somthing is wrong", Toast.LENGTH_SHORT).show();
                    }
                }
            }else if (message.what == 6) {
                if (message.obj != null) {
                    try {
                        dbAdapter = new DBSchoolAdaptor(getApplicationContext());
                        JSONObject jsonObject = new JSONObject(message.obj.toString());
                        udise = jsonObject.getJSONArray("UDISE_CODE");
                        String s ,u;
                        for(int i = 0; i < udise.length() ; i++){
                            u = udise.getString(i).toString();
                            sc =  dbAdapter.getschoolName(u.trim()).split("-");
                            s =  sc[0] + " - " + u.toString();
                            list.add(s.toString());
                            l.add(String.valueOf(i + 1)+". ");
                        }

                        customeAdapter = new CustomeAdapter(DistrictWiseSchoolInfo.this,list,l);
                        schoolList.setAdapter(customeAdapter);
/*                        l = list ;
                        aa = new ArrayAdapter<String>(DistrictWiseSchoolInfo.this, android.R.layout.simple_list_item_1, l);
                        schoolList.setAdapter(aa);*/

                    }catch (JSONException err){
                        Toast.makeText(getApplicationContext(), "Somthing is wrong", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    };

    ListView schoolList;
    TextView tv;
    ImageView searchView;
    ArrayList<String> list = new ArrayList<>();
    ArrayList<String> l = new ArrayList<>();
    DBSchoolAdaptor dbAdapter;
    DBAdapter dbAdapter1;
    ArrayAdapter<String> aa;
    SchoolListAdapter adapter;
    String distName;
    String str;
    String label ;
    CustomeAdapter customeAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_district_wise_school_info);

        new ToolbarConfigurer(DistrictWiseSchoolInfo.this, (Toolbar) findViewById(R.id.toolbar1), true);

        Intent intent = getIntent();
        str = intent.getStringExtra("dist");
        schoolList = (ListView) findViewById(R.id.schoolList);
        label = intent.getStringExtra("label");
        tv = (TextView) findViewById(R.id.listTitle);
        try {
            if (label.equals("Internet")) {
                label = "1";
                tv.setText("   Attendance By Internet   ");
            } else if (label.equals("sms")) {
                label = "0";
                tv.setText("   Attendance By Sms   ");
            } else if (label.equals("Unique")) {
                label = "2";
                tv.setText("   Total Unique Attendance   ");
            } else {
                tv.setVisibility(View.GONE);
                label = "2";
            }

            if (getIntent().getBooleanExtra("worm", true)) {
                if (isInternetOn()) {
                    new downloadSchoolForWorm(DistrictWiseSchoolInfo.this, DistrictWiseSchoolInfo.this, str.trim(), handler).execute("");
                } else {
                    Toast.makeText(this, "Please check internet connetion", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (isInternetOn()) {
                    new getSchoolUdise(DistrictWiseSchoolInfo.this, DistrictWiseSchoolInfo.this, str.trim(), label.trim(), handler).execute("");
                } else {
                    Toast.makeText(this, "Please check your internet connetion", Toast.LENGTH_SHORT).show();
                }
/*        try {
            dbAdapter = new DBSchoolAdaptor(getApplicationContext());
            dbAdapter1 = new DBAdapter(getApplicationContext(), DBAdapter.DATABASE_NAME, null, DBAdapter.DATABASE_VERSION, DBAdapter.db);
            distName = dbAdapter1.getDistID(str.trim());
            String id = distName.trim();
            //dbAdapter.getSchoolCode(id.trim());
            list = dbAdapter.getSCNames(id.trim());
//                Toast.makeText(this, "Name "+list, Toast.LENGTH_SHORT).show();
            l = list;
            aa = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, l);
            schoolList.setAdapter(aa);
        } catch (Exception err) {
            err.printStackTrace();
            Toast.makeText(this, "Error :" + err, Toast.LENGTH_SHORT).show();
        }*/
            }
            setTitle("School list (" + str + ")");
        }catch(Exception e){

        }
        schoolList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                String s = list.get(position).toString();
                //Toast.makeText(DistrictWiseSchoolInfo.this, ""+s, Toast.LENGTH_SHORT).show();
                String udise[] = s.replace(" - ", "-").split("-");
                if (udise[1].equals("")) {
                    Toast.makeText(DistrictWiseSchoolInfo.this, "School information is not found", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(DistrictWiseSchoolInfo.this, SchoolDetails.class);
                    intent.putExtra("UDISE", udise[1]);
                    startActivity(intent);
                }
                // finish();

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*getMenuInflater().inflate(R.menu.addsearch, menu);
        return super.onCreateOptionsMenu(menu);*/

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.addsearch, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        android.support.v7.widget.SearchView searchView = (android.support.v7.widget.SearchView)
                menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setFocusable(true);
        searchView.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        searchView.setQueryHint("Enter Udise OR School Name");

        searchView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( KeyEvent.KEYCODE_ENTER == keyCode)
                {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow
                            (findViewById(android.R.id.content).getWindowToken(), 0);
                }
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //DistrictWiseSchoolInfo.this.aa.getFilter().filter(newText);
                customeAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(findViewById(android.R.id.content).getWindowToken(), 0);
            super.onBackPressed();
        }
        return true;
    }


    public boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {

            return false;
        }
        return false;
    }


    public static class ToolbarConfigurer implements View.OnClickListener {
        private Activity activity;

        public ToolbarConfigurer(final Activity activity, Toolbar toolbar, boolean displayHomeAsUpEnabled) {
            // toolbar.setTitle((this.activity = activity).getTitle());
            if (!displayHomeAsUpEnabled) return;
            //toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
            toolbar.setNavigationOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            // Toast.makeText(activity, "back", Toast.LENGTH_SHORT).show();
            // NavUtils.navigateUpFromSameTask(activity);
        }
    }


    public static class CustomeAdapter extends BaseAdapter implements Filterable{

        private Activity activity;
        private static LayoutInflater inflater = null;
        public ArrayList<String> custData,bean;
        public ArrayList<String> sno ;
        ArrayList<String> b = new ArrayList<>();
        ValueFilter valueFilter;
        public CustomeAdapter(Activity a, ArrayList<String> bean,ArrayList<String> s) {
            activity = a;
            custData = bean;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.bean = new ArrayList<>();
            this.bean.addAll(custData);
            b = bean ;
            sno = s;
        }

        @Override
        public int getCount() {
            return bean.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View vi = inflater.inflate(R.layout.customerlistrow, parent, false);

            TextView srno = (TextView) vi.findViewById(R.id.srno); // title
            TextView scname = (TextView) vi.findViewById(R.id.scname); // artist name
            srno.setText(String.valueOf(sno.get(position)));
            scname.setText(bean.get(position).toString());
            return vi;
        }

        @Override
        public Filter getFilter() {
            if (valueFilter == null) {
                valueFilter = new ValueFilter();
            }
            return valueFilter;
        }

        private class ValueFilter extends Filter
        {
            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                FilterResults results = new FilterResults();

                if (constraint != null && constraint.length() > 0)
                {
                    ArrayList<String> filterList = new ArrayList<>();
                    for (int i = 0; i < b.size(); i++)
                    {
                        if (    ((b.get(i).toUpperCase()).contains(constraint.toString().toUpperCase())))
                        {
                            filterList.add(b.get(i));
                        }
                    }
                    results.count = filterList.size();
                    results.values = filterList;
                }
                else
                {
                    results.count = b.size();
                    results.values = b;
                }
                return results;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                bean = (ArrayList<String>) results.values;
                notifyDataSetChanged();
            }
        }
    }
}
