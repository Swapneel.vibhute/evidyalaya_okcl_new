package com.okcl.ICTMonitor.app;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import Support.SharedValues;
import WebServices.DownloadSchoolDetails;
import WebServices.DownloadSchoolDetails2;

/*
*   This Activity will Designed School Details page Displays.
* 	That will  class shows the Student Learning , Teacher Learning ,
* 	Tickets ,HM Details, and SC Details.
*
*/

public class SchoolDetails extends AppCompatActivity {
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                if (msg.obj != null) {
                    data1 = msg.obj.toString();
                    new SharedValues(getApplicationContext()).saveSharedPreference("schoolinfo", data1);
                    try {
                        new DownloadSchoolDetails2(SchoolDetails.this, SchoolDetails.this, udise, handler).execute("");
                    }catch(Exception er){
                        er.printStackTrace();
                        Toast.makeText(SchoolDetails.this, "School information is not available", Toast.LENGTH_SHORT).show();
                    }
                    //showDetailsOfSchool(data1);
                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }
            }else if(msg.what == 2){
                if(msg.obj != null){
                    data2 = msg.obj.toString();
                    Log.i("Data 1 ","*"+data1);
                    Log.i("Data 2 ","**"+data2);
                    new SharedValues(getApplicationContext()).saveSharedPreference("schoolinfo2", data2);
                    showDetailsOfSchool(data1,data2);
                }else{
                    Toast.makeText(SchoolDetails.this, "No Data Found", Toast.LENGTH_SHORT).show();
                    new android.support.v7.app.AlertDialog.Builder(SchoolDetails.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Closing Activity")
                            .setMessage("School information is not available")
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    SchoolDetails.super.onBackPressed();
                                }
                            });
                }
            }
        }
    };
    TextView school,udiseCode,dist,zone,hname,hmobile,scname,scmobile,ninth,tenth,enroll,pass,topen,tclose,oopen,oclose,showError;
    String data1,data2;
    String udise;
    DBAdapter database;
    String role ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_details);
        setTitle("School Details");
        Intent intent = getIntent();
        udise = intent.getStringExtra("UDISE");
        showError = (TextView) findViewById(R.id.offlineError);
        school = (TextView) findViewById(R.id.textSchool);
        udiseCode = (TextView) findViewById(R.id.textUdise);
        dist = (TextView) findViewById(R.id.textDist);
        zone = (TextView) findViewById(R.id.textZone);
        hname = (TextView) findViewById(R.id.textHName);
        hmobile = (TextView) findViewById(R.id.textHMobile);
        scname = (TextView) findViewById(R.id.textSCName);
        scmobile = (TextView) findViewById(R.id.textSCMobile);
        ninth = (TextView) findViewById(R.id.text9thcount);
        tenth = (TextView) findViewById(R.id.text10thcount);
        enroll = (TextView) findViewById(R.id.textEnrolled);
        pass = (TextView) findViewById(R.id.textPassed);
        topen = (TextView) findViewById(R.id.textTicketOpen);
        tclose = (TextView) findViewById(R.id.textTicketClose);
        oopen = (TextView) findViewById(R.id.textOverdueOpen);
        oclose = (TextView) findViewById(R.id.textOverdueClose);
        database = new DBAdapter(getApplicationContext(), DBAdapter.DATABASE_NAME, null, DBAdapter.DATABASE_VERSION, DBAdapter.db);

        Toolbar toolbar= (Toolbar) findViewById(R.id.toolbar2);
       role = new SharedValues(this).loadSharedPreference_UserRole();
        if(role.equals("2")){
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

            if (getSupportActionBar()==null){
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
            }

        }else if(role.equals("5") || role.equals("8")){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            if (getSupportActionBar()==null){
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        }


//        new ToolbarConfigurer(SchoolDetails.this, (Toolbar) findViewById(R.id.toolbar2), true);

        if(isInternetOn()){
//            Toast.makeText(this, "UDISE " +udise, Toast.LENGTH_SHORT).show();
            try {
                if(udise.equals("")){
                    Toast.makeText(this, "School information is not found", Toast.LENGTH_SHORT).show();
                }else {
                    new DownloadSchoolDetails(SchoolDetails.this, SchoolDetails.this, udise, handler).execute("");
                }
            }catch (Exception er){
                er.printStackTrace();
                Toast.makeText(this, "School information is not available", Toast.LENGTH_SHORT).show();
            }
        }else{//            String str;
            Toast.makeText(this, "Check your inernet Connection", Toast.LENGTH_SHORT).show();
           // showError.setVisibility(View.VISIBLE);
            data1 = new SharedValues(getApplicationContext()).loadSharedPreference_SchoolInformation();
            data2 = new SharedValues(getApplicationContext()).loadSharedPreference_SchoolInformation2();
            showDetailsOfSchool(data1,data2);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        String role = new SharedValues(this).loadSharedPreference_UserRole();
        MenuItem item = menu.findItem(R.id.item2);
        MenuItem itemlogout = menu.findItem(R.id.item3);
        MenuItem itemrefresh = menu.findItem(R.id.item1);

        if(role.equals("2")) {
            itemlogout.setVisible(true);
        }
        item.setVisible(false);
        itemrefresh.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.item3){
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Closing Activity")
                    .setMessage("Are you sure you want to Log Out?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            database.setStatusDeActive();

                            /*adapter.juniordelete(addjunoirbeen);
                            adapter.assigntabledelete(assignbeen);*/

                            startActivity(new Intent(SchoolDetails.this, LoginActivity.class));

                            Toast.makeText(SchoolDetails.this, "Logout Successfully", Toast.LENGTH_SHORT).show();
                            new SharedValues(getApplicationContext()).saveSharedPreference("userRole","NA");
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        }
        if(item.getItemId()==android.R.id.home){
         //   String role = new SharedValues(this).loadSharedPreference_UserRole();
            if(role.trim().equals("2")) {
                new android.support.v7.app.AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Closing Activity")
                        .setMessage("Are you sure you want to Exit?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }else{
                super.onBackPressed();
            }
        }
//        onBackPressed();
        return true;
    }
    @Override
    public void onBackPressed() {
       // String role = new SharedValues(this).loadSharedPreference_UserRole();
        if(role.trim().equals("2")) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Closing Activity")
                    .setMessage("Are you sure you want to Exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }else{
            super.onBackPressed();
        }
    }

    public void showDetailsOfSchool(String str, String str2){
        try {
            JSONObject jsonObject;
            //String info ;
            if((str == null) || (str2 == null)){
                Toast.makeText(this, "School information is not found", Toast.LENGTH_SHORT).show();
            }else{
                jsonObject = new JSONObject(str);
                String ududiseCode1[] = new String[1];
                ududiseCode1 = jsonObject.getString("UdiseCode").replace("[", "").replace("]", "").replace("\"","").split(",");
                udiseCode.setText(ududiseCode1[0].toString());

                ududiseCode1 = jsonObject.getString("Zone").replace("[", "").replace("]", "").replace("\"","").split(",");
                zone.setText(ududiseCode1[0].toString());

                ududiseCode1 = jsonObject.getString("District_Name").replace("[", "").replace("]", "").replace("\"","").split(",");
                dist.setText(ududiseCode1[0].toString());

                ududiseCode1 = jsonObject.getString("HM_Name").replace("[", "").replace("]", "").replace("\"","").split(",") ;
                hname.setText(ududiseCode1[0].toString());

                ududiseCode1 = jsonObject.getString("HM_Mobile").replace("[", "").replace("]", "").replace("\"","").split(",");
                hmobile.setText(ududiseCode1[0].toString());

                jsonObject = new JSONObject(str2);

                ududiseCode1 = jsonObject.getString("SchoolName").replace("[", "").replace("]", "").replace("\"","").split(",");
                school.setText(ududiseCode1[0].toString().trim().toUpperCase());

                ududiseCode1 = jsonObject.getString("_9th").replace("[", "").replace("]", "").replace("\"","").split(",");
                ninth.setText(ududiseCode1[0].toString());

                ududiseCode1 = jsonObject.getString("_10th").replace("[", "").replace("]", "").replace("\"","").split(",");
                tenth.setText(ududiseCode1[0].toString());

                ududiseCode1 = jsonObject.getString("ScName").replace("[", "").replace("]", "").replace("\"","").split(",") ;
                scname.setText(ududiseCode1[0].toString());


                ududiseCode1 = jsonObject.getString("Sc_Mobile").replace("[", "").replace("]", "").replace("\"","").split(",");
                scmobile.setText(ududiseCode1[0].toString());

                ududiseCode1 = jsonObject.getString("TeacherPassed").replace("[", "").replace("]", "").replace("\"","").split(",");
                pass.setText(ududiseCode1[0].toString());

                ududiseCode1 = jsonObject.getString("OverdueClosed").replace("[", "").replace("]", "").replace("\"","").split(",");
                oclose.setText(ududiseCode1[0].toString());

                ududiseCode1 = jsonObject.getString("TeacherEnrolled").replace("[", "").replace("]", "").replace("\"","").split(",");
                enroll.setText(ududiseCode1[0].toString());

                ududiseCode1 = jsonObject.getString("TicketClosed").replace("[", "").replace("]", "").replace("\"","").split(",");
                tclose.setText(ududiseCode1[0].toString());

                ududiseCode1 = jsonObject.getString("Overdue").replace("[", "").replace("]", "").replace("\"","").split(",");
                oopen.setText(ududiseCode1[0].toString());


                ududiseCode1 = jsonObject.getString("TicketOpen").replace("[", "").replace("]", "").replace("\"","").split(",");
                topen.setText(ududiseCode1[0].toString());

            }

            Log.i("UD"," "+udiseCode);
            Log.i("Zone"," "+zone);
        }catch(JSONException err){

        }
    }
    public boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {

            return false;
        }
        return false;
    }
    /*public static class ToolbarConfigurer implements View.OnClickListener {
        private Activity activity;

        public ToolbarConfigurer(final Activity activity, Toolbar toolbar, boolean displayHomeAsUpEnabled) {
           // toolbar.setTitle((this.activity = activity).getTitle());
            if (!displayHomeAsUpEnabled) return;
           // toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
            toolbar.setNavigationOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            // Toast.makeText(activity, "back", Toast.LENGTH_SHORT).show();
            // NavUtils.navigateUpFromSameTask(activity);
        }
    }
*/
}
