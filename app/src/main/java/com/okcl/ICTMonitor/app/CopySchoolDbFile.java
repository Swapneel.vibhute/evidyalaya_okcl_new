package com.okcl.ICTMonitor.app;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by abhinav on 12/12/16.
 */
public class CopySchoolDbFile
{
    AssetManager assetmanager;
    String b;
    InputStream inputStream;
    private Context mcontext;

    public CopySchoolDbFile(Context context) {
        this.mcontext = context;
        assetmanager = mcontext.getAssets();
    }

    public void copySchoolDB() {
        try {
            File dbFile = new File("SchoolsDB");    //FILENAME
            File exportDir = new File(Environment.getDataDirectory() + "/data/" + mcontext.getPackageName() + "/databases", "");  //create file at run time
            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }
            File gpxfile = new File(exportDir, dbFile.getName());
            try {
                gpxfile.createNewFile();
                //this.copyFile(dbFile, file);
                b = "1";
            } catch (IOException e) {
                Log.e("mypck", e.getMessage(), e);
                b = "2";
            }
            try {
                inputStream = assetmanager.open("SchoolsDB.DB");//"assetFile.txt"
                if (inputStream != null)
                    Log.i("k", "It worked!");
                saveToFile(inputStream, gpxfile);
            } catch (IOException e) {
                e.printStackTrace();
            }
            // loadFromFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveToFile(InputStream is, File f) {
        int size = 0;
        byte[] buffer = null;
        try {
            size = is.available();
            buffer = new byte[size];
            is.read(buffer);

        } catch (IOException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        try {
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(buffer);
            fos.close();
            Log.i("File Write", "Success");
        } catch (IOException e) {
            Log.e("Controller", e.getMessage() + e.getLocalizedMessage() + e.getCause());
        }
    }
}
