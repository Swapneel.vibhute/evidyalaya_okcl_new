package com.okcl.ICTMonitor.app;

import android.app.Activity;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Highlight;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import Support.SharedValues;
import WebServices.DownlaodGetdistwormdata;
import WebServices.DownloadDistWiseIssueState;
import WebServices.DownloadDistWiseSCAttendancestring;
import WebServices.DownloadTeacherTrainingCounts;
import WebServices.DownloadZoneWiseSchoolCoordinatorAttendance;
import WebServices.DownloadbarDistWiseStudentLearning9th10th1617;

/*
*   This Activity will Designed for Displaying All Zone wise and District wise Graph.
*/

public class DistWiseGraph extends AppCompatActivity {
    Handler Dhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                if (msg.obj != null) {
        /*            if(lineChart.equals("true")){
                        drawDistWiseOffline(msg.obj.toString());
                    }else{
//                        new SharedValues(getApplicationContext()).saveSharedPreference("districtWiseSC", msg.obj.toString().trim());
                        new SharedValues(getApplicationContext()).saveSharedPreference("studentlearning",msg.obj.toString().trim());
                        switch(zone){
                            case "Zone 1":{
                                labels = dbAdapter.getzone1areaname();
                                setTitle("Zone 1 District Attendance");
                                drawDistrictWise(msg.obj.toString(),labels);
                                break;
                            }
                            case "Zone 2":{
                                labels = dbAdapter.getzone2areaname();
                                setTitle("Zone 2 District Attendance");
                                drawDistrictWise(msg.obj.toString(),labels);
                                break;
                            }
                            case "Zone 3":{
                                labels = dbAdapter.getzone3areaname();
                                setTitle("Zone 3 District Attendance");
                                drawDistrictWise(msg.obj.toString(),labels);
                                break;
                            }
                            case "Zone 4":{
                                labels = dbAdapter.getzone4areaname();
                                setTitle("Zone 4 District Attendance");
                                drawDistrictWise(msg.obj.toString(),labels);
                                break;
                            }
                            case "Zone 5":{
                                labels = dbAdapter.getzone5areaname();
                                setTitle("Zone 5 District Attendance");
                                drawDistrictWise(msg.obj.toString(),labels);
                                break;
                            }
                            case "Zone 6":{
                                labels = dbAdapter.getzone6areaname();
                                setTitle("Zone 6 District Attendance");
                                drawDistrictWise(msg.obj.toString(),labels);
                                break;
                            }
                        }
                    }*/
                    new SharedValues(getApplicationContext()).saveSharedPreference("zonewise",msg.obj.toString().trim());
                    drawZoneWiseSCAttendance(msg.obj.toString());
                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }
            }else if(msg.what == 2){
                if (msg.obj != null) {
                    new SharedValues(getApplicationContext()).saveSharedPreference("districtWiseWorm", msg.obj.toString().trim());
                    switch(zone){
                        case "Zone 1":{
                            labels = dbAdapter.getzone1areaname();
                            lbl.setText("Zone 1");
                            drawDistrictWiseWorm(msg.obj.toString(),labels);
                            break;
                        }
                        case "Zone 2":{
                            labels = dbAdapter.getzone2areaname();
                            lbl.setText("Zone 2");
                            drawDistrictWiseWorm(msg.obj.toString(),labels);
                            break;
                        }
                        case "Zone 3":{
                            labels = dbAdapter.getzone3areaname();
                            lbl.setText("Zone 3");
                            drawDistrictWiseWorm(msg.obj.toString(),labels);
                            break;
                        }
                        case "Zone 4":{
                            labels = dbAdapter.getzone4areaname();
                            lbl.setText("Zone 4");
                            drawDistrictWiseWorm(msg.obj.toString(),labels);
                            break;
                        }
                        case "Zone 5":{
                            labels = dbAdapter.getzone5areaname();
                            lbl.setText("Zone 5");
                            drawDistrictWiseWorm(msg.obj.toString().trim(),labels);
                            break;
                        }
                        case "Zone 6":{
                            labels = dbAdapter.getzone6areaname();
                            lbl.setText("Zone 6");
                            drawDistrictWiseWorm(msg.obj.toString().trim(),labels);
                            break;
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }
            }else if(msg.what == 3){
                if (msg.obj != null) {
                    new SharedValues(getApplicationContext()).saveSharedPreference("districtWise9_10", msg.obj.toString().trim());
                    drawDistrictWise9_10(msg.obj.toString());
                } else {
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }
            }else if(msg.what == 4){
                if(msg.obj!=null) {
                    new SharedValues(getApplicationContext()).saveSharedPreference("districtWiseSC", msg.obj.toString().trim());
                    switch (zone) {
                        case "Zone 1": {
                            labels = dbAdapter.getzone1areaname();
                            lbl.setText("Zone 1");
                            drawDistrictWise(msg.obj.toString(), labels);
                            break;
                        }
                        case "Zone 2": {
                            labels = dbAdapter.getzone2areaname();
                            lbl.setText("Zone 2");
                            drawDistrictWise(msg.obj.toString(), labels);
                            break;
                        }
                        case "Zone 3": {
                            labels = dbAdapter.getzone3areaname();
                            lbl.setText("Zone 3");
                            drawDistrictWise(msg.obj.toString(), labels);
                            break;
                        }
                        case "Zone 4": {
                            labels = dbAdapter.getzone4areaname();
                            lbl.setText("Zone 4");
                            drawDistrictWise(msg.obj.toString(), labels);
                            break;
                        }
                        case "Zone 5": {
                            labels = dbAdapter.getzone5areaname();
                            lbl.setText("Zone 5");
                            drawDistrictWise(msg.obj.toString(), labels);
                            break;
                        }
                        case "Zone 6": {
                            labels = dbAdapter.getzone6areaname();
                            lbl.setText("Zone 6");
                            drawDistrictWise(msg.obj.toString(), labels);
                            break;
                        }
                    }
                }
            }else if(msg.what == 5){
               if(msg.obj != null){
                    new SharedValues(getApplicationContext()).saveSharedPreference("DistrictWiseTT",msg.obj.toString());
                    drawZoneWiseTTGraph(msg.obj.toString());
               }
            }else if(msg.what == 6){
                if(msg.obj != null){
                   // Toast.makeText(DistWiseGraph.this, "result : "+msg.obj.toString(), Toast.LENGTH_SHORT).show();
                    new SharedValues(getApplicationContext()).saveSharedPreference("DistrictWiseIssueState",msg.obj.toString());
                    draweZoneWiseIssueState(msg.obj.toString());
                }
            }
        }
    };
    BarChart distWiseChartVer,distWiseChartHor;
    SharedValues sv = new SharedValues(this);
    TextView tv,txt1,txt2,txt3,lbl,val1,val2,val3,title,barTitle;
    DBAdapter dbAdapter;
    String zone;
    ArrayList<String> labels;
    String lineChart;
    YAxis y;
    YAxis y1;
    boolean isFlag,isZoneWiseGraph,isZoneWiseTT,isTT,isIssueState,isZoneWiseIssueState;
    int count1 = 0, count2 =0 , count3 = 0;
    SwipeRefreshLayout swipeContainer;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            swipeContainer.setEnabled(true);
            if(isZoneWiseGraph == true){
                isZoneWiseGraph = false;
                if(isInternetOn()){
                    new DownloadZoneWiseSchoolCoordinatorAttendance(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
                }else{
                    Toast.makeText(this, "You are offline,\n please press again to go back", Toast.LENGTH_SHORT).show();
//                tv.setVisibility(View.VISIBLE);
                }

            }else if(isZoneWiseTT == true){
                isZoneWiseTT= false;
                //Toast.makeText(this, "going to TTZOne", Toast.LENGTH_SHORT).show();
                if(isInternetOn()){
                    new DownloadTeacherTrainingCounts(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
                }else{
                    Toast.makeText(this, "You are offline,\n please press again to go back", Toast.LENGTH_SHORT).show();
                }

            }else if(isZoneWiseIssueState == true){
                isZoneWiseIssueState = false;
                if(isInternetOn()){
                    new DownloadDistWiseIssueState(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
                }else{
                    Toast.makeText(this, "You are offline,\n please press again to go back", Toast.LENGTH_SHORT).show();
                }
            }else{
                Intent intent = new Intent(DistWiseGraph.this,NewDashBoardGraphActivity.class);
                intent.putExtra("fromLogin", false);
                startActivity(intent);
                finish();
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        swipeContainer.setEnabled(true);
        if(isZoneWiseGraph == true){
            isZoneWiseGraph = false;
            if(isInternetOn()){
                new DownloadZoneWiseSchoolCoordinatorAttendance(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
            }else{
                Toast.makeText(this, "Check your inernet Connection", Toast.LENGTH_SHORT).show();
//                tv.setVisibility(View.VISIBLE);
            }

        }else if(isZoneWiseTT == true){
            isZoneWiseTT= false;
            //Toast.makeText(this, "going to TTZOne", Toast.LENGTH_SHORT).show();
            if(isInternetOn()){
                new DownloadTeacherTrainingCounts(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
            }else{
                Toast.makeText(this, "Please check your internet connetion", Toast.LENGTH_SHORT).show();
            }

        }else if(isZoneWiseIssueState == true){
            isZoneWiseIssueState = false;
            if(isInternetOn()){
                new DownloadDistWiseIssueState(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
            }else{
                Toast.makeText(this, "please check your internet connetion", Toast.LENGTH_SHORT).show();
            }
        }else{
            Intent intent = new Intent(DistWiseGraph.this,NewDashBoardGraphActivity.class);
            intent.putExtra("fromLogin", false);
            startActivity(intent);
            finish();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dist_wise_graph);
        new ToolbarConfigurer(DistWiseGraph.this, (Toolbar) findViewById(R.id.toolbar), true);
        setTitle("District Wise Dashboard");
/*
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DistWiseGraph.this, "AFasd", Toast.LENGTH_SHORT).show();

            }
        });*/
//        setSupportActionBar(toolbar);
       /* getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);*/

        distWiseChartVer= (BarChart) findViewById(R.id.distWiseSC);
        distWiseChartHor= (BarChart) findViewById(R.id.horizonDistWiseBar);
        tv = (TextView) findViewById(R.id.offlineErr);
        txt1 = (TextView) findViewById(R.id.text1);
        txt2 = (TextView) findViewById(R.id.text2);
        txt3 = (TextView) findViewById(R.id.text3);
        val1 = (TextView) findViewById(R.id.value1);
        val2 = (TextView) findViewById(R.id.value2);
        val3 = (TextView) findViewById(R.id.value3);
        barTitle = (TextView) findViewById(R.id.barTitle);

        lbl = (TextView) findViewById(R.id.label);
        title = (TextView) findViewById(R.id.title) ;
        dbAdapter = new DBAdapter(getApplicationContext(), DBAdapter.DATABASE_NAME, null, DBAdapter.DATABASE_VERSION, DBAdapter.db);
        Intent intent = getIntent();
        String barChart = intent.getStringExtra("isBarchart");
        String barchart1 = intent.getStringExtra("isbarchart1");
        final String barChart3 = intent.getStringExtra("isBarchart3");
        String TT = intent.getStringExtra("TT");
        String barChart2 = intent.getStringExtra("isBarchart2");
        lineChart = intent.getStringExtra("lineBar");
        zone = intent.getStringExtra("zone");
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        //String barchart2 = intent.getStringExtra("isBarchart2");
//        Toast.makeText(this, "Zone : "+zone, Toast.LENGTH_SHORT).show();
        isZoneWiseIssueState = false ;
        isZoneWiseTT = false ;
        isZoneWiseGraph = false ;
        isIssueState = true ;
        isFlag = true ;
        isTT = true ;


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeContainer.setColorSchemeResources(R.color.sms);
                if(isInternetOn()){
                    Intent intent = getIntent();
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    startActivity(intent);
                }else{
                    Toast.makeText(DistWiseGraph.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                }

                swipeContainer.setRefreshing(false);

//                DistWiseGraph.this.overridePendingTransition(0, 0);
            }
        });
        if(barChart3.equals("true")){
            if (isInternetOn()) {
//                Toast.makeText(this, "Zone "+zone, Toast.LENGTH_SHORT).show();
                //Toast.makeText(this, "Your Online", Toast.LENGTH_SHORT).show();
                try {
                    new DownlaodGetdistwormdata(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
                } catch (Exception err) {
                    Log.i("WebServiceError", "Err " + err);
                    Toast.makeText(this, "Error " + err, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Check your inernet Connection", Toast.LENGTH_SHORT).show();
                //tv.setVisibility(View.VISIBLE);
                String str;
                str = sv.loadSharedPreference_DistWiseWorm();
                labels = dbAdapter.getzone1areaname();
                drawDistrictWiseWorm(str,labels);
            }
        }else if(barchart1.equals("true")){
            if (isInternetOn()) {
                //Toast.makeText(this, "Your Online", Toast.LENGTH_SHORT).show();
                try {
                    new DownloadDistWiseSCAttendancestring(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
                } catch (Exception err) {
                    Log.i("WebServiceError", "Err " + err);
                    Toast.makeText(this, "Error " + err, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Check your inernet Connection", Toast.LENGTH_SHORT).show();
//                tv.setVisibility(View.VISIBLE);
                String str;
                str = sv.loadSharedPreference_DistrictWiseSC();
                drawDistWiseOffline(str);
            }
        }else if(barChart.equals("true")){
            if (isInternetOn()) {
                //Toast.makeText(this, "Your Online", Toast.LENGTH_SHORT).show();
                try {
                    String sendOp;
                    if(zone.equals("16-17")) {
                        sendOp = "barStudentLearning9thand10th1617";
                        new DownloadbarDistWiseStudentLearning9th10th1617(DistWiseGraph.this, DistWiseGraph.this,sendOp, Dhandler).execute("");
                    }else if(zone.equals("17-18")){
                        sendOp = "barStudentLearning9thand10th1718";
                        new DownloadbarDistWiseStudentLearning9th10th1617(DistWiseGraph.this, DistWiseGraph.this,sendOp, Dhandler).execute("");
                    }else if(zone.equals("18-19")){
                        sendOp = "barStudentLearning9thand10th1819";
                     new DownloadbarDistWiseStudentLearning9th10th1617(DistWiseGraph.this, DistWiseGraph.this,sendOp, Dhandler).execute("");
                    }else{
                        sendOp = "barStudentLearning9thand10th";
                        new DownloadbarDistWiseStudentLearning9th10th1617(DistWiseGraph.this, DistWiseGraph.this,sendOp, Dhandler).execute("");
                    }
                } catch (Exception err) {
                    Log.i("WebServiceError", "Err " + err);
                    Toast.makeText(this, "Error " + err, Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(this, "Check your inernet Connection", Toast.LENGTH_SHORT).show();
//                tv.setVisibility(View.VISIBLE);
                String str;
                str = sv.loadSharedPreference_districtWise9_10();
                drawDistrictWise9_10(str);
            }
        }else if(lineChart.equals("true")){
            if (isInternetOn()) {
                try {
                    //new DownloadDistWiseSCAttendancestring(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
                    new DownloadZoneWiseSchoolCoordinatorAttendance(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
//                    Toast.makeText(this, "Year "+zone, Toast.LENGTH_SHORT).show();
                } catch (Exception err) {
                    Log.i("WebServiceError", "Err " + err);
                    Toast.makeText(this, "Error " + err, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Check your inernet Connection", Toast.LENGTH_SHORT).show();
//                tv.setVisibility(View.VISIBLE);
                String str;
                str = sv.loadSharedPreference_ZoneWise();
                drawZoneWiseSCAttendance(str);
            }
        }else if(TT.equals("true")){
            if (isInternetOn()) {
                try {
                    new DownloadTeacherTrainingCounts(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
                } catch (Exception err) {
                    Log.i("WebServiceError", "Err " + err);
                    Toast.makeText(this, "Error " + err, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Check your inernet Connection", Toast.LENGTH_SHORT).show();
//                tv.setVisibility(View.VISIBLE);
                String str;
                str = sv.loadSharedPreference_DistWiseTT();
                drawZoneWiseTTGraph(str);
            }

        }if(barChart2.equals("true")){
            if (isInternetOn()) {
                try {
                    new DownloadDistWiseIssueState(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
                } catch (Exception err) {
                    Log.i("WebServiceError", "Err " + err);
                    Toast.makeText(this, "Error " + err, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Check your inernet Connection", Toast.LENGTH_SHORT).show();//
                String str;
                str = sv.loadSharedPreference_DistWiseIssue();
                draweZoneWiseIssueState(str);
            }
        }

        distWiseChartHor.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                if(isFlag == false){
                    swipeContainer.setEnabled(false);
                    String barValue = distWiseChartHor.getBarData().getXVals().get(e.getXIndex());
                    zone = "Zone "+barValue.trim();
                    if(isInternetOn()) {
                        new DownloadDistWiseSCAttendancestring(DistWiseGraph.this, DistWiseGraph.this, Dhandler).execute("");
                    }else{
                        Toast.makeText(DistWiseGraph.this, "Please check internet connetion", Toast.LENGTH_SHORT).show();
//                        String str = "";
//                        str = new SharedValues(getApplicationContext()).loadSharedPreference_DistrictWiseSC();
                    }
                }else if(isTT == false){
                    swipeContainer.setEnabled(false);
                    String barValue = distWiseChartHor.getBarData().getXVals().get(e.getXIndex());
                    zone = "Zone "+barValue.trim();
                    String str = new SharedValues(getApplicationContext()).loadSharedPreference_DistWiseTT();
                    ArrayList<String> lbl ;
                    switch(zone){
                        case "Zone 1" :
                            lbl = dbAdapter.getzone1areaname();
                            drawDistWiseTTGraph(str,lbl);
                            break;

                        case "Zone 2" :
                            lbl = dbAdapter.getzone2areaname();
                            drawDistWiseTTGraph(str,lbl);
                            break;

                        case "Zone 3" :
                            lbl = dbAdapter.getzone3areaname();
                            drawDistWiseTTGraph(str,lbl);
                            break;

                        case "Zone 4" :
                            lbl = dbAdapter.getzone4areaname();
                            drawDistWiseTTGraph(str,lbl);
                            break;

                        case "Zone 5" :
                            lbl = dbAdapter.getzone5areaname();
                            drawDistWiseTTGraph(str,lbl);
                            break;

                        case "Zone 6" :
                            lbl = dbAdapter.getzone6areaname();
                            drawDistWiseTTGraph(str,lbl);
                            break;
                    }
                }else if(isIssueState == false){
                    swipeContainer.setEnabled(false);
                    //Toast.makeText(DistWiseGraph.this, "Code on click of issue state", Toast.LENGTH_SHORT).show();
                    String barValue = distWiseChartHor.getBarData().getXVals().get(e.getXIndex());
                    zone = "Zone "+barValue.trim();
                    String str = new SharedValues(getApplicationContext()).loadSharedPreference_DistWiseIssue();
                    ArrayList<String> lbl ;
                    switch(zone){
                        case "Zone 1" :
                            lbl = dbAdapter.getzone1areaname();
                            drawDistWiseIssueGraph(str,lbl);
                            break;

                        case "Zone 2" :
                            lbl = dbAdapter.getzone2areaname();
                            drawDistWiseIssueGraph(str,lbl);
                            break;

                        case "Zone 3" :
                            lbl = dbAdapter.getzone3areaname();
                            drawDistWiseIssueGraph(str,lbl);
                            break;

                        case "Zone 4" :
                            lbl = dbAdapter.getzone4areaname();
                            drawDistWiseIssueGraph(str,lbl);
                            break;

                        case "Zone 5" :
                            lbl = dbAdapter.getzone5areaname();
                            drawDistWiseIssueGraph(str,lbl);
                            break;

                        case "Zone 6" :
                            lbl = dbAdapter.getzone6areaname();
                            drawDistWiseIssueGraph(str,lbl);
                            break;
                    }
                }else if(barChart3.equals("true")){
                    if(isInternetOn())
                    {
                        String label = distWiseChartHor.getBarData().getDataSetByIndex(dataSetIndex).getLabel();
                        String barValue = distWiseChartHor.getBarData().getXVals().get(e.getXIndex());
                        Intent intent = new Intent(DistWiseGraph.this, DistrictWiseSchoolInfo.class);
                        intent.putExtra("dist", barValue.trim());
                        intent.putExtra("worm", true);
                        intent.putExtra("label", label.trim());
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(DistWiseGraph.this, "Please check internet connetion", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    try{
                        if(isInternetOn()) {
                            String label = distWiseChartHor.getBarData().getDataSetByIndex(dataSetIndex).getLabel();
                            String barValue = distWiseChartHor.getBarData().getXVals().get(e.getXIndex());
                            Intent intent = new Intent(DistWiseGraph.this, DistrictWiseSchoolInfo.class);
                            intent.putExtra("dist", barValue.trim());
                            intent.putExtra("worm", false);
                            intent.putExtra("label", label.trim());
                            startActivity(intent);
                        }
                        else
                        {
                            Toast.makeText(DistWiseGraph.this, "Please check internet connetion", Toast.LENGTH_SHORT).show();
                        }
                       // finish();
                    } catch (Exception err) {
                        err.printStackTrace();
                        Toast.makeText(DistWiseGraph.this, "Error :" + err, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onNothingSelected() {

            }
        });
    }

    public boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {

            return false;
        }
        return false;
    }

    public void drawDistWiseOffline(String str){
        barTitle.setText("Districts");
        final ArrayList<BarEntry> bargroupbysms = new ArrayList<>();
        final ArrayList<BarEntry> bargroupbyunique = new ArrayList<>();
        final ArrayList<BarEntry> bargroupbyinternet = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();
        String[] attendance = new String[30];
        //setTitle("District Wise SC Attendance");
        count1 = 0;
        count2 = 0;
        count3 = 0 ;
        try {
            JSONObject jsonObject = new JSONObject(str);
//by SMS
            attendance = jsonObject.getString("SMSAttendanceDist").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < attendance.length; i++) {
                bargroupbysms.add(new BarEntry(Integer.parseInt(attendance[i]), i));
                count1 = (count1 + (Integer.parseInt(attendance[i])));

            }

//by Internet
            attendance = jsonObject.getString("InternetAttendanceDist").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < attendance.length; i++) {
                bargroupbyinternet.add(new BarEntry(Integer.parseInt(attendance[i]), i));
                count2 = (count2 + (Integer.parseInt(attendance[i])));
            }

//by unique
            attendance = jsonObject.getString("UniqueAttendanceDist").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < attendance.length; i++) {
                bargroupbyunique.add(new BarEntry(Integer.parseInt(attendance[i]), i));
                count3 = (count3 + (Integer.parseInt(attendance[i])));
            }

//Dataset
            BarDataSet barDataSetSMS = new BarDataSet(bargroupbysms, "sms");  // creating dataset for group1
            barDataSetSMS.setColor(getResources().getColor(R.color.sms));

            BarDataSet barDataSetInternet = new BarDataSet(bargroupbyinternet, "Internet"); // creating dataset for group1
            barDataSetInternet.setColor(getResources().getColor(R.color.internet));

            BarDataSet barDataSetUbique = new BarDataSet(bargroupbyunique, "Unique");  // creating dataset for group1
            barDataSetUbique.setColor(getResources().getColor(R.color.unique));

//Zone
            attendance = jsonObject.getString("District").replace("[", "").replace("]", "").replace("\"","").split(",");
            for (int i = 0; i < attendance.length; i++) {
                labels.add(attendance[i].trim());
            }

            ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
            dataSets.add(barDataSetUbique);
            dataSets.add(barDataSetInternet);
            dataSets.add(barDataSetSMS);

            y = distWiseChartHor.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = distWiseChartHor.getAxisRight();
            y1.setEnabled(false);

            distWiseChartHor.getAxisRight().setDrawGridLines(false);
            distWiseChartHor.getAxisLeft().setDrawGridLines(false);
            distWiseChartHor.getXAxis().setDrawGridLines(false);
            distWiseChartHor.setDescription("");

            BarData data = new BarData(labels, dataSets); // initialize the Bardata with argument labels and dataSet
            distWiseChartHor.setData(data);

            //barChart1.setDescription("Set Bar Chart Description");  // set the description

            distWiseChartHor.animateY(2000);
           /* lbl.setText("Total Attendance Count");
            txt3.setText("Sms");
            val3.setText(count1);

            txt1.setText("Unique");
            val1.setText(count3);

            txt2.setText("Internet");
            val2.setText(count2);*/

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void drawDistrictWise(String str , ArrayList<String> labels){
        barTitle.setText("Districts");
        setTitle("District Wise Dashboard");
        isFlag = true ;
        isZoneWiseGraph = true ;
        final ArrayList<BarEntry> bargroupbysms = new ArrayList<>();
        final ArrayList<BarEntry> bargroupbyunique = new ArrayList<>();
        final ArrayList<BarEntry> bargroupbyinternet = new ArrayList<>();
        String[] attendance = new String[30];
        String[] smscount = new String[30];
        String[] internet = new String[30];
        String[] unique = new String[30]; count1=0; count2=0;count3=0;
        int k=0;
        try {
            JSONObject jsonObject = new JSONObject(str);
            attendance = jsonObject.getString("District").replace("[", "").replace("]", "").replace("\"","").split(",");
            smscount = jsonObject.getString("SMSAttendanceDist").replace("[", "").replace("]", "").split(",");
            internet = jsonObject.getString("InternetAttendanceDist").replace("[", "").replace("]", "").split(",");
            unique = jsonObject.getString("UniqueAttendanceDist").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < attendance.length; i++) {
                for(int j=0;j<labels.size();j++) {
                    if (attendance[i].trim().equals(labels.get(j))) {
                        bargroupbysms.add(new BarEntry(Integer.parseInt(smscount[i]), k));
                        bargroupbyinternet.add(new BarEntry(Integer.parseInt(internet[i]), k));
                        bargroupbyunique.add(new BarEntry(Integer.parseInt(unique[i]), k));
                        count1 = (count1 + (Integer.parseInt(smscount[i])));
                        count2 = (count2 + (Integer.parseInt(unique[i])));
                        count3 = (count3 + (Integer.parseInt(internet[i])));
                        k++;
                    }
                }
            }

            BarDataSet barDataSetSMS = new BarDataSet(bargroupbysms, "sms");  // creating dataset for group1
            barDataSetSMS.setColor(getResources().getColor(R.color.sms));

            BarDataSet barDataSetInternet = new BarDataSet(bargroupbyinternet, "Internet"); // creating dataset for group1
            barDataSetInternet.setColor(getResources().getColor(R.color.internet));

            BarDataSet barDataSetUbique = new BarDataSet(bargroupbyunique, "Unique");  // creating dataset for group1
            barDataSetUbique.setColor(getResources().getColor(R.color.unique));

            ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
            dataSets.add(barDataSetUbique);
            dataSets.add(barDataSetInternet);
            dataSets.add(barDataSetSMS);
            y = distWiseChartHor.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = distWiseChartHor.getAxisRight();
            y1.setEnabled(false);

            distWiseChartHor.getAxisRight().setDrawGridLines(false);
            distWiseChartHor.getAxisLeft().setDrawGridLines(false);
            distWiseChartHor.getXAxis().setDrawGridLines(false);
            distWiseChartHor.setDescription("");

            BarData data = new BarData(labels, dataSets); // initialize the Bardata with argument labels and dataSet
            distWiseChartHor.setVerticalScrollBarEnabled(false);
            distWiseChartHor.setData(data);
            //barChart1.setDescription("Set Bar Chart Description");  // set the description

            distWiseChartHor.animateY(2000);
            title.setText("District Wise SC Attendance");

            txt3.setVisibility(View.VISIBLE);
            val3.setVisibility(View.VISIBLE);

            txt1.setVisibility(View.VISIBLE);
            val1.setVisibility(View.VISIBLE);

            txt2.setVisibility(View.VISIBLE);
            val2.setVisibility(View.VISIBLE);
            String yourFormattedString;
            txt3.setText("Sms");
            val3.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count1))));

            txt1.setText("Unique");
            val1.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count2))));

            txt2.setText("Internet");
            val2.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count3))));

        }catch (JSONException e){

        }

    }

    public void drawDistrictWiseWorm(String str, ArrayList<String> labels){
        barTitle.setText("Districts");
        setTitle("District Wise Dashboard");
        ArrayList<BarEntry> bargroupzonecount = new ArrayList<>();
       String[] zCount = new String[30];
        String[] zoneName = new String[30];
      ArrayList<String> lbl = new ArrayList<>();
        //setTitle("District Wise Worm Upload");
        count2=0;
        int k = 0;
        try {
            JSONObject jsonObject = new JSONObject(str);
//bargroupzonecount
            zCount = jsonObject.getString("count").replace("[", "").replace("]", "").split(",");
            zoneName = jsonObject.getString("DistrictName").toString().trim().replace("[", "").replace("]", "").replace("\"","").split(",");
            for (int i = 0; i < zoneName.length; i++) {
                for(int j=0;j<labels.size();j++){
                    if(zoneName[i].trim().equals(labels.get(j))){
                     /*   String s =zoneName[i];
                        String s1 = labels.get(j);*/
                        bargroupzonecount.add(new BarEntry(Integer.parseInt(zCount[i]), k));
                        lbl.add(labels.get(j));
                        count2 = (count2 + (Integer.parseInt(zCount[i])));
                        k++;
                    }
                }
            }
//Dataset
            BarDataSet barDataSetzonecount = new BarDataSet(bargroupzonecount, "Zone Count");  // creating dataset for group1
            barDataSetzonecount.setColor(getResources().getColor(R.color.internet));

            ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();  // combined all dataset into an arraylist
            dataSets.add(barDataSetzonecount);

            y = distWiseChartHor.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = distWiseChartHor.getAxisRight();
            y1.setEnabled(false);

            distWiseChartHor.getAxisRight().setDrawGridLines(false);
            distWiseChartHor.getAxisLeft().setDrawGridLines(false);
            distWiseChartHor.getXAxis().setDrawGridLines(false);
            distWiseChartHor.setDescription("");

            BarData data = new BarData(lbl, dataSets); // initialize the Bardata with argument labels and dataSet
            distWiseChartHor.setData(data);

            //barChart1.setDescription("Set Bar Chart Description");  // set the description

            distWiseChartHor.animateY(2000);
            title.setText("Dist Wise Worm Upload");
            txt2.setVisibility(View.VISIBLE);
            val2.setVisibility(View.VISIBLE);
            String yourFormattedString;

            txt2.setText("Total Count");
            val2.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count2))));

            txt1.setVisibility(View.GONE);
            val1.setVisibility(View.GONE);
            txt3.setVisibility(View.GONE);
            val3.setVisibility(View.GONE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void drawDistrictWise9_10(String str){
        barTitle.setText("Districts");
        setTitle("District Wise Dashboard");
        String[] arr_10_std_count = new String[30];
        ArrayList<String> labels = new ArrayList<String>();
        final ArrayList<BarEntry> bargroup10 = new ArrayList<>();
        final ArrayList<BarEntry> bargroup9 = new ArrayList<>();
        count1=0;
        count2=0;
        try {
            JSONObject jsonObject = new JSONObject(str);

//Tenth Std
            arr_10_std_count = jsonObject.getString("studcount10th").replace("[", "").replace("]", "").split(",");
            for(int i=0;i<arr_10_std_count.length;i++) {
                bargroup10.add(new BarEntry(Integer.parseInt(arr_10_std_count[i]), i));
                count1 = (count1 + (Integer.parseInt(arr_10_std_count[i])));
            }

//Ninth Std
            arr_10_std_count = jsonObject.getString("studcount9th").replace("[", "").replace("]", "").split(",");
            for(int i=0;i<arr_10_std_count.length;i++) {
                bargroup9.add(new BarEntry(Integer.parseInt(arr_10_std_count[i]), i));
                count2 = (count2 + (Integer.parseInt(arr_10_std_count[i])));
            }

            BarDataSet barDataSet1 = new BarDataSet(bargroup10, "10th");  // creating dataset for group1
            barDataSet1.setColor(getResources().getColor(R.color.unique));

            BarDataSet barDataSet2 = new BarDataSet(bargroup9, "9th"); // creating dataset for group1
            barDataSet2.setColor(getResources().getColor(R.color.internet));

//Year
            arr_10_std_count = jsonObject.getString("District").replace("[", "").replace("]", "").replace("\"","").split(",");
            for(int i=0;i<arr_10_std_count.length;i++) {
                labels.add(arr_10_std_count[i]);
            }

            ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
            dataSets.add(barDataSet2);
            dataSets.add(barDataSet1);

            y = distWiseChartHor.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = distWiseChartHor.getAxisRight();
            y1.setEnabled(false);
            distWiseChartHor.getAxisRight().setDrawGridLines(false);
            distWiseChartHor.getAxisLeft().setDrawGridLines(false);
            distWiseChartHor.getXAxis().setDrawGridLines(false);
            distWiseChartHor.setDescription("");

            BarData data = new BarData(labels, dataSets); // initialize the Bardata with argument labels and dataSet
            distWiseChartHor.setData(data);

            //barChart.setDescription("Set Bar Chart Description");  // set the description

            distWiseChartHor.animateY(2000);
            title.setText("SL Dashboard 9th-10th");
            lbl.setText("Year " + zone +" Total Count");

            String yourFormattedString;

            txt3.setText("10th");
            val3.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count1))));
            txt2.setVisibility(View.GONE);
            val2.setVisibility(View.GONE);
            txt1.setText("9th");
            val1.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count2))));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void drawZoneWiseSCAttendance(String str){
        barTitle.setText("Zone");
        setTitle("Zone Wise DashBoard");
        isFlag = false ;
        final ArrayList<BarEntry> bargroupbysms = new ArrayList<>();
        final ArrayList<BarEntry> bargroupbyunique = new ArrayList<>();
        final ArrayList<BarEntry> bargroupbyinternet = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();
        //setTitle("Zone Wise SC Attedance");
        String[] attendance = new String[6];
        String s[] = new String[2];count1=0; count2=0;count3=0;
        try {
            JSONObject jsonObject = new JSONObject(str);
//by SMS
            attendance = jsonObject.getString("SMSAttendanceZone").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < attendance.length; i++) {
                bargroupbysms.add(new BarEntry(Integer.parseInt(attendance[i]), i));
                count3 = count3 + Integer.parseInt(attendance[i]);
            }

//by Internet
            attendance = jsonObject.getString("InternetAttendanceZone").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < attendance.length; i++) {
                bargroupbyinternet.add(new BarEntry(Integer.parseInt(attendance[i]), i));
                count2 = count2 + Integer.parseInt(attendance[i]);
            }

//by unique
            attendance = jsonObject.getString("UniqueAttendanceZone").replace("[", "").replace("]", "").split(",");
            for (int i = 0; i < attendance.length; i++) {
                bargroupbyunique.add(new BarEntry(Integer.parseInt(attendance[i]), i));
                count1 = count1 + Integer.parseInt(attendance[i]);
            }

//Dataset
            BarDataSet barDataSetSMS = new BarDataSet(bargroupbysms, "Sms");  // creating dataset for group1
            barDataSetSMS.setColor(getResources().getColor(R.color.sms));

            BarDataSet barDataSetInternet = new BarDataSet(bargroupbyinternet, "Internet"); // creating dataset for group1
            barDataSetInternet.setColor(getResources().getColor(R.color.internet));

            BarDataSet barDataSetUbique = new BarDataSet(bargroupbyunique, "Unique");  // creating dataset for group1
            barDataSetUbique.setColor(getResources().getColor(R.color.unique));

//Zone
            attendance = jsonObject.getString("Zone").replace("[", "").replace("]", "").replace("\"","").split(",");
            for (int i = 0; i < attendance.length; i++) {
                s = attendance[i].split(" ");
                labels.add(s[1]);
            }

            ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
            dataSets.add(barDataSetUbique);
            dataSets.add(barDataSetInternet);
            dataSets.add(barDataSetSMS);

            y = distWiseChartHor.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = distWiseChartHor.getAxisRight();
            y1.setEnabled(false);

            distWiseChartHor.getAxisRight().setDrawGridLines(false);
            distWiseChartHor.getAxisLeft().setDrawGridLines(false);
            distWiseChartHor.getXAxis().setDrawGridLines(false);
            distWiseChartHor.setDescription("");

            BarData data = new BarData(labels, dataSets); // initialize the Bardata with argument labels and dataSet
            distWiseChartHor.setData(data);

            //barChart1.setDescription("Set Bar Chart Description");  // set the description

            distWiseChartHor.animateY(2000);
            title.setText("Zone Wise SC Attendance");
            lbl.setText("Total Count");
            txt3.setVisibility(View.VISIBLE);
            val3.setVisibility(View.VISIBLE);
            String yourFormattedString;

            txt3.setText("Sms");
            val3.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count3))));

            txt1.setText("Unique");
            val1.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count1))));

            txt2.setText("Internet");
            val2.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count2))));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void drawZoneWiseTTGraph(String str){
        barTitle.setText("Zone");
        setTitle("Zone Wise DashBoard");
        isTT = false ;
        final ArrayList<BarEntry> bargrouppass = new ArrayList<>();
        final ArrayList<BarEntry> bargroupenroll = new ArrayList<>();
        String[]  districts ;
        String[] pass ;
        String[] enroll ;
        ArrayList<String> zones;
        ArrayList<String> label = new ArrayList<>();
        int sum1 = 0 , sum2 = 0;
        count1 = 0;
        count2 = 0;
        int j;
        try {
            JSONObject jsonObject = new JSONObject(str);
            enroll = jsonObject.getString("Enroll").replace("[", "").replace("]", "").split(",");
            districts = jsonObject.getString("CenterDistrict").replace("[", "").replace("]", "").replace("\"","").split(",");
            pass = jsonObject.getString("Passed").replace("[", "").replace("]", "").replace("\"","").split(",");
//Zone 1
            zones = dbAdapter.getzone1areaname();
            label.add("1");
            for(int i = 0 ; i < zones.size() ; i++ ){
                for (j = 0; j < districts.length; j++) {
                    if(zones.get(i).trim().equals(districts[j].toString().trim().toUpperCase())){
                        count1 = (count1 + (Integer.parseInt(enroll[j])));
                        count2 = (count2 + (Integer.parseInt(pass[j])));
                    }

                }
            }
            sum1 = sum1 + count1 ;
            sum2 = sum2 + count2 ;
            bargroupenroll.add(new BarEntry(count1, 0));
            bargrouppass.add(new BarEntry(count2, 0));
//Zone 2
            count1 = 0 ;count2 = 0;
            zones = dbAdapter.getzone2areaname();
            label.add("2");
            for(int i = 0 ; i < zones.size() ; i++ ){
                for (j = 0; j < districts.length; j++) {
                    if(zones.get(i).trim().equals(districts[j].toString().trim().toUpperCase())){
                        count1 = (count1 + (Integer.parseInt(enroll[j])));
                        count2 = (count2 + (Integer.parseInt(pass[j])));
                    }

                }
            }
            sum1 = sum1 + count1 ;
            sum2 = sum2 + count2 ;
            bargroupenroll.add(new BarEntry(count1, 1));
            bargrouppass.add(new BarEntry(count2, 1));
//Zone 3
            count1 = 0; count2 = 0;
            zones = dbAdapter.getzone3areaname();
            label.add("3");
            for(int i = 0 ; i < zones.size() ; i++ ){
                for (j = 0; j < districts.length; j++) {
                    if(zones.get(i).trim().equals(districts[j].toString().trim().toUpperCase())){
                        count1 = (count1 + (Integer.parseInt(enroll[j])));
                        count2 = (count2 + (Integer.parseInt(pass[j])));
                    }

                }
            }
            sum1 = sum1 + count1 ;
            sum2 = sum2 + count2 ;
            bargroupenroll.add(new BarEntry(count1, 2));
            bargrouppass.add(new BarEntry(count2, 2));
//Zone 4
            count1 = 0; count2 = 0;
            zones = dbAdapter.getzone4areaname();
            label.add("4");
            for(int i = 0 ; i < zones.size() ; i++ ){
                for (j = 0; j < districts.length; j++) {
                    if(zones.get(i).trim().equals(districts[j].toString().trim().toUpperCase())){
                        count1 = (count1 + (Integer.parseInt(enroll[j])));
                        count2 = (count2 + (Integer.parseInt(pass[j])));
                    }

                }
            }
            sum1 = sum1 + count1 ;
            sum2 = sum2 + count2 ;
            bargroupenroll.add(new BarEntry(count1, 3));
            bargrouppass.add(new BarEntry(count2, 3));
//Zone 5
            count1 = 0; count2 = 0;
            zones = dbAdapter.getzone5areaname();
            label.add("5");
            for(int i = 0 ; i < zones.size() ; i++ ){
                for (j = 0; j < districts.length; j++) {
                    if(zones.get(i).trim().equals(districts[j].toString().trim().toUpperCase())){
                        count1 = (count1 + (Integer.parseInt(enroll[j])));
                        count2 = (count2 + (Integer.parseInt(pass[j])));
                    }

                }
            }
            sum1 = sum1 + count1 ;
            sum2 = sum2 + count2 ;
            bargroupenroll.add(new BarEntry(count1, 4));
            bargrouppass.add(new BarEntry(count2, 4));
//Zone 6
            count1 = 0; count2 = 0;
            zones = dbAdapter.getzone6areaname();
            label.add("6");
            for(int i = 0 ; i < zones.size() ; i++ ){
                for (j = 0; j < districts.length; j++) {
                    if(zones.get(i).trim().equals(districts[j].toString().trim().toUpperCase())){
                        count1 = (count1 + (Integer.parseInt(enroll[j])));
                        count2 = (count2 + (Integer.parseInt(pass[j])));
                    }

                }
            }
            sum1 = sum1 + count1 ;
            sum2 = sum2 + count2 ;
            bargroupenroll.add(new BarEntry(count1, 5));
            bargrouppass.add(new BarEntry(count2, 5));




//Dataset
            BarDataSet barDataSetSMS = new BarDataSet(bargrouppass, "Passed");  // creating dataset for group1
            barDataSetSMS.setColor(getResources().getColor(R.color.unique));

            BarDataSet barDataSetInternet = new BarDataSet(bargroupenroll, "Enroll"); // creating dataset for group1
            barDataSetInternet.setColor(getResources().getColor(R.color.internet));

            ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
            dataSets.add(barDataSetInternet);
            dataSets.add(barDataSetSMS);

            y = distWiseChartHor.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = distWiseChartHor.getAxisRight();
            y1.setEnabled(false);

            distWiseChartHor.getAxisRight().setDrawGridLines(false);
            distWiseChartHor.getAxisLeft().setDrawGridLines(false);
            distWiseChartHor.getXAxis().setDrawGridLines(false);
            distWiseChartHor.setDescription("");
            title.setText("Zone Wise Teacher Training");
            lbl.setText("Total Count");

            txt1.setVisibility(View.VISIBLE);
            val1.setVisibility(View.VISIBLE);
            txt3.setVisibility(View.VISIBLE);
            val3.setVisibility(View.VISIBLE);
            String yourFormattedString;

            txt1.setText("Enrolled");
            val1.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(sum1))));

            txt3.setText("Passed");
            val3.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(sum2))));

            txt2.setVisibility(View.GONE);
            val2.setVisibility(View.GONE);


            BarData data = new BarData(label, dataSets); // initialize the Bardata with argument labels and dataSet
            distWiseChartHor.setData(data);

            //barChart1.setDescription("Set Bar Chart Description");  // set the description

            distWiseChartHor.animateY(2000);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void drawDistWiseTTGraph(String str, ArrayList<String> lbls){
        barTitle.setText("Districts");
        setTitle("District Wise Dashboard");
        isTT = true ;
        isZoneWiseTT = true ;
        final ArrayList<BarEntry> bargrouppass = new ArrayList<>();
        final ArrayList<BarEntry> bargroupenroll = new ArrayList<>();
        String[]  districts ;
        String[] pass ;
        String[] enroll ;
        count1 = 0;
        count2 = 0;
        int k = 0;
        if(!isInternetOn()){
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
        {
            try {
                JSONObject jsonObject = new JSONObject(str);
                enroll = jsonObject.getString("Enroll").replace("[", "").replace("]", "").split(",");
                districts = jsonObject.getString("CenterDistrict").replace("[", "").replace("]", "").replace("\"", "").split(",");
                pass = jsonObject.getString("Passed").replace("[", "").replace("]", "").replace("\"", "").split(",");

                for (int i = 0; i < lbls.size(); i++) {
                    for (int j = 0; j < districts.length; j++) {
                        if (districts[j].equals(lbls.get(i))) {
                            bargroupenroll.add(new BarEntry(Integer.parseInt(enroll[j]), k));
                            bargrouppass.add(new BarEntry(Integer.parseInt(pass[j]), k));
                            count1 = (count1 + (Integer.parseInt(enroll[j])));
                            count2 = (count2 + (Integer.parseInt(pass[j])));
                            k++;
                        }
                    }
                }
//Dataset
                BarDataSet barDataSetSMS = new BarDataSet(bargrouppass, "Passed");  // creating dataset for group1
                barDataSetSMS.setColor(getResources().getColor(R.color.unique));

                BarDataSet barDataSetInternet = new BarDataSet(bargroupenroll, "Enroll"); // creating dataset for group1
                barDataSetInternet.setColor(getResources().getColor(R.color.internet));

                ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
                dataSets.add(barDataSetInternet);
                dataSets.add(barDataSetSMS);

                y = distWiseChartHor.getAxisLeft();
                y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
                y.setTextSize(10);
                y.setAxisMinValue(0);

                y1 = distWiseChartHor.getAxisRight();
                y1.setEnabled(false);

                distWiseChartHor.getAxisRight().setDrawGridLines(false);
                distWiseChartHor.getAxisLeft().setDrawGridLines(false);
                distWiseChartHor.getXAxis().setDrawGridLines(false);
                distWiseChartHor.setDescription("");
                title.setText("Dist Wise Teacher Training");
                lbl.setText(zone);

                txt1.setVisibility(View.VISIBLE);
                val1.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                val3.setVisibility(View.VISIBLE);
                String yourFormattedString;

                txt3.setText("Passed");
                val3.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count1))));

                txt1.setText("Enrolled");
                val1.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count2))));

                txt2.setVisibility(View.GONE);
                val2.setVisibility(View.GONE);

                BarData data = new BarData(lbls, dataSets); // initialize the Bardata with argument labels and dataSet
                distWiseChartHor.setData(data);

                //barChart1.setDescription("Set Bar Chart Description");  // set the description

                distWiseChartHor.animateY(2000);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void draweZoneWiseIssueState(String str){
        barTitle.setText("Zone");
        setTitle("Zone Wise DashBoard");
        isIssueState = false ;
        final ArrayList<BarEntry> bargroupraised = new ArrayList<>();
        final ArrayList<BarEntry> bargroupoverdue = new ArrayList<>();
        final ArrayList<BarEntry> bargroupclosed = new ArrayList<>();
        String[]  districts ;
        String[] raised ;
        String[] overdue ;
        String[] closed ;
        ArrayList<String> zones;
        ArrayList<String> label = new ArrayList<>();
        int sum1 = 0, sum2 = 0, sum3 = 0 ;
        count1 = 0;
        count2 = 0;
        count3 = 0;
        int j;
        try {
            JSONObject jsonObject = new JSONObject(str);
            districts = jsonObject.getString("DistrictName").replace("[", "").replace("]", "").replace("\"","").split(",");
            raised = jsonObject.getString("Raised").replace("[", "").replace("]", "").replace("\"","").split(",");
            overdue = jsonObject.getString("overdue").replace("[", "").replace("]", "").replace("\"","").split(",");
            closed = jsonObject.getString("Closed").replace("[", "").replace("]", "").replace("\"","").split(",");
//Zone 1
            zones = dbAdapter.getzone1areaname();
            label.add("1");
            for(int i = 0 ; i < zones.size() ; i++ ){
                for (j = 0; j < districts.length; j++) {
                    if(zones.get(i).trim().equals(districts[j].toString().trim().toUpperCase())){
                        count1 = (count1 + (Integer.parseInt(raised[j])));
                        count2 = (count2 + (Integer.parseInt(overdue[j])));
                        count3 = (count3 + (Integer.parseInt(closed[j])));
                    }

                }
            }

            sum1 = sum1 + count1;
            sum2 = sum2 + count2;
            sum3 = sum3 + count3;
            bargroupraised.add(new BarEntry(count1, 0));
            bargroupoverdue.add(new BarEntry(count2, 0));
            bargroupclosed.add(new BarEntry(count3, 0));
//Zone 2
            count1 = 0 ;count2 = 0;   count3 = 0;
            zones = dbAdapter.getzone2areaname();
            label.add("2");
            for(int i = 0 ; i < zones.size() ; i++ ){
                for (j = 0; j < districts.length; j++) {
                    if(zones.get(i).trim().equals(districts[j].toString().trim().toUpperCase())){
                        count1 = (count1 + (Integer.parseInt(raised[j])));
                        count2 = (count2 + (Integer.parseInt(overdue[j])));
                        count3 = (count3 + (Integer.parseInt(closed[j])));
                    }

                }
            }
            sum1 = sum1 + count1;
            sum2 = sum2 + count2;
            sum3 = sum3 + count3;
            bargroupraised.add(new BarEntry(count1, 1));
            bargroupoverdue.add(new BarEntry(count2, 1));
            bargroupclosed.add(new BarEntry(count3, 1));
//Zone 3
            count1 = 0; count2 = 0;   count3 = 0;
            zones = dbAdapter.getzone3areaname();
            label.add("3");
            for(int i = 0 ; i < zones.size() ; i++ ){
                for (j = 0; j < districts.length; j++) {
                    if(zones.get(i).trim().equals(districts[j].toString().trim().toUpperCase())){
                        count1 = (count1 + (Integer.parseInt(raised[j])));
                        count2 = (count2 + (Integer.parseInt(overdue[j])));
                        count3 = (count3 + (Integer.parseInt(closed[j])));
                    }

                }
            }
            sum1 = sum1 + count1;
            sum2 = sum2 + count2;
            sum3 = sum3 + count3;
            bargroupraised.add(new BarEntry(count1, 2));
            bargroupoverdue.add(new BarEntry(count2, 2));
            bargroupclosed.add(new BarEntry(count3, 2));
//Zone 4
            count1 = 0; count2 = 0;   count3 = 0;
            zones = dbAdapter.getzone4areaname();
            label.add("4");
            for(int i = 0 ; i < zones.size() ; i++ ){
                for (j = 0; j < districts.length; j++) {
                    if(zones.get(i).trim().equals(districts[j].toString().trim().toUpperCase())){
                        count1 = (count1 + (Integer.parseInt(raised[j])));
                        count2 = (count2 + (Integer.parseInt(overdue[j])));
                        count3 = (count3 + (Integer.parseInt(closed[j])));
                    }

                }
            }
            sum1 = sum1 + count1;
            sum2 = sum2 + count2;
            sum3 = sum3 + count3;
            bargroupraised.add(new BarEntry(count1, 3));
            bargroupoverdue.add(new BarEntry(count2, 3));
            bargroupclosed.add(new BarEntry(count3, 3));
//Zone 5
            count1 = 0; count2 = 0;   count3 = 0;
            zones = dbAdapter.getzone5areaname();
            label.add("5");
            for(int i = 0 ; i < zones.size() ; i++ ){
                for (j = 0; j < districts.length; j++) {
                    if(zones.get(i).trim().equals(districts[j].toString().trim().toUpperCase())){
                        count1 = (count1 + (Integer.parseInt(raised[j])));
                        count2 = (count2 + (Integer.parseInt(overdue[j])));
                        count3 = (count3 + (Integer.parseInt(closed[j])));
                    }

                }
            }
            sum1 = sum1 + count1;
            sum2 = sum2 + count2;
            sum3 = sum3 + count3;
            bargroupraised.add(new BarEntry(count1, 4));
            bargroupoverdue.add(new BarEntry(count2, 4));
            bargroupclosed.add(new BarEntry(count3, 4));
//Zone 6
            count1 = 0; count2 = 0;   count3 = 0;
            zones = dbAdapter.getzone6areaname();
            label.add("6");
            for(int i = 0 ; i < zones.size() ; i++ ){
                for (j = 0; j < districts.length; j++) {
                    if(zones.get(i).trim().equals(districts[j].toString().trim().toUpperCase())){
                        count1 = (count1 + (Integer.parseInt(raised[j])));
                        count2 = (count2 + (Integer.parseInt(overdue[j])));
                        count3 = (count3 + (Integer.parseInt(closed[j])));
                    }

                }
            }
            sum1 = sum1 + count1;
            sum2 = sum2 + count2;
            sum3 = sum3 + count3;
            bargroupraised.add(new BarEntry(count1, 5));
            bargroupoverdue.add(new BarEntry(count2, 5));
            bargroupclosed.add(new BarEntry(count3, 5));


//Dataset
            BarDataSet barDataSetRaised = new BarDataSet(bargroupraised, "Raised");  // creating dataset for group1
            barDataSetRaised.setColor(getResources().getColor(R.color.orange));

            BarDataSet barDataSetClosed= new BarDataSet(bargroupclosed, "Closed"); // creating dataset for group1
            barDataSetClosed.setColor(getResources().getColor(R.color.limb));

            BarDataSet barDataSetOverdue= new BarDataSet(bargroupoverdue, "Overdue"); // creating dataset for group1
            barDataSetOverdue.setColor(getResources().getColor(R.color.RED));

            ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
            dataSets.add(barDataSetRaised);
            dataSets.add(barDataSetClosed);
            dataSets.add(barDataSetOverdue);


            y = distWiseChartHor.getAxisLeft();
            y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            y.setTextSize(10);
            y.setAxisMinValue(0);

            y1 = distWiseChartHor.getAxisRight();
            y1.setEnabled(false);

            distWiseChartHor.getAxisRight().setDrawGridLines(false);
            distWiseChartHor.getAxisLeft().setDrawGridLines(false);
            distWiseChartHor.getXAxis().setDrawGridLines(false);
            distWiseChartHor.setDescription("");
            title.setText("Zone Wise Issue Status");
            lbl.setText("Total Count");


            txt1.setText("Raised");
            val1.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(sum1))));

            txt3.setText("overdue");
            val3.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(sum2))));

            txt2.setText("Closed");
            val2.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(sum3))));

            BarData data = new BarData(label, dataSets); // initialize the Bardata with argument labels and dataSet
            distWiseChartHor.setData(data);

            //barChart1.setDescription("Set Bar Chart Description");  // set the description

            distWiseChartHor.animateY(2000);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void drawDistWiseIssueGraph(String str, ArrayList<String> lbls){
        barTitle.setText("Districts");
        isIssueState = true ;
        isZoneWiseIssueState = true ;
        setTitle("District Wise Dashboard");
        final ArrayList<BarEntry> bargroupraised = new ArrayList<>();
        final ArrayList<BarEntry> bargroupoverdue = new ArrayList<>();
        final ArrayList<BarEntry> bargroupclosed = new ArrayList<>();
        String[]  districts ;
        String[] raised ;
        String[] overdue ;
        String[] closed ;
        count1 = 0;
        count2 = 0;
        count3 = 0;
        int k = 0;
        if(!isInternetOn()){
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
        {
            try {
                JSONObject jsonObject = new JSONObject(str);
                districts = jsonObject.getString("DistrictName").replace("[", "").replace("]", "").replace("\"", "").split(",");
                raised = jsonObject.getString("Raised").replace("[", "").replace("]", "").replace("\"", "").split(",");
                overdue = jsonObject.getString("overdue").replace("[", "").replace("]", "").replace("\"", "").split(",");
                closed = jsonObject.getString("Closed").replace("[", "").replace("]", "").replace("\"", "").split(",");


                for (int i = 0; i < lbls.size(); i++) {
                    for (int j = 0; j < districts.length; j++) {
                        if (districts[j].equals(lbls.get(i))) {
                            bargroupraised.add(new BarEntry(Integer.parseInt(raised[j]), k));
                            bargroupclosed.add(new BarEntry(Integer.parseInt(closed[j]), k));
                            bargroupoverdue.add(new BarEntry(Integer.parseInt(overdue[j]), k));
                            count1 = (count1 + (Integer.parseInt(raised[j])));
                            count2 = (count2 + (Integer.parseInt(closed[j])));
                            count3 = (count3 + (Integer.parseInt(overdue[j])));
                            k++;
                        }
                    }
                }
//Dataset
                BarDataSet barDataSetRaised = new BarDataSet(bargroupraised, "Raised");  // creating dataset for group1
                barDataSetRaised.setColor(getResources().getColor(R.color.orange));

                BarDataSet barDataSetClosed = new BarDataSet(bargroupclosed, "Closed"); // creating dataset for group1
                barDataSetClosed.setColor(getResources().getColor(R.color.limb));

                BarDataSet barDataSetOverdue = new BarDataSet(bargroupoverdue, "Overdue"); // creating dataset for group1
                barDataSetOverdue.setColor(getResources().getColor(R.color.RED));

                ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
                dataSets.add(barDataSetRaised);
                dataSets.add(barDataSetClosed);
                dataSets.add(barDataSetOverdue);

                y = distWiseChartHor.getAxisLeft();
                y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
                y.setTextSize(10);
                y.setAxisMinValue(0);

                y1 = distWiseChartHor.getAxisRight();
                y1.setEnabled(false);

                distWiseChartHor.getAxisRight().setDrawGridLines(false);
                distWiseChartHor.getAxisLeft().setDrawGridLines(false);
                distWiseChartHor.getXAxis().setDrawGridLines(false);
                distWiseChartHor.setDescription("");
                title.setText("Dist Wise Issue Status");
                lbl.setText(zone);

                txt3.setText("Overdue");
                val3.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count3))));

                txt1.setText("Raised");
                val1.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count1))));


                txt2.setText("Closed");
                val2.setText(String.valueOf(NumberFormat.getNumberInstance(new Locale("en", "in")).format(new BigDecimal(count2))));


                BarData data = new BarData(lbls, dataSets); // initialize the Bardata with argument labels and dataSet
                distWiseChartHor.setData(data);

                //barChart1.setDescription("Set Bar Chart Description");  // set the description

                distWiseChartHor.animateY(2000);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class ToolbarConfigurer implements View.OnClickListener {
        private Activity activity;

        public ToolbarConfigurer(final Activity activity, Toolbar toolbar, boolean displayHomeAsUpEnabled) {
            toolbar.setTitle((this.activity = activity).getTitle());
            if (!displayHomeAsUpEnabled) return;
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
            toolbar.setNavigationOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
           // Toast.makeText(activity, "back", Toast.LENGTH_SHORT).show();
           // NavUtils.navigateUpFromSameTask(activity);
        }
    }
}
