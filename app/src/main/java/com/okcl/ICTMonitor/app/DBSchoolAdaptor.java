package com.okcl.ICTMonitor.app;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.SQLException;

/**
 * Created by abhinav on 12/12/16.
 */
public class DBSchoolAdaptor
{
    private static final String DATABASE_NAME = "SchoolsDB";// State District Taluka
    private static final String TABLE_NAME_1 = "SchoolsDB";

    private static final int DATABASE_VERSION = 4;
    private static final String TAG = "DBAdapter";

    // private static final String
    // asdf="create table if not exists sampletable1(firstname varchar,lastname varchar , no integer)";
    private static final String TABLE_CREATE_1 = "create table if not exists "
            + TABLE_NAME_1
            + " (Id integer primary key , Schoolcode text, Name text , State integer, District integer);";

    private final Context context;

    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public DBSchoolAdaptor(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
         //   db.execSQL("DROP TABLE IF EXISTS TABLE_NAME_1");

            Log.i("Table Create ", "Table Create Success");

            // db.execSQL(TABLE_CREATE_1);
            db.execSQL(TABLE_CREATE_1);
            Log.i("Table CReated : ", "DB Created 1 : " + db.getPath());
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {

            Log.w(TAG, "Upgrading database from version " + oldV + " to "
                    + newV + ", which will destroy all old data");
            // db.execSQL("DROP TABLE IF EXISTS urldata");
            //db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_1);

            onCreate(db);
        }
    }

    public DBSchoolAdaptor open() throws SQLException {

        db = DBHelper.getWritableDatabase();
        Log.i("IN Open DB ::: ", "here is the DB " + db.getPath());
        return this;
    }
//Fetch School Details


    public String getschoolName(String udise) {
        try {
            open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String d = "NA";
        String query = "Select * from " + TABLE_NAME_1 + " where Schoolcode = '" + udise + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            d = cursor.getString((cursor.getColumnIndex("Name")));
        }
        return d;
    }
}
