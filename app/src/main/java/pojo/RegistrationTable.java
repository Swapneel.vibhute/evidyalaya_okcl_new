package pojo;

/**
 * Created by abhinav on 11/9/16.
 */
public class RegistrationTable
{
    String MobilNumber, FirstName, LastName, RoleName, TalukaName, DistrictName, StateName, EMail, ReferalMobile, Pin;
    int TalukaID, DistrictID, StateID, RoleID;
    String IMEI, photo, zone;
    String sim;

    String OTPstatus;
    double Latitude, Longitude;
    int OTP;

    public String getMobilNumber() {  return MobilNumber; }
    public void setMobilNumber(String mobilNumber) {  MobilNumber = mobilNumber;  }

    public String getFirstName() {  return FirstName; }
    public void setFirstName(String firstName) {  FirstName = firstName;  }

    public String getLastName() {  return LastName;   }
    public void setLastName(String lastName) {  LastName = lastName;  }

    public String getRoleName() {   return RoleName;  }
    public void setRoleName(String roleName) {  RoleName = roleName;  }

    public String getTalukaName() {  return TalukaName;  }
    public void setTalukaName(String talukaName) {  TalukaName = talukaName;  }

    public String getDistrictName() {  return DistrictName;   }
    public void setDistrictName(String districtName) {   DistrictName = districtName;   }

    public String getStateName() {  return StateName;   }
    public void setStateName(String stateName) {  StateName = stateName;  }

    public String getEMail() {  return EMail;   }
    public void setEMail(String EMail) {  this.EMail = EMail;  }

    public String getReferalMobile() {  return ReferalMobile;  }
    public void setReferalMobile(String referalMobile) {   ReferalMobile = referalMobile;   }

    public String getPin() {  return Pin;  }
    public void setPin(String pin) {   Pin = pin;   }

    public int getTalukaID() {   return TalukaID;  }
    public void setTalukaID(int talukaID) {  TalukaID = talukaID;  }

    public int getDistrictID() {  return DistrictID;  }
    public void setDistrictID(int districtID) {  DistrictID = districtID;   }

    public int getStateID() {  return StateID;  }
    public void setStateID(int stateID) {  StateID = stateID;    }

    public int getRoleID() {  return RoleID;  }
    public void setRoleID(int roleID) {  RoleID = roleID;  }

    public String getIMEI() {  return IMEI;  }
    public void setIMEI(String IMEI) {  this.IMEI = IMEI;  }

    public String getSim() {  return sim; }
    public void setSim(String sim) {  this.sim = sim;  }

    public double getLatitude() {  return Latitude;  }
    public void setLatitude(double latitude) {  Latitude = latitude;  }

    public double getLongitude() {  return Longitude;  }
    public void setLongitude(double longitude) {  Longitude = longitude;  }

    public int getOTP() {  return OTP; }
    public void setOTP(int OTP) {  this.OTP = OTP; }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getOTPstatus() {  return OTPstatus; }
    public void setOTPstatus(String OTPstatus) {   this.OTPstatus = OTPstatus; }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }
}
