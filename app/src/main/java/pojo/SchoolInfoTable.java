package pojo;

/**
 * Created by abhinav on 18/10/16.
 */
public class SchoolInfoTable
{
    String serverID, UDISE, SchoolName, DistrictName, Zone, LandLine, HMName, HMMob, SchoolEMailID, Latitude, Longitude, IMEI, Date, Status;
    String InsertBy, InsertDate, ModifyBy, ModifyDate, Mobile, InsertByName, Block;
    int DistrictID, id;
    boolean selected;
    String UploadString;
    String Remark, RoleName, Photo;
    int RoleID;

    public SchoolInfoTable() {

    }

    public SchoolInfoTable(int id, String udise, String sName, String remark, int roleID, String roleName, String insertByMobileNumber, String insertByName, String imei, String date, boolean b)
    {
        this.id = id;
        this.UDISE = udise;
        this.SchoolName = sName;
        this.Remark = remark;
        this.RoleID = roleID;
        this.RoleName = roleName;
        this.Mobile = insertByMobileNumber;
        this.InsertByName = insertByName;
        this.IMEI = imei;
        this.Date = date;
    }


    public String getServerID() {   return serverID;  }
    public void setServerID(String serverID) {   this.serverID = serverID;  }

    public String getUDISE() {    return UDISE;  }
    public void setUDISE(String UDISE) {   this.UDISE = UDISE;  }

    public String getSchoolName() {  return SchoolName; }
    public void setSchoolName(String schoolName) {  SchoolName = schoolName; }

    public String getDistrictName() {   return DistrictName;  }
    public void setDistrictName(String districtName) {   DistrictName = districtName; }

    public String getZone() {  return Zone;  }
    public void setZone(String zone) {   Zone = zone;  }

    public String getLandLine() {  return LandLine; }
    public void setLandLine(String landLine) {   LandLine = landLine;  }

    public String getHMName() {   return HMName;  }
    public void setHMName(String HMName) {   this.HMName = HMName; }

    public String getHMMob() {return HMMob; }
    public void setHMMob(String HMMob) {  this.HMMob = HMMob; }

    public String getSchoolEMailID() {  return SchoolEMailID; }
    public void setSchoolEMailID(String schoolEMailID) { SchoolEMailID = schoolEMailID;  }

    public String getLatitude() { return Latitude;  }
    public void setLatitude(String latitude) {  Latitude = latitude; }

    public String getLongitude() {  return Longitude; }
    public void setLongitude(String longitude) {   Longitude = longitude; }

    public String getIMEI() {   return IMEI; }
    public void setIMEI(String IMEI) {  this.IMEI = IMEI; }

    public String getInsertBy() {   return InsertBy; }
    public void setInsertBy(String insertBy) {  InsertBy = insertBy;  }

    public String getInsertDate() {  return InsertDate;  }
    public void setInsertDate(String insertDate) {   InsertDate = insertDate;  }

    public String getModifyBy() {   return ModifyBy;  }
    public void setModifyBy(String modifyBy) {   ModifyBy = modifyBy;  }

    public String getModifyDate() {  return ModifyDate; }
    public void setModifyDate(String modifyDate) {   ModifyDate = modifyDate;  }

    public int getDistrictID() {  return DistrictID;  }
    public void setDistrictID(int districtID) {  DistrictID = districtID; }

    public boolean isSelected() {  return selected; }
    public void setSelected(boolean selected) {   this.selected = selected; }

    public String getUploadString() {  return UploadString;  }
    public void setUploadString(String uploadString) {  UploadString = uploadString; }

    public String getStatus() {  return Status; }
    public void setStatus(String status) {  Status = status; }

    public int getId() {  return id; }
    public void setId(int id) {  this.id = id; }

    public String getRemark() {  return Remark; }
    public void setRemark(String remark) {  Remark = remark;  }

    public String getRoleName() {  return RoleName; }
    public void setRoleName(String roleName) {  RoleName = roleName;  }

    public int getRoleID() {  return RoleID; }
    public void setRoleID(int roleID) {  RoleID = roleID; }

    public String getDate() { return Date; }
    public void setDate(String date) {  Date = date; }

    public String getMobile() {   return Mobile;  }
    public void setMobile(String mobile) {   Mobile = mobile;  }

    public String getInsertByName() {  return InsertByName;  }
    public void setInsertByName(String insertByName) {   InsertByName = insertByName;  }

    public String getPhoto() {
        return Photo;
    }
    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getBlock() {  return Block; }
    public void setBlock(String block) { Block = block; }
}