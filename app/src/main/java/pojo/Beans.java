package pojo;

/**
 * Created by abhinav on 19/8/16.
 */
public class Beans
{
    private int TalID, DistID, StateID;
    private String TalNM, DistNM, StateNM;

    public int getTalID() {
        return TalID;
    }

    public void setTalID(int talID) {
        TalID = talID;
    }

    public int getDistID() {
        return DistID;
    }

    public void setDistID(int distID) {
        DistID = distID;
    }

    public int getStateID() {
        return StateID;
    }

    public void setStateID(int stateID) {
        StateID = stateID;
    }

    public String getTalNM() {
        return TalNM;
    }

    public void setTalNM(String talNM) {
        TalNM = talNM;
    }

    public String getDistNM() {
        return DistNM;
    }

    public void setDistNM(String distNM) {
        DistNM = distNM;
    }

    public String getStateNM() {
        return StateNM;
    }

    public void setStateNM(String stateNM) {
        StateNM = stateNM;
    }
}
