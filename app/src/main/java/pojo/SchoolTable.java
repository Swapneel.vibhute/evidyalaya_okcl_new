package pojo;

/**
 * Created by abhinav on 12/9/16.
 */
public class SchoolTable
{
    String UIDISE, SchoolName, SchoolAddress, DistrictName, StateName, TalukaName, PinCode, EMail, MobileNumber, STDNumber;
    String SchoolCoOrdinatorName, SchoolCoOrdinatorMobileNumber;
    int StateID;
    int DistrictID;
    int TalukaID;
    int bolckID;
    int id;
    String Status;
    String uploadString;
    boolean selected;

    public SchoolTable(int id, String uidisEcode, String schoolName, String schoolAddress, int stateID, int districtID, int block, String pinCode, String eMail, String mobileNumber, String stDnumber, String schoolCoOrdinatorName, String schoolCoOrdinatorNumber, boolean b)
    {
        this.id = id;
        this.UIDISE = uidisEcode;
        this.SchoolName = schoolName;
        this.SchoolAddress = schoolAddress;
        this.StateID = stateID;
        this.DistrictID = districtID;
        this.bolckID = block;
        this.PinCode = pinCode;
        this.EMail = eMail;
        this.MobileNumber = mobileNumber;
        this.STDNumber = stDnumber;
        this.SchoolCoOrdinatorName = schoolCoOrdinatorName;
        this.SchoolCoOrdinatorMobileNumber = schoolCoOrdinatorNumber;
        this.selected = b;
    }

    public SchoolTable() {

    }

    public String getStatus() { return Status;}
    public void setStatus(String status) {  Status = status; }

    public String getUIDISE() {   return UIDISE;}
    public void setUIDISE(String UIDISE) {  this.UIDISE = UIDISE; }

    public String getSchoolName() {  return SchoolName;  }
    public void setSchoolName(String schoolName) {  SchoolName = schoolName;  }

    public String getSchoolAddress() {  return SchoolAddress;  }
    public void setSchoolAddress(String schoolAddress) {  SchoolAddress = schoolAddress;  }

    public String getDistrictName() {  return DistrictName;  }
    public void setDistrictName(String districtName) {  DistrictName = districtName;  }

    public String getStateName() {  return StateName; }
    public void setStateName(String stateName) {  StateName = stateName;  }

    public String getTalukaName() {  return TalukaName; }
    public void setTalukaName(String talukaName) {   TalukaName = talukaName; }

    public String getPinCode() {  return PinCode;  }
    public void setPinCode(String pinCode) {  PinCode = pinCode;  }

    public String getEMail() {  return EMail;  }
    public void setEMail(String EMail) {  this.EMail = EMail;   }

    public String getMobileNumber() { return MobileNumber;  }
    public void setMobileNumber(String mobileNumber) {  MobileNumber = mobileNumber; }

    public String getSTDNumber() {  return STDNumber;  }
    public void setSTDNumber(String STDNumber) {  this.STDNumber = STDNumber; }

    public String getSchoolCoOrdinatorName() {   return SchoolCoOrdinatorName;  }
    public void setSchoolCoOrdinatorName(String schoolCoOrdinatorName) {  SchoolCoOrdinatorName = schoolCoOrdinatorName;  }

    public String getSchoolCoOrdinatorMobileNumber() { return SchoolCoOrdinatorMobileNumber;  }
    public void setSchoolCoOrdinatorMobileNumber(String schoolCoOrdinatorMobileNumber) { SchoolCoOrdinatorMobileNumber = schoolCoOrdinatorMobileNumber;  }

    public int getStateID() { return StateID;  }
    public void setStateID(int stateID) {  StateID = stateID;  }

    public int getDistrictID() {  return DistrictID;  }
    public void setDistrictID(int districtID) {  DistrictID = districtID;  }

    public int getTalukaID() {  return TalukaID;  }
    public void setTalukaID(int talukaID) { TalukaID = talukaID; }

    public int getBolckID() { return bolckID;  }
    public void setBolckID(int bolckID) {  this.bolckID = bolckID; }

    public int getId() {  return id;}
    public void setId(int id) {   this.id = id; }

    public boolean isSelected() {  return selected;}
    public void setSelected(boolean selected) {  this.selected = selected; }

    public String getUploadString() {  return uploadString; }
    public void setUploadString(String uploadString) {   this.uploadString = uploadString;  }
}
