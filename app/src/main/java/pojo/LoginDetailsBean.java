package pojo;

/**
 * Created by abhinav on 23/10/16.
 */
public class LoginDetailsBean
{

    int id;
    String talukaid;
    String pkregid;

    public String getTalukaid() {
        return talukaid;
    }

    public void setTalukaid(String talukaid) {
        this.talukaid = talukaid;
    }

    String firstname;
    String lastname;
    String username;
    String roleid;
    String rolename;
    String reffmobile;
    String districtid;

    String userpassword;
    String imei;
    String simserial;
    String longi;
    String lati;
    String date;
    String status;
    String email;
    String photo;

    public String getDistrictid() {
        return districtid;
    }

    public void setDistrictid(String districtid) {
        this.districtid = districtid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LoginDetailsBean()
    {
        this.id = id;
        this.pkregid = pkregid;
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.roleid = roleid;
        this.rolename = rolename;
        this.reffmobile = reffmobile;
        this.userpassword = userpassword;
        this.imei = imei;
        this.simserial = simserial;
        this.longi = longi;
        this.lati = lati;
        this.date=date;
        this.status=status;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPkregid() {
        return pkregid;
    }

    public void setPkregid(String pkregid) {
        this.pkregid = pkregid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getReffmobile() {
        return reffmobile;
    }

    public void setReffmobile(String reffmobile) {
        this.reffmobile = reffmobile;
    }

    public String getUserpassword() {
        return userpassword;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSimserial() {
        return simserial;
    }

    public void setSimserial(String simserial) {
        this.simserial = simserial;
    }

    public String getLongi() {
        return longi;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }

    public String getLati() {
        return lati;
    }

    public void setLati(String lati) {
        this.lati = lati;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}







