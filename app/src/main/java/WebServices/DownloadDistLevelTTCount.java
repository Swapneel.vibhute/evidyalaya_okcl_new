package WebServices;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.okcl.ICTMonitor.app.DBAdapter;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import Support.WebAddress;

/**
 * Created by abhinav on 28/7/18.
 */

public class DownloadDistLevelTTCount extends AsyncTask<String, Void, String> {
    public final String SOAP_ACTION = "http://tempuri.org/GetDistrictName";
    public final String OPERATION_NAME = "GetDistrictName";
    public final String WSDL_TARGET_NAMESPACE = "http://tempuri.org/";
    public final String SOAP_ADDRESS = WebAddress.DashboardReports;

    Context context;
    Activity activity;
    Handler handler;
    DBAdapter adapter;
    String ID;
    int Success = 1;
    private ProgressDialog dialog;

    public DownloadDistLevelTTCount(Activity activity, Context context,String Id, Handler handler) {
        this.activity = activity;
        this.context = context;
        this.handler = handler;
        this.ID = Id ;
    }

    @Override
    protected void onPreExecute() {
//        dialog = new ProgressDialog(activity);
//        dialog.setMessage("Please wait...");
//        dialog.setIndeterminate(false);
//        dialog.setCancelable(false);
//        dialog.show();
    }

    @Override
    protected String doInBackground(String... params) {

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        long l = 0;

        PropertyInfo pi = new PropertyInfo();
        pi.setName("DistrictName");
        pi.setValue(ID);
        pi.setType(String.class);
        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS, 30000);
        String s1 = null;
        try {
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);

            SoapObject response = (SoapObject) envelope.bodyIn;

            SchoolParser schoolParser = new SchoolParser();

            Log.i("response_1", response.toString());

            s1 = httpTransport.responseDump;
            Log.i("xml", s1);

            schoolParser.parser(s1, context);

            Log.i("DumpResponse", "==> " + s1);
            Log.i("REsponse  response XML", " " + httpTransport.responseDump);

            Log.i("REsponse Soap response", " " + response.toString());

            if (envelope.bodyIn instanceof SoapObject) { // SoapObject = SUCCESS
                response = (SoapObject) envelope.bodyIn;
            } else if (envelope.bodyIn instanceof SoapFault) { // SoapFault =
                // FAILURE
                SoapFault soapFault = (SoapFault) envelope.bodyIn;
                throw new Exception(soapFault.getMessage());
            }
            //----------end for--------
            //Log.i("Response", ""+s);
            envelope.setOutputSoapObject(request);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return s1;
    }
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
//        dialog.dismiss();
        Log.i("Result ", "** " + result);

        String xml =result; //Populated XML String....
        try {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = null;
            builder = factory.newDocumentBuilder();

            Document document = builder.parse(new InputSource(new StringReader(xml)));
            Element rootElement = document.getDocumentElement();
            result=getString("GetDistrictNameResult",rootElement);
            Message message = handler.obtainMessage();
            message.obj=result;
            message.what = 5;
            handler.sendMessage(message);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    protected String getString(String tagName, Element element) {
        NodeList list = element.getElementsByTagName(tagName);
        if (list != null && list.getLength() > 0) {
            NodeList subList = list.item(0).getChildNodes();

            if (subList != null && subList.getLength() > 0) {
                return subList.item(0).getNodeValue();
            }
        }

        return null;
    }
}
