package WebServices;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

import Support.EncDecArray;
import Support.WebAddress;
import pojo.RegistrationTable;

/**
 * Created by abhinav on 28/2/17.
 */

public class ForgetPasswordWebservice extends AsyncTask<String, Void, String> {

    public final String SOAP_ACTION = "http://tempuri.org/GetPassWord";
    public final String OPERATION_NAME = "GetPassWord";
    public final String WSDL_TARGET_NAMESPACE = "http://tempuri.org/";
    public  final String SOAP_ADDRESS = WebAddress.OKCLSOAPADDRESS;

    boolean error = false;
    String const_str;
    Handler handler;
    SoapObject response = null;
    ArrayList<String> nodelist = new ArrayList<String>();
    List<String> UrlItems = new ArrayList<String>();
    RegistrationTable beans;
    private Context mContext;
    private Activity mActivity;
    private ProgressDialog pDialog;
    int success = 1;
    String value;

    public ForgetPasswordWebservice(Context context, Activity activity, String value, Handler passwordhandler)
    {
        this.mContext=context;
        this.mActivity=activity;
        this.value=value;
        this.handler=passwordhandler;
    }

    @Override
    protected void onPreExecute() {
        pDialog = new ProgressDialog(mActivity);
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    protected String doInBackground(String... urls) {
        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        PropertyInfo pi = null;


        pi = new PropertyInfo();
        pi.setName("UserName");
        pi.setValue(EncDecArray.encrypt(value));
        pi.setType(String.class);
        Log.i("Mobile no. is", " " + value);
        Log.i("Encrypt Mobile no. is", " " + EncDecArray.encrypt(value));
        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS,60000);
        String s1 = null,s=null;
        Object obj_response = null;

        try
        {
            httpTransport.debug = true;//extra
            httpTransport.call(SOAP_ACTION, envelope);

            SoapObject response = (SoapObject)envelope.bodyIn;

            obj_response = envelope.getResponse();

            s1 = httpTransport.responseDump;

            s = obj_response.toString();
            //for checking response------------
            if (envelope.bodyIn instanceof SoapObject) { // SoapObject = SUCCESS
                response = (SoapObject) envelope.bodyIn;
            } else if (envelope.bodyIn instanceof SoapFault) { // SoapFault =
                // FAILURE
                SoapFault soapFault = (SoapFault) envelope.bodyIn;
                throw new Exception(soapFault.getMessage());
            }
            //----------end for--------
            //Log.i("REsponse", ""+s);
            envelope.setOutputSoapObject(request);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            //pDialog.dismiss();
        }
        return s;
    }

    @Override
    protected void onPostExecute(String  content)
    {
        try
        {
            pDialog.dismiss();
            pDialog= null;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        try {
            if ((this.pDialog != null) && this.pDialog.isShowing())
            {
                this.pDialog.dismiss();
            }
            if(content.equals("106"))
            {
                /*handler.obtainMessage(success).sendToTarget();*/
                handler.obtainMessage(106).sendToTarget();
            }
            else if(content.equals("103"))
            {
                handler.obtainMessage(103).sendToTarget();
            }
            else if(content.equals("104"))
            {
                handler.obtainMessage(104).sendToTarget();
            }
            else if(content.equals("108"))
            {
                handler.obtainMessage(108).sendToTarget();
            } else {
                handler.obtainMessage(0).sendToTarget();
            }
        }
        catch (final IllegalArgumentException e)
        {
            // Handle or log or ignore
        } catch (final Exception e) {
            // Handle or log or ignore
        } finally {
            this.pDialog = null;
        }
/*
        try{
            if(pDialog != null)
            {
                pDialog.dismiss();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        try{

        }catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }*/
    }

}
