package WebServices;

import android.content.Context;
import android.util.Log;

import com.okcl.ICTMonitor.app.DBAdapter;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;

import Support.SharedValues;
import pojo.LoginDetailsBean;

/**
 * Created by abhinav on 23/10/16.
 */
public class LoginParsing
{
    Context context;
    DBAdapter dbAdapter;
    public static boolean b = false;
    static long status;
    ArrayList<LoginDetailsBean> list1 = new ArrayList<LoginDetailsBean>();
    LoginDetailsBean loginDetailsBean;
    String a ="";

    public void parser(String result, Context context)
    {
        XmlPullParserFactory pullParserFactory;
        this.context = context;
        dbAdapter = new DBAdapter(context, DBAdapter.DATABASE_NAME, null, DBAdapter.DATABASE_VERSION, DBAdapter.db);
        Log.i("Response in Signup ", result);

        InputStream is = new ByteArrayInputStream(result.getBytes());
        Log.i("Stream ", is.toString());

        try {
            pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
            parser.setInput(is, null);
            parseXML(parser);

        } catch (Exception ex) {
            Log.i("Error in Signup", ex.getMessage());
        }
    }

    private void parseXML(XmlPullParser parser) throws XmlPullParserException, IOException, SQLException {

        int eventType = parser.getEventType();
        ArrayList<LoginDetailsBean> storeList=null;
        ArrayList<LoginDetailsBean> storeList1=null;
        LoginDetailsBean loginDetailsBean = null;
        while (eventType != XmlPullParser.END_DOCUMENT)
        {
            String name = null;

            switch (eventType)
            {
                case XmlPullParser.START_DOCUMENT:
                    storeList = new ArrayList<LoginDetailsBean>();
                    break;
                case XmlPullParser.START_TAG:
                {
                    name = parser.getName();

                    if (name.equals("tblLoginDetails"))
                    {
                        loginDetailsBean = new LoginDetailsBean();
                    }
                    if (name.equals("AssignZoneMaster"))
                    {
                        loginDetailsBean = new LoginDetailsBean();
                    }
                    if (loginDetailsBean != null)
                    {
                        if (name.equals("PK_RegId")) {
                            loginDetailsBean.setPkregid(parser.nextText().trim());
                        }
                        if (name.equals("FirstName")) {
                            loginDetailsBean.setFirstname(parser.nextText().trim());
                        }
                        if (name.equals("LastName")) {
                            loginDetailsBean.setLastname(parser.nextText().trim());
                        }
                        if (name.equals("UserName"))
                        {
                            String a = parser.nextText().trim();
                            new SharedValues(context).saveSharedPreference("mobileNo", a);
                            loginDetailsBean.setUsername(a);
                        }
                        if (name.equals("RoleId"))
                        {
                            String b = parser.nextText();//as per role id to manage screens
                            new SharedValues(context).saveSharedPreference("userRole", b);
                            loginDetailsBean.setRoleid(b);
                        }
                        if (name.equals("RoleName"))
                        {
                            String c = parser.nextText().trim();
                            new SharedValues(context).saveSharedPreference("userrolename", c);
                            loginDetailsBean.setRolename(c);
                        }
                        if (name.equals("RefMobNumber"))
                        {
                            loginDetailsBean.setReffmobile(parser.nextText().trim());
                        }
                        if (name.equals("UserPassword")) {
                            loginDetailsBean.setUserpassword(parser.nextText().trim());
                        }
                        if (name.equals("IMEINumber")) {
                            loginDetailsBean.setImei(parser.nextText().trim());
                        }
                        if (name.equals("SimSerialNumber")) {
                            loginDetailsBean.setSimserial(parser.nextText().trim());
                        }
                        if (name.equals("Longitude")) {
                            loginDetailsBean.setLongi(parser.nextText().trim());
                        }
                        if (name.equals("Latitude")) {
                            loginDetailsBean.setLati(parser.nextText().trim());
                        }
                        if (name.equals("InserDate")) {
                            loginDetailsBean.setDate(parser.nextText().trim());
                        }
                        if (name.equals("EmailId")) {
                            loginDetailsBean.setEmail(parser.nextText().trim());
                        }
                        if (name.equals("TalukaId"))
                        {
                            loginDetailsBean.setTalukaid(parser.nextText().trim());
                        }
                        if (name.equals("DistrictId"))
                        {
                            loginDetailsBean.setDistrictid(parser.nextText().trim());
                        }
                        if (name.equals("Photo"))
                        {
                            String b=(parser.nextText().trim());

                            if(b!=null)
                            {
                                loginDetailsBean.setPhoto(b);
                            }
                            else
                            {
                                loginDetailsBean.setPhoto("0");
                            }
                        }
                        if (name.equals("Zone"))
                        {
                            if (a.equals(""))
                            {
                                a = a + parser.nextText().trim();
                            } else
                            {
                                a = a + "," + parser.nextText().trim();
                            }

                        }

                        loginDetailsBean.setStatus("1");
                    }
                }
                break;

                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("tblLoginDetails") && loginDetailsBean != null)
                    {
                        list1.add(loginDetailsBean);
                    }
            }
            eventType = parser.next();
        }
        Insert();
    }

    public void Insert()
    {
        if (list1.size() > 0)
        {
            if (dbAdapter.getUser().size() > 0)
            {
                 dbAdapter.deleteLoginDetails(loginDetailsBean);

                for (LoginDetailsBean tt : list1)
                {
                    status = dbAdapter.insertLoginDetails(tt, a);
                }

            }

            else
            {
                for (LoginDetailsBean tt : list1)
                {
                    status = dbAdapter.insertLoginDetails(tt, a);
                }
            }
        }
    }
}
