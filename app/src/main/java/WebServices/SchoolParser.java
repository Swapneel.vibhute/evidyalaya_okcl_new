package WebServices;

import android.content.Context;
import android.util.Log;

import com.okcl.ICTMonitor.app.DBAdapter;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;

import pojo.SchoolInfoTable;

/**
 * Created by abhinav on 19/10/16.
 */
public class SchoolParser
{
    Context context;
    DBAdapter dbAdapter;
    public static boolean b = false;
    static long status;
    ArrayList<SchoolInfoTable> list1 = new ArrayList<SchoolInfoTable>();
    SchoolInfoTable schoolInfoTable;

    public void parser(String result, Context context)
    {
        XmlPullParserFactory pullParserFactory;
        this.context = context;
        dbAdapter = new DBAdapter(context, DBAdapter.DATABASE_NAME, null, DBAdapter.DATABASE_VERSION, DBAdapter.db);
        Log.i("Response in Signup ", result);

        InputStream is = new ByteArrayInputStream(result.getBytes());
        Log.i("Stream ", is.toString());

        try {
            pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
            parser.setInput(is, null);
            parseXML(parser);

        } catch (Exception ex) {
            Log.i("Error in Signup", ex.getMessage());
        }
    }

    private void parseXML(XmlPullParser parser) throws XmlPullParserException, IOException, SQLException {

        int eventType = parser.getEventType();
        ArrayList<SchoolInfoTable> storeList=null;
        ArrayList<SchoolInfoTable> storeList1=null;
        SchoolInfoTable schoolInfoTable = null;
        DBAdapter dbAdapter = new DBAdapter(context, DBAdapter.DATABASE_NAME, null, DBAdapter.DATABASE_VERSION, DBAdapter.db);
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String name = null;

            switch (eventType)
            {
                case XmlPullParser.START_DOCUMENT:
                    storeList = new ArrayList<SchoolInfoTable>();
                    break;
                case XmlPullParser.START_TAG:
                {
                    name = parser.getName();

                    if (parser.getName().equals("tblSchoolInformation"))
                    {
                        schoolInfoTable = new SchoolInfoTable();
                    }
                    if (schoolInfoTable != null)
                    {
                        if (name.equals("UdiseCode"))
                        {
                            schoolInfoTable.setUDISE(parser.nextText().trim());
                        }
                        if (name.equals("SchoolName")) {
                            schoolInfoTable.setSchoolName(parser.nextText().trim());
                        }
                        if (name.equals("DistrictId")) {
                            schoolInfoTable.setDistrictID(parser.next());
                        }
                        if (name.equals("DistrictName")) {
                            schoolInfoTable.setDistrictName(parser.nextText().trim());
                        }
                        if (name.equals("Zone")) {
                            schoolInfoTable.setZone(parser.nextText().trim());
                        }
                        if (name.equals("LandLineNo")) {
                            schoolInfoTable.setLandLine(parser.nextText().trim());
                        }
                        if (name.equals("HeadMasterName"))
                        {
                            schoolInfoTable.setHMName(parser.nextText().trim());

                        }
                        if (name.equals("HeadMasterMobile")) {
                            schoolInfoTable.setHMMob(parser.nextText().trim());
                        }
                        if (name.equals("SchoolEmailId")) {
                            schoolInfoTable.setSchoolEMailID(parser.nextText().trim());
                        }
                        if (name.equals("Lattitude")) {
                            schoolInfoTable.setLatitude(parser.nextText().trim());
                        }
                        if (name.equals("Longitude")) {
                            schoolInfoTable.setLongitude(parser.nextText().trim());
                        }
                        if (name.equals("Photo"))
                        {
                            schoolInfoTable.setPhoto(parser.nextText().trim());
                        }
                        schoolInfoTable.setBlock("0");
                        schoolInfoTable.setStatus("2");
                    }
                }
                break;

                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("tblSchoolInformation") && schoolInfoTable != null)
                    {
                        list1.add(schoolInfoTable);
                    }
            }
            eventType = parser.next();
        }
        Insert();
    }

    public void Insert()
    {
        /*if (list1.size() > 0)
        {
            if (dbAdapter.getTicketCount().size() > 0)
            {
               // dbAdapter.deleteTicket(schoolInfoTable);

                for (SchoolInfoTable tt : list1)
                {
                    status = dbAdapter.insertSchoolInformation(tt);
                }

            } else
            {
                for (SchoolInfoTable tt : list1)
                {
                    status = dbAdapter.insertSchoolInformation(tt);
                }
            }
        }*/
    }
}
