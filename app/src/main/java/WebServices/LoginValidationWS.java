package WebServices;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import Support.WebAddress;

public class LoginValidationWS extends AsyncTask<String,Void,String>
{
    ProgressDialog progressDialog;
    Activity activity;
    Context context;
    Handler handler;
    String mobile;
    String reffmob;
    String IMEI;

    private Context mContext;
    int Success=105;

    public final String SOAP_ACTION = "http://tempuri.org/DownloadLoginDetails";//GetMemberAppsDetails//need change
    public final String OPERATION_NAME = "DownloadLoginDetails"; //GetMemberAppsDetails//need change
    public final String WSDL_TARGET_NAMESPACE = "http://tempuri.org/";
    public final String SOAP_ADDRESS =  WebAddress.OKCLSOAPADDRESS;

    public LoginValidationWS(Activity activity, Context context,String mobilenumber, String pass, String imei,  Handler handler)
    {
        this.activity = activity;
        this.mobile = String.valueOf(mobilenumber);
        this.reffmob= String.valueOf(pass);
        this.IMEI = imei;
        this.handler = handler;
        this.context=context;
    }
    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();
        progressDialog=new ProgressDialog(activity);
        progressDialog.setTitle("Please Wait...");
        progressDialog.setMessage("Send Login");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    @Override
    protected String doInBackground(String... params)
    {
        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);
        long l =0;

      /*  <usrMobile>string</usrMobile>
        <refMobile>string</refMobile>
        <imeiNumber>string</imeiNumber>
        <appKeyword>string</appKeyword>*/

        PropertyInfo pi = new PropertyInfo();
        pi.setName("username");
        pi.setValue(mobile);
        pi.setType(String.class);
        request.addProperty(pi);
//        Log.d("VALUE", pi.getValue().toString());

        pi = new PropertyInfo();
        pi.setName("password");
        pi.setValue(reffmob);
        pi.setType(String.class);
        request.addProperty(pi);
//        Log.d("VALUE", pi.getValue().toString());

        pi = new PropertyInfo();
        pi.setName("imei");
        pi.setValue(IMEI);
        pi.setType(String.class);
        request.addProperty(pi);
        Log.d("VALUE", pi.getValue().toString());

       /* pi = new PropertyInfo();
        pi.setName("appKeyword");
        pi.setValue("MYCTAPP");
        pi.setType(String.class);
        request.addProperty(pi);
        Log.d("VALUE", pi.getValue().toString());*/

        l = 0;
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS,30000);

        String s1 = null;
        try
        {
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);

            SoapObject response = (SoapObject)envelope.bodyIn;

            LoginParsing loginparsing = new LoginParsing();

            Log.i("response_1", response.toString());

            s1 = httpTransport.responseDump;
            Log.i("xml",s1);

            loginparsing.parser(s1, context);

            if (envelope.bodyIn instanceof SoapObject) { // SoapObject = SUCCESS
                response = (SoapObject) envelope.bodyIn;
            } else if (envelope.bodyIn instanceof SoapFault) { // SoapFault =
                // FAILURE
                SoapFault soapFault = (SoapFault) envelope.bodyIn;
                throw new Exception(soapFault.getMessage());
            }
            //----------end for--------
            //Log.i("Response", ""+s);
            envelope.setOutputSoapObject(request);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        return s1;
    }
    @Override
    protected void onPostExecute(String result)
    {
        super.onPostExecute(result);
        //Log.i("Result ", "** " + result);
        progressDialog.dismiss();
        try {
            if (result.contains("<Table><PK_RegId>"))
            {
                handler.obtainMessage(1).sendToTarget();
            }
            else if (result.contains("<tblLoginDetails><PK_RegId>"))
            {
                handler.obtainMessage(1).sendToTarget();
            }
            else if (result.contains("<NoRecord>106"))
            {
                handler.obtainMessage(105).sendToTarget();
            }
            else if (result.contains("<Table1><IMEIError>109"))
            {
                handler.obtainMessage(109).sendToTarget();
            }
            else
            {
                handler.obtainMessage(0).sendToTarget();
            }
        }
        catch (Exception e)
        {

        }
    }
}